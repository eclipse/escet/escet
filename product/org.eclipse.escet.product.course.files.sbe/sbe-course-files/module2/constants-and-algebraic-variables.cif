//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

automaton bridge_deck:
    disc int angle = 0;
    event open_further, close_further;

    const int step = 10;

    alg bool is_open   = angle >= 90;
    alg bool is_closed = angle <= 0;

    location:
        initial;
        edge open_further  when angle < 90 do angle := angle + step;
        edge close_further when angle <  0 do angle := angle - step;
end

automaton bridge_light:
    event turn_on, turn_off;

    alg bool is_on = On;
    alg bool is_off = Off;

    location On:
        initial;
        edge turn_off goto Off;

    location Off:
        edge turn_on goto On;
end

alg bool car_may_drive     =     bridge_deck.is_closed and bridge_light.is_off;
alg bool car_may_not_drive = not bridge_deck.is_closed or  bridge_light.is_on;
