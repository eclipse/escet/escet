//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

import "plant_definitions.cif";

group def Side():
    Gate: OpenClosedComponent(true);

    GateOpenCommand:  Command();
    GateCloseCommand: Command();

    Bridge: OpenClosedComponent(true);

    BridgeOpenCommand:  Command();
    BridgeCloseCommand: Command();

    group Culverts:
        East: OpenClosedComponent(true);
        West: OpenClosedComponent(true);

        OpenCommand:  Command();
        CloseCommand: Command();

        alg bool IsOpen   = East.IsOpen   and West.IsOpen;
        alg bool IsClosed = East.IsClosed and West.IsClosed;
    end

    group TrafficBarriers:
        East: OpenClosedComponent(false);
        West: OpenClosedComponent(false);

        OpenCommand:  Command();
        CloseCommand: Command();

        alg bool IsOpen   = East.IsOpen   and West.IsOpen;
        alg bool IsClosed = East.IsClosed and West.IsClosed;
    end

    group EnteringTLs:
        East: EnteringTL();
        West: EnteringTL();

        RedCommand:      Command();
        RedRedCommand:   Command();
        RedGreenCommand: Command();
        GreenCommand:    Command();

        alg bool IsRed      = East.IsRed      and West.IsRed;
        alg bool IsRedRed   = East.IsRedRed   and West.IsRedRed;
        alg bool IsRedGreen = East.IsRedGreen and West.IsRedGreen;
        alg bool IsGreen    = East.IsGreen    and West.IsGreen;
    end

    group LeavingTLs:
        East: LeavingTL();
        West: LeavingTL();

        RedCommand:   Command();
        GreenCommand: Command();

        alg bool IsRed   = East.IsRed   and West.IsRed;
        alg bool IsGreen = East.IsGreen and West.IsGreen;
    end

    group BridgeTLs:
        East: BridgeTL();
        West: BridgeTL();

        OnCommand:  Command();
        OffCommand: Command();

        alg bool IsOn  = East.IsOn  and West.IsOn;
        alg bool IsOff = East.IsOff and West.IsOff;
    end
end

North: Side();
South: Side();
