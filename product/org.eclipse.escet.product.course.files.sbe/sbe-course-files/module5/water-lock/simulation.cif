//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

import "lock.cif";
svgfile "lock.svg";

// Windows.

automaton def Window(alg string name):
    monitor;
    event vis, invis;

    location Invisible:
        initial;
        edge vis   goto Visible;

    location Visible:
        edge invis goto Invisible;

    svgin id name  event vis;
    svgin id name + "X" event invis;
    svgout id name + "Q" attr "visibility" value if Invisible: "hidden" else "visible" end;
end

WGateN:     Window("gateN");
WGateS:     Window("gateS");
WBridgeN:   Window("bridgeN");
WBridgeS:   Window("bridgeS");
WCulvertsN: Window("culvertsN");
WCulvertsS: Window("culvertsS");
WTrafficN:  Window("trafficN");
WTrafficS:  Window("trafficS");
WETLN:      Window("ETLN");
WETLS:      Window("ETLS");
WLTLN:      Window("LTLN");
WLTLS:      Window("LTLS");
WBTLN:      Window("BTLN");
WBTLS:      Window("BTLS");

// Commands.

svgin id "gateNOpen"      event North.GateOpenCommand.u_request;
svgin id "gateNClose"     event North.GateCloseCommand.u_request;
svgin id "gateSOpen"      event South.GateOpenCommand.u_request;
svgin id "gateSClose"     event South.GateCloseCommand.u_request;

svgin id "bridgeNOpen"    event North.BridgeOpenCommand.u_request;
svgin id "bridgeNClose"   event North.BridgeCloseCommand.u_request;
svgin id "bridgeSOpen"    event South.BridgeOpenCommand.u_request;
svgin id "bridgeSClose"   event South.BridgeCloseCommand.u_request;

svgin id "culvertsNOpen"  event North.Culverts.OpenCommand.u_request;
svgin id "culvertsNClose" event North.Culverts.CloseCommand.u_request;
svgin id "culvertsSOpen"  event South.Culverts.OpenCommand.u_request;
svgin id "culvertsSClose" event South.Culverts.CloseCommand.u_request;

svgin id "trafficNOpen"   event North.TrafficBarriers.OpenCommand.u_request;
svgin id "trafficNClose"  event North.TrafficBarriers.CloseCommand.u_request;
svgin id "trafficSOpen"   event South.TrafficBarriers.OpenCommand.u_request;
svgin id "trafficSClose"  event South.TrafficBarriers.CloseCommand.u_request;

svgin id "ETLNred"        event North.EnteringTLs.RedCommand.u_request;
svgin id "ETLNredgreen"   event North.EnteringTLs.RedGreenCommand.u_request;
svgin id "ETLNgreen"      event North.EnteringTLs.GreenCommand.u_request;
svgin id "ETLNredred"     event North.EnteringTLs.RedRedCommand.u_request;
svgin id "ETLSred"        event South.EnteringTLs.RedCommand.u_request;
svgin id "ETLSredgreen"   event South.EnteringTLs.RedGreenCommand.u_request;
svgin id "ETLSgreen"      event South.EnteringTLs.GreenCommand.u_request;
svgin id "ETLSredred"     event South.EnteringTLs.RedRedCommand.u_request;

svgin id "LTLNred"        event North.LeavingTLs.RedCommand.u_request;
svgin id "LTLNgreen"      event North.LeavingTLs.GreenCommand.u_request;
svgin id "LTLSred"        event South.LeavingTLs.RedCommand.u_request;
svgin id "LTLSgreen"      event South.LeavingTLs.GreenCommand.u_request;

svgin id "bridgeTLNOn"    event North.BridgeTLs.OnCommand.u_request;
svgin id "bridgeTLNOff"   event North.BridgeTLs.OffCommand.u_request;
svgin id "bridgeTLSOn"    event South.BridgeTLs.OnCommand.u_request;
svgin id "bridgeTLSOff"   event South.BridgeTLs.OffCommand.u_request;

// Command deactivators.

automaton def Deactivator(uncontrollable u_request, u_unrequest):
    cont t;

    location NotRequested:
        initial;
        equation t' = 0;
        edge u_request goto Requested;

    location Requested:
        equation t' = 1;
        edge u_unrequest when t > 0.1 do t := 0 goto NotRequested;
end

group NorthDeactivators:
    GateOpenCommandDeactivator:            Deactivator(North.GateOpenCommand.u_request,              North.GateOpenCommand.u_unrequest);
    GateCloseCommandDeactivator:           Deactivator(North.GateCloseCommand.u_request,             North.GateCloseCommand.u_unrequest);

    BridgeOpenCommandDeactivator:          Deactivator(North.BridgeOpenCommand.u_request,            North.BridgeOpenCommand.u_unrequest);
    BridgeCloseCommandDeactivator:         Deactivator(North.BridgeCloseCommand.u_request,           North.BridgeCloseCommand.u_unrequest);

    CulvertsOpenCommandDeactivator:        Deactivator(North.Culverts.OpenCommand.u_request,         North.Culverts.OpenCommand.u_unrequest);
    CulvertsCloseCommandDeactivator:       Deactivator(North.Culverts.CloseCommand.u_request,        North.Culverts.CloseCommand.u_unrequest);

    TrafficBarriersOpenCommandDeactivator: Deactivator(North.TrafficBarriers.OpenCommand.u_request,  North.TrafficBarriers.OpenCommand.u_unrequest);
    TrafficBarriersloseCommandDeactivator: Deactivator(North.TrafficBarriers.CloseCommand.u_request, North.TrafficBarriers.CloseCommand.u_unrequest);

    EnteringTLsRedRedCommandDeactivator:   Deactivator(North.EnteringTLs.RedRedCommand.u_request,    North.EnteringTLs.RedRedCommand.u_unrequest);
    EnteringTLsRedCommandDeactivator:      Deactivator(North.EnteringTLs.RedCommand.u_request,       North.EnteringTLs.RedCommand.u_unrequest);
    EnteringTLsRedGreenCommandDeactivator: Deactivator(North.EnteringTLs.RedGreenCommand.u_request,  North.EnteringTLs.RedGreenCommand.u_unrequest);
    EnteringTLsGreenCommandDeactivator:    Deactivator(North.EnteringTLs.GreenCommand.u_request,     North.EnteringTLs.GreenCommand.u_unrequest);

    LeavingTLsGreenCommandDeactivator:     Deactivator(North.LeavingTLs.GreenCommand.u_request,      North.LeavingTLs.GreenCommand.u_unrequest);
    LeavingTLsRedCommandDeactivator:       Deactivator(North.LeavingTLs.RedCommand.u_request,        North.LeavingTLs.RedCommand.u_unrequest);

    BridgeTLsOnCommandDeactivator:         Deactivator(North.BridgeTLs.OnCommand.u_request,          North.BridgeTLs.OnCommand.u_unrequest);
    BridgeTLsOffCommandDeactivator:        Deactivator(North.BridgeTLs.OffCommand.u_request,         North.BridgeTLs.OffCommand.u_unrequest);
end

group SouthDeactivators:
    GateOpenCommandDeactivator:            Deactivator(South.GateOpenCommand.u_request,              South.GateOpenCommand.u_unrequest);
    GateCloseCommandDeactivator:           Deactivator(South.GateCloseCommand.u_request,             South.GateCloseCommand.u_unrequest);

    BridgeOpenCommandDeactivator:          Deactivator(South.BridgeOpenCommand.u_request,            South.BridgeOpenCommand.u_unrequest);
    BridgeCloseCommandDeactivator:         Deactivator(South.BridgeCloseCommand.u_request,           South.BridgeCloseCommand.u_unrequest);

    CulvertsOpenCommandDeactivator:        Deactivator(South.Culverts.OpenCommand.u_request,         South.Culverts.OpenCommand.u_unrequest);
    CulvertsCloseCommandDeactivator:       Deactivator(South.Culverts.CloseCommand.u_request,        South.Culverts.CloseCommand.u_unrequest);

    TrafficBarriersOpenCommandDeactivator: Deactivator(South.TrafficBarriers.OpenCommand.u_request,  South.TrafficBarriers.OpenCommand.u_unrequest);
    TrafficBarriersloseCommandDeactivator: Deactivator(South.TrafficBarriers.CloseCommand.u_request, South.TrafficBarriers.CloseCommand.u_unrequest);

    EnteringTLsRedRedCommandDeactivator:   Deactivator(South.EnteringTLs.RedRedCommand.u_request,    South.EnteringTLs.RedRedCommand.u_unrequest);
    EnteringTLsRedCommandDeactivator:      Deactivator(South.EnteringTLs.RedCommand.u_request,       South.EnteringTLs.RedCommand.u_unrequest);
    EnteringTLsRedGreenCommandDeactivator: Deactivator(South.EnteringTLs.RedGreenCommand.u_request,  South.EnteringTLs.RedGreenCommand.u_unrequest);
    EnteringTLsGreenCommandDeactivator:    Deactivator(South.EnteringTLs.GreenCommand.u_request,     South.EnteringTLs.GreenCommand.u_unrequest);

    LeavingTLsGreenCommandDeactivator:     Deactivator(South.LeavingTLs.GreenCommand.u_request,      South.LeavingTLs.GreenCommand.u_unrequest);
    LeavingTLsRedCommandDeactivator:       Deactivator(South.LeavingTLs.RedCommand.u_request,        South.LeavingTLs.RedCommand.u_unrequest);

    BridgeTLsOnCommandDeactivator:         Deactivator(South.BridgeTLs.OnCommand.u_request,          South.BridgeTLs.OnCommand.u_unrequest);
    BridgeTLsOffCommandDeactivator:        Deactivator(South.BridgeTLs.OffCommand.u_request,         South.BridgeTLs.OffCommand.u_unrequest);
end

// Gates.

svgout id "gateN" attr "transform" value if North.Gate.IsClosed: "translate(0,64)" else "translate(0,0)" end;
svgout id "gateS" attr "transform" value if South.Gate.IsClosed: "translate(0,64)" else "translate(0,0)" end;

// Bridges.

svgout id "bridgeNopen"   attr "display" value if North.Bridge.IsClosed: "none" else "block" end;
svgout id "bridgeNclosed" attr "display" value if North.Bridge.IsOpen:   "none" else "block" end;
svgout id "bridgeSopen"   attr "display" value if South.Bridge.IsClosed: "none" else "block" end;
svgout id "bridgeSclosed" attr "display" value if South.Bridge.IsOpen:   "none" else "block" end;

// Culverts.

svgout id "culvertNW" attr "fill" value if North.Culverts.IsClosed: "#4094ff" else "#0058f1" end;
svgout id "culvertNE" attr "fill" value if North.Culverts.IsClosed: "#4094ff" else "#0058f1" end;
svgout id "culvertSW" attr "fill" value if South.Culverts.IsClosed: "#4094ff" else "#0058f1" end;
svgout id "culvertSE" attr "fill" value if South.Culverts.IsClosed: "#4094ff" else "#0058f1" end;

// Traffic barriers.

svgout id "TBNWred"   attr "d" value if North.TrafficBarriers.IsClosed: "m 233.511, 58.625  20.107,0" else "m 233.511, 58.625  2.107,0" end;
svgout id "TBNWwhite" attr "d" value if North.TrafficBarriers.IsClosed: "m 233.511, 58.625  20.107,0" else "m 233.511, 58.625  2.107,0" end;
svgout id "TBNEred"   attr "d" value if North.TrafficBarriers.IsClosed: "m 253.436,129.589 -20.220,0" else "m 253.436,129.589 -2.220,0" end;
svgout id "TBNEwhite" attr "d" value if North.TrafficBarriers.IsClosed: "m 253.436,129.589 -20.220,0" else "m 253.436,129.589 -2.220,0" end;
svgout id "TBSWred"   attr "d" value if South.TrafficBarriers.IsClosed: "m  59.659, 58.715  20.220,0" else "m  59.659, 58.715  2.220,0" end;
svgout id "TBSWwhite" attr "d" value if South.TrafficBarriers.IsClosed: "m  59.659, 58.715  20.220,0" else "m  59.659, 58.715  2.220,0" end;
svgout id "TBSEred"   attr "d" value if South.TrafficBarriers.IsClosed: "m  79.272,129.511 -20.220,0" else "m  79.272,129.511 -2.220,0" end;
svgout id "TBSEwhite" attr "d" value if South.TrafficBarriers.IsClosed: "m  79.272,129.511 -20.220,0" else "m  79.272,129.511 -2.220,0" end;

// Entering/leaving traffic lights.

group def EnteringTLV(alg string Red, Green, RedRed; alg bool SensorRed, SensorGreen, SensorRedRed):
    svgout id Red    attr "fill" value if SensorRed:    "red"   else "#313131" end;
    svgout id Green  attr "fill" value if SensorGreen:  "green" else "#313131" end;
    svgout id RedRed attr "fill" value if SensorRedRed: "red"   else "#313131" end;
end

EnteringTLNW: EnteringTLV("ETLNW1", "ETLNW2", "ETLNW3", North.EnteringTLs.West.SensorTopRed.On, North.EnteringTLs.West.SensorGreen.On, North.EnteringTLs.West.SensorBottomRed.On);
EnteringTLNE: EnteringTLV("ETLNE1", "ETLNE2", "ETLNE3", North.EnteringTLs.East.SensorTopRed.On, North.EnteringTLs.East.SensorGreen.On, North.EnteringTLs.East.SensorBottomRed.On);
EnteringTLSW: EnteringTLV("ETLSW1", "ETLSW2", "ETLSW3", South.EnteringTLs.West.SensorTopRed.On, South.EnteringTLs.West.SensorGreen.On, South.EnteringTLs.West.SensorBottomRed.On);
EnteringTLSE: EnteringTLV("ETLSE1", "ETLSE2", "ETLSE3", South.EnteringTLs.East.SensorTopRed.On, South.EnteringTLs.East.SensorGreen.On, South.EnteringTLs.East.SensorBottomRed.On);

// Leaving traffic lights.

group def LeavingTLV(alg string Red, Green; alg bool SensorRed, SensorGreen):
    svgout id Red   attr "fill" value if SensorRed:   "red"   else "#313131" end;
    svgout id Green attr "fill" value if SensorGreen: "green" else "#313131" end;
end

LeavingTLNW: LeavingTLV("LTLNW1", "LTLNW2", North.LeavingTLs.West.SensorRed.On, North.LeavingTLs.West.SensorGreen.On);
LeavingTLNE: LeavingTLV("LTLNE1", "LTLNE2", North.LeavingTLs.East.SensorRed.On, North.LeavingTLs.East.SensorGreen.On);
LeavingTLSW: LeavingTLV("LTLSW1", "LTLSW2", South.LeavingTLs.West.SensorRed.On, South.LeavingTLs.West.SensorGreen.On);
LeavingTLSE: LeavingTLV("LTLSE1", "LTLSE2", South.LeavingTLs.East.SensorRed.On, South.LeavingTLs.East.SensorGreen.On);

// Bridge traffic lights.

automaton BridgeTLN:
    cont t der 1;

    location Off:
        initial;
        edge when North.BridgeTLs.IsOn do t := 0 goto Blinking1;

    location Blinking1:
        edge when t > 0.6 do t := 0 goto Blinking2;
        edge when North.BridgeTLs.IsOff goto Off;

    location Blinking2:
        edge when t > 0.6 do t := 0 goto Blinking1;
        edge when North.BridgeTLs.IsOff goto Off;
end

automaton BridgeTLS:
    cont t der 1;

    location Off:
        initial;
        edge when South.BridgeTLs.IsOn do t := 0 goto Blinking1;

    location Blinking1:
        edge when t > 0.6 do t := 0 goto Blinking2;
        edge when South.BridgeTLs.IsOff goto Off;

    location Blinking2:
        edge when t > 0.6 do t := 0 goto Blinking1;
        edge when South.BridgeTLs.IsOff goto Off;
end

svgout id "BTLNW1" attr "fill" value if BridgeTLN.Blinking1: "red" else "#313131" end;
svgout id "BTLNW2" attr "fill" value if BridgeTLN.Blinking2: "red" else "#313131" end;
svgout id "BTLNE1" attr "fill" value if BridgeTLN.Blinking1: "red" else "#313131" end;
svgout id "BTLNE2" attr "fill" value if BridgeTLN.Blinking2: "red" else "#313131" end;
svgout id "BTLSW1" attr "fill" value if BridgeTLS.Blinking1: "red" else "#313131" end;
svgout id "BTLSW2" attr "fill" value if BridgeTLS.Blinking2: "red" else "#313131" end;
svgout id "BTLSE1" attr "fill" value if BridgeTLS.Blinking1: "red" else "#313131" end;
svgout id "BTLSE2" attr "fill" value if BridgeTLS.Blinking2: "red" else "#313131" end;

// Water level.

automaton WaterLevel:
    disc int H_Chamber = H_South, H_North = 2, H_South = 1;

    location CCCC:
        initial;
        edge when North.Gate.IsOpen       goto OCCC;
        edge when North.Culverts.IsOpen   goto COCC;
        edge when South.Gate.IsOpen       goto CCOC;
        edge when South.Culverts.IsOpen   goto CCCO;

    location OCCC:
        edge when H_Chamber != H_North    do H_Chamber := H_North;
        edge when North.Gate.IsClosed     goto CCCC;
        edge when North.Culverts.IsOpen   goto OOCC;
        edge when South.Gate.IsOpen       goto OCOC;
        edge when South.Culverts.IsOpen   goto OCCO;

    location COCC:
        edge when H_Chamber != H_North    do H_Chamber := H_North;
        edge when North.Gate.IsOpen       goto OOCC;
        edge when North.Culverts.IsClosed goto CCCC;
        edge when South.Gate.IsOpen       goto COOC;
        edge when South.Culverts.IsOpen   goto COCO;

    location CCOC:
        edge when H_Chamber != H_South    do H_Chamber := H_South;
        edge when North.Gate.IsOpen       goto OCOC;
        edge when North.Culverts.IsOpen   goto COOC;
        edge when South.Gate.IsClosed     goto CCCC;
        edge when South.Culverts.IsOpen   goto CCOO;

    location CCCO:
        edge when H_Chamber != H_South    do H_Chamber := H_South;
        edge when North.Gate.IsOpen       goto OCCO;
        edge when North.Culverts.IsOpen   goto COCO;
        edge when South.Gate.IsOpen       goto CCOO;
        edge when South.Culverts.IsClosed goto CCCC;

    location OOCC:
        edge when H_Chamber != H_North    do H_Chamber := H_North;
        edge when North.Gate.IsClosed     goto COCC;
        edge when North.Culverts.IsClosed goto OCCC;
        edge when South.Gate.IsOpen       goto OOOC;
        edge when South.Culverts.IsOpen   goto OOCO;

    location OCOC:
        edge when North.Gate.IsClosed     goto CCOC;
        edge when North.Culverts.IsOpen   goto OOOC;
        edge when South.Gate.IsClosed     goto OCCC;
        edge when South.Culverts.IsOpen   goto OCOO;

    location OCCO:
        edge when North.Gate.IsClosed     goto CCCO;
        edge when North.Culverts.IsOpen   goto OOCO;
        edge when South.Gate.IsOpen       goto OCOO;
        edge when South.Culverts.IsClosed goto OCCC;

    location COOC:
        edge when North.Gate.IsOpen       goto OOOC;
        edge when North.Culverts.IsClosed goto CCOC;
        edge when South.Gate.IsClosed     goto COCC;
        edge when South.Culverts.IsOpen   goto COOO;

    location COCO:
        edge when North.Gate.IsOpen       goto OOCO;
        edge when North.Culverts.IsClosed goto CCCO;
        edge when South.Gate.IsOpen       goto COOO;
        edge when South.Culverts.IsClosed goto COCC;

    location CCOO:
        edge when H_Chamber != H_South    do H_Chamber := H_South;
        edge when North.Gate.IsOpen       goto OCOO;
        edge when North.Culverts.IsOpen   goto COOO;
        edge when South.Gate.IsClosed     goto CCCO;
        edge when South.Culverts.IsClosed goto CCOC;

    location OOOC:
        edge when North.Gate.IsClosed     goto COOC;
        edge when North.Culverts.IsClosed goto OCOC;
        edge when South.Gate.IsClosed     goto OOCC;
        edge when South.Culverts.IsOpen   goto OOOO;

    location OOCO:
        edge when North.Gate.IsClosed     goto COCO;
        edge when North.Culverts.IsClosed goto OCCO;
        edge when South.Gate.IsOpen       goto OOOO;
        edge when South.Culverts.IsClosed goto OOCC;

    location OCOO:
        edge when North.Gate.IsClosed     goto CCOO;
        edge when North.Culverts.IsOpen   goto OOOO;
        edge when South.Gate.IsClosed     goto OCCO;
        edge when South.Culverts.IsClosed goto OCOC;

    location COOO:
        edge when North.Gate.IsOpen       goto OOOO;
        edge when North.Culverts.IsClosed goto CCOO;
        edge when South.Gate.IsClosed     goto COCO;
        edge when South.Culverts.IsClosed goto COOC;

    location OOOO:
        edge when North.Gate.IsClosed     goto COOO;
        edge when North.Culverts.IsClosed goto OCOO;
        edge when South.Gate.IsClosed     goto OOCO;
        edge when South.Culverts.IsClosed goto OOOC;
end

svgout id "waternorth"    attr "fill" value if WaterLevel.H_North   = 1: "#4094ff" else "#0058f1" end;
svgout id "waterchamber"  attr "fill" value if WaterLevel.H_Chamber = 1: "#4094ff" else "#0058f1" end;
svgout id "culvertNinlet" attr "fill" value if WaterLevel.H_Chamber = 1: "#4094ff" else "#0058f1" end;
svgout id "culvertSinlet" attr "fill" value if WaterLevel.H_Chamber = 1: "#4094ff" else "#0058f1" end;

svgout id "Flood" attr "visibility"
    value if WaterLevel.OCOC or WaterLevel.OCCO or WaterLevel.COOC or WaterLevel.COCO or WaterLevel.OOCO or
             WaterLevel.OCOO or WaterLevel.COOO or WaterLevel.OOOC or WaterLevel.OOOO:
              "visible"
          else
              "hidden"
          end;
