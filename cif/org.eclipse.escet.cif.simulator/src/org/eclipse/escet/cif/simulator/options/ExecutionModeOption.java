//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.options;

import org.eclipse.escet.common.app.framework.options.EnumOption;
import org.eclipse.escet.common.app.framework.options.Options;

/** {@link ExecutionMode Execution mode} option. */
public class ExecutionModeOption extends EnumOption<ExecutionMode> {
    /** Constructor for the {@link ExecutionModeOption} class. */
    public ExecutionModeOption() {
        super(
                // name
                "Execution mode",

                // description
                "Whether to use execution mode. Specify \"on\" to enable execution mode, \"off\" to disable it. "
                        + "[DEFAULT=off]",

                // cmdShort
                null,

                // cmdLong
                "exec-mode",

                // cmdValue
                "EMODE",

                // defaultValue
                ExecutionMode.OFF,

                // showInDialog
                true,

                // optDialogDescr
                "Whether to use execution mode.");
    }

    @Override
    protected String getDialogText(ExecutionMode value) {
        return switch (value) {
            case ON -> "On";
            case OFF -> "Off";
            default -> throw new RuntimeException("Unknown execution mode: " + value);
        };
    }

    /**
     * Returns a value indicating whether execution mode is enabled.
     *
     * @return A value indicating whether execution mode is enabled.
     */
    public static ExecutionMode getExecutionMode() {
        return Options.get(ExecutionModeOption.class);
    }
}
