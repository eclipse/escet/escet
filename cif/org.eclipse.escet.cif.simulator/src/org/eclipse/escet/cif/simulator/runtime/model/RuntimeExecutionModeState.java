//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.runtime.model;

/** Runtime execution mode state. */
public enum RuntimeExecutionModeState {
    /** Execute environment events. */
    ENVIRONMENT,

    /** Execute uncontrollable events loop. */
    UNCONTROLLABLE,

    /** Execute controllable events loop. */
    CONTROLLABLE,

    /** Execute time transition. */
    TIME;
}
