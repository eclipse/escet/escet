//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.runtime.model;

import org.eclipse.escet.common.java.Assert;

/** Information for the {@link RuntimeSpec#executionMode}. */
public class RuntimeExecutionModeInfo {
    /** The runtime execution mode state. */
    public final RuntimeExecutionModeState state;

    /**
     * The 0-based index of the event to consider next, given the {@link #state}. Is {@code -1} if {@link #state} is
     * {@link RuntimeExecutionModeState#TIME}.
     */
    public final int eventIdx;

    /**
     * Constructor for the {@link RuntimeExecutionModeInfo} class.
     *
     * @param state The runtime execution mode state.
     * @param eventIdx The 0-based index of the event to consider next, given the {@link #state}. Must be {@code -1} if
     *     and only if {@link #state} is {@link RuntimeExecutionModeState#TIME}.
     */
    public RuntimeExecutionModeInfo(RuntimeExecutionModeState state, int eventIdx) {
        this.state = state;
        this.eventIdx = eventIdx;
        Assert.areEqual(state == RuntimeExecutionModeState.TIME, eventIdx == -1);
    }
}
