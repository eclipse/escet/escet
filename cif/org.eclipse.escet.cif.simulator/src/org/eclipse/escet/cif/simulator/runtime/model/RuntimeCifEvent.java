//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.runtime.model;

import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.common.java.Assert;

/**
 * Runtime CIF event, representing either a {@link RuntimeEventKind#REGULAR regular} event declared in the CIF model, or
 * the {@link RuntimeEventKind#TAU tau} event.
 *
 * @param <S> The type of state objects to use.
 */
public abstract class RuntimeCifEvent<S extends RuntimeState> extends RuntimeEvent<S> {
    /**
     * Constructor for the {@link RuntimeCifEvent} class.
     *
     * @param name The name of the event. For {@link RuntimeEventKind#REGULAR regular events}, it must be the absolute
     *     name of the event in the CIF model, with keyword escaping, obtained via {@link CifTextUtils#getAbsName}. For
     *     {@link RuntimeEventKind#TAU event 'tau'}, it must be {@code "tau"}.
     * @param idx The unique 0-based index of the event.
     * @param kind The kind of runtime event.
     * @param controllable Is the event controllable? {@code null} for neither controllable nor uncontrollable.
     */
    public RuntimeCifEvent(String name, int idx, RuntimeEventKind kind, Boolean controllable) {
        super(name, idx, kind, controllable);
        Assert.check(kind == RuntimeEventKind.REGULAR || kind == RuntimeEventKind.TAU);
    }

    /**
     * Fills the {@link RuntimeSpec#syncData}, {@link RuntimeSpec#sendData}, {@link RuntimeSpec#recvData}, and/or
     * {@link RuntimeSpec#tauData} in the {@link RuntimeSpec specification} for this event.
     *
     * @param state The state to use to evaluate the guards.
     * @return {@code true} if the event is enabled (guard-wise), {@code false} otherwise.
     */
    public abstract boolean fillData(S state);

    /**
     * Is this event allowed by the state/event exclusion invariants, for the given state?
     *
     * @param state The state to use to evaluate the state/event exclusion invariants.
     * @return {@code true} if the event is allowed, {@code false} if it is disabled.
     */
    public abstract boolean allowedByInvs(S state);
}
