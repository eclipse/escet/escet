//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.cif2cif;

import static org.eclipse.escet.common.emf.EMFHelper.deepclone;
import static org.eclipse.escet.common.java.Lists.concat;
import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Lists.listc;
import static org.eclipse.escet.common.java.Maps.copy;
import static org.eclipse.escet.common.java.Maps.map;
import static org.eclipse.escet.common.java.Maps.mapc;
import static org.eclipse.escet.common.java.Sets.setc;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import org.eclipse.escet.cif.common.CifCollectUtils;
import org.eclipse.escet.cif.common.CifEdgeUtils;
import org.eclipse.escet.cif.common.CifEquationUtils;
import org.eclipse.escet.cif.common.CifLocationUtils;
import org.eclipse.escet.cif.common.CifScopeUtils;
import org.eclipse.escet.cif.common.CifTypeUtils;
import org.eclipse.escet.cif.common.CifUpdateUtils;
import org.eclipse.escet.cif.common.CifUpdateUtils.UnsupportedUpdateException;
import org.eclipse.escet.cif.common.CifValueUtils;
import org.eclipse.escet.cif.metamodel.cif.ComplexComponent;
import org.eclipse.escet.cif.metamodel.cif.InvKind;
import org.eclipse.escet.cif.metamodel.cif.Invariant;
import org.eclipse.escet.cif.metamodel.cif.IoDecl;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.SupKind;
import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.automata.Edge;
import org.eclipse.escet.cif.metamodel.cif.automata.Location;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgIn;
import org.eclipse.escet.cif.metamodel.cif.declarations.AlgVariable;
import org.eclipse.escet.cif.metamodel.cif.declarations.ContVariable;
import org.eclipse.escet.cif.metamodel.cif.declarations.Declaration;
import org.eclipse.escet.cif.metamodel.cif.declarations.DiscVariable;
import org.eclipse.escet.cif.metamodel.cif.expressions.AlgVariableExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.BinaryExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.BinaryOperator;
import org.eclipse.escet.cif.metamodel.cif.expressions.ComponentExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.ContVariableExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.DiscVariableExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.Expression;
import org.eclipse.escet.cif.metamodel.cif.expressions.LocationExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.SelfExpression;
import org.eclipse.escet.cif.metamodel.cif.functions.InternalFunction;
import org.eclipse.escet.cif.metamodel.java.CifConstructors;
import org.eclipse.escet.cif.metamodel.java.CifWalker;
import org.eclipse.escet.cif.metamodel.java.CifWithArgWalker;
import org.eclipse.escet.common.emf.EMFHelper;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.Sets;
import org.eclipse.escet.common.position.metamodel.position.PositionObject;

/**
 * In-place transformation that replaces state invariants by extra initialization predicates and guards on edges.
 *
 * <p>
 * This transformation has several preconditions. The following are not supported:
 * </p>
 * <ul>
 * <li>Specifications with component definitions/instantiations.</li>
 * <li>Specifications with SVG input mappings with updates.</li>
 * <li>Specifications with multi-assignments (tuple addressables)</li>.
 * <li>Specifications with partial-variable assignments (projected addressables).</li>
 * <li>Specifications with time-dependent state invariants.</li>
 * <li>Specifications with state invariants that depend on the values of input variables.</li>
 * <li>Specifications with state invariants that directly or indirectly contain an automaton ('self') reference.</li>
 * </ul>
 */
public class ElimStateInvs implements CifToCifTransformation {
    /** The state objects collector to use. */
    private final StateObjectsCollector stateObjectsCollector = new StateObjectsCollector();

    /** The state objects replacer to use. */
    private final StateObjectsReplacer stateObjectsReplacer = new StateObjectsReplacer(stateObjectsCollector);

    /** The initial values obtainer to use. */
    private final InitialValuesObtainer initialValuesObtainer = new InitialValuesObtainer();

    /** The supervisory kinds of state invariant to transform. */
    private final Set<SupKind> stateInvKindsToTransform;

    /** Constructor for the {@link ElimStateInvs} class. Eliminates all state invariants. */
    public ElimStateInvs() {
        this(EnumSet.allOf(SupKind.class));
    }

    /**
     * Constructor for the {@link ElimStateInvs} class.
     *
     * @param stateInvKindsToTransform The supervisory kinds of state invariant to transform.
     */
    public ElimStateInvs(Set<SupKind> stateInvKindsToTransform) {
        this.stateInvKindsToTransform = Collections.unmodifiableSet(stateInvKindsToTransform);
    }

    @Override
    public void transform(Specification spec) {
        // Check 'no component definition/instantiation' precondition.
        if (CifScopeUtils.hasCompDefInst(spec)) {
            throw new CifToCifPreconditionException("Eliminating state invariants for a CIF specification with " +
                    "component definitions is currently not supported.");
        }

        // Check 'no SVG input mappings with updates' precondition.
        for (IoDecl ioDecl: CifCollectUtils.collectIoDeclarations(spec, list())) {
            if (ioDecl instanceof SvgIn svgIn && !svgIn.getUpdates().isEmpty()) {
                throw new CifToCifPreconditionException("Eliminating state invariants for a CIF specification with SVG "
                        + "input mappings with updates is currently not supported.");
            }
        }

        // Check 'no multi-assignments and partial-variable assignments' precondition.
        List<Automaton> automata = CifCollectUtils.collectAutomata(spec, list());
        for (Automaton aut: automata) {
            for (Location loc: aut.getLocations()) {
                for (Edge edge: loc.getEdges()) {
                    if (CifUpdateUtils.hasTupleAddressable(edge.getUpdates())) {
                        throw new CifToCifPreconditionException("Eliminating state invariants for a CIF specification "
                                + "with multi-assignments on edges is currently not supported.");
                    }
                    if (CifUpdateUtils.hasProjectedAddressable(edge.getUpdates())) {
                        throw new CifToCifPreconditionException("Eliminating state invariants for a CIF specification "
                                + "with partial-variable assignments on edges is currently not supported.");
                    }
                }
            }
        }

        // Check 'no time/input-variable dependent state invariants' precondition.
        List<Invariant> stateInvs = CifCollectUtils.collectInvariants(spec, list());
        stateInvs = stateInvs.stream().filter(i -> i.getInvKind() == InvKind.STATE).toList();
        for (Invariant stateInv: stateInvs) {
            if (!CifValueUtils.isTimeConstant(stateInv.getPredicate(), true)) {
                throw new CifToCifPreconditionException("Eliminating state invariants for a CIF specification with "
                        + "time-dependent state invariants is currently not supported.");
            }
            if (!CifValueUtils.isTimeConstant(stateInv.getPredicate(), false)) {
                throw new CifToCifPreconditionException("Eliminating state invariants for a CIF specification with "
                        + "state invariants that rely on the values of input variables is currently not supported.");
            }
        }

        // Check 'no automaton (self) references in state invariants' precondition.
        for (Invariant stateInv: stateInvs) {
            Predicate<Expression> isAutRef = expr -> expr instanceof SelfExpression
                    || (expr instanceof ComponentExpression cexpr && CifTypeUtils.isAutRefExpr(cexpr));
            if (CifValueUtils.hasSubExprSatisfyingPred(stateInv.getPredicate(), isAutRef)) {
                throw new CifToCifPreconditionException("Eliminating state invariants for a CIF specification with "
                        + "state invariants that directly or indirectly contain a component (self) reference is "
                        + "currently not supported.");
            }
        }

        // Consider only state invariants with requested supervisory kinds.
        stateInvs = stateInvs.stream().filter(i -> stateInvKindsToTransform.contains(i.getSupKind())).toList();

        // Get initial values of state objects, caching them for later use. We do this before transforming the
        // specification, since we may later add additional initialization, which could further restrict the initial
        // locations of automata. Note that it is OK to do this upfront, and ignore the extra initialization predicates,
        // since they can only further restrict initialization. If a variable or automata has a single potential initial
        // value, it can't get more values. If no value is allowed, the specification has no initial state, and further
        // restrictions will not change that.
        List<DiscVariable> discVars = CifCollectUtils.collectDiscVariables(spec, list());
        List<ContVariable> contVars = CifCollectUtils.collectContVariables(spec, list());
        for (PositionObject stateObj: concat(discVars, contVars, automata)) {
            initialValuesObtainer.get(stateObj);
        }

        // Transform the specification for each state invariant.
        for (Invariant stateInv: stateInvs) {
            // If the state invariants is contained in a location, adapt it to be in the location's automaton, such
            // that we can treat it like invariants in components.
            if (stateInv.eContainer() instanceof Location) {
                adaptLocStateInv(stateInv);
            }

            // Add an extra initialization predicate for the state invariant.
            addInit(spec, stateInv);

            // Add extra edge guards to replace the state invariant.
            addEdgeGuards(automata, stateInv);

            // Remove the state invariant from the specification.
            EMFHelper.removeFromParentContainment(stateInv);
        }
    }

    /**
     * Adapt a given state invariant that is contained in a location, such that we can treat it like invariants in
     * components:
     *
     * <ul>
     * <li>If the location's automaton has more then one location, adapt the state invariant's predicate to be
     * conditional on being in the location.</li>
     * <li>Move the state invariant to its automaton.</li>
     * </ul>
     *
     * <p>
     * We move the state invariant out of the location, since keeping state invariants in locations can cause various
     * issues, if we then also add the extra initialization predicates for state invariants to those locations:
     * </p>
     *
     * <ul>
     * <li>If a location has no initialization predicates it implicitly has initialization predicate 'false'. If we add
     * additional initialization predicates, we get their conjunction, which may no longer be 'false'. This would mean
     * the location could becomes a potentially-initial location, rather than not being a potentially-initial location.
     * The location could thus becomes <em>more</em> initial, rather than <em>less</em> initial. Since state invariants
     * can only restrict behavior, this could mean we eliminate state invariants in the wrong way.</li>
     * <li>Adding certain invariants as initialization predicates to a location may cause the initialization of the
     * automaton to be recursively defined in terms of itself. For instance, this may occur when the invariant includes
     * a 'switch' expression over the same automaton (which are currently not supported by this transformation). Such
     * recursive definition leads to the type checker considering the model invalid. We must not produce invalid models
     * as output of this transformation.</li>
     * </ul>
     *
     * <p>
     * Therefore, we move a state invariant out of its location. This requires adapting it, in case there are multiple
     * locations in the automaton, to ensure the state invariant only applies in case its location is the current
     * location. The invariant can then be treated the same as any other invariant in a component, making sure we don't
     * have to treat invariants in locations as special cases.
     * </p>
     *
     * @param locStateInv The state invariant contained in a location. Is modified in-place (if applicable) and moved.
     */
    private void adaptLocStateInv(Invariant locStateInv) {
        // Get location.
        Location loc = (Location)locStateInv.eContainer();

        // If the automaton has more than one location, adapt the state invariant's predicate to be conditional on being
        // in the location. That is, transform "location <loc>: inv <pred>;" to "location <loc>: inv <loc> => <pred>;".
        Automaton aut = CifLocationUtils.getAutomaton(loc);
        if (aut.getLocations().size() > 1) {
            BinaryExpression implExpr = CifConstructors.newBinaryExpression();
            implExpr.setOperator(BinaryOperator.IMPLICATION);
            implExpr.setLeft(CifConstructors.newLocationExpression(loc, null, CifConstructors.newBoolType()));
            implExpr.setRight(locStateInv.getPredicate());
            implExpr.setType(CifConstructors.newBoolType());
            locStateInv.setPredicate(implExpr);
        }

        // Move the state invariant to its automaton.
        aut.getInvariants().add(locStateInv);
    }

    /**
     * Add an extra initialization predicate to the specification for the given state invariant.
     *
     * @param spec The specification that contains the state invariant. Is modified in-place.
     * @param stateInv The state invariant.
     */
    private void addInit(Specification spec, Invariant stateInv) {
        // Get state objects that directly or indirectly occur in the given state invariant.
        Set<PositionObject> occurringObjs = stateObjectsCollector.collect(stateInv.getPredicate());

        // Get initial values replacements for the occurring state objects.
        Map<PositionObject, Expression> replacements = mapc(occurringObjs.size());
        for (PositionObject occurringObj: occurringObjs) {
            Expression initValue = initialValuesObtainer.get(occurringObj);
            if (initValue != null) {
                replacements.put(occurringObj, initValue);
            }
        }

        // Perform replacements, if there is anything to replace.
        Expression replacedPred = deepclone(stateInv.getPredicate());
        if (!occurringObjs.isEmpty()) {
            replacedPred = stateObjectsReplacer.replace(replacedPred, replacements);
        }

        // If the state invariant is trivially 'true' in the initial state, skip it.
        if (CifValueUtils.isTriviallyTrue(replacedPred, true, true)) {
            return;
        }

        // Simplify the state invariant as much as possible.
        replacedPred = new SimplifyValues().transform(replacedPred);

        // Add the simplified state invariant as an extra initialization predicate, in the same component as where the
        // invariant is contained.
        ComplexComponent parent = (ComplexComponent)stateInv.eContainer();
        parent.getInitials().add(replacedPred);
    }

    /**
     * Add extra edge guards to the specification for the given state invariant.
     *
     * @param automata The automata of the specification. They are modified in-place.
     * @param stateInv The state invariant.
     */
    private void addEdgeGuards(List<Automaton> automata, Invariant stateInv) {
        for (Automaton aut: automata) {
            for (Location loc: aut.getLocations()) {
                for (Edge edge: loc.getEdges()) {
                    addEdgeGuards(edge, aut, stateInv);
                }
            }
        }
    }

    /**
     * Add extra edge guards to the specification for the given state invariant.
     *
     * @param edge The edge. Is modified in-place.
     * @param aut The automaton that contains the edge.
     * @param stateInv The state invariant.
     */
    private void addEdgeGuards(Edge edge, Automaton aut, Invariant stateInv) {
        // Get for each potentially-updated variable, its new value as a single expression.
        Map<Declaration, Expression> varToNewValueExpr;
        try {
            varToNewValueExpr = CifUpdateUtils.updatesToNewValueExprPerVar(edge.getUpdates());
        } catch (UnsupportedUpdateException e) {
            throw new AssertionError("Precondition violation.");
        }

        // Get for each potentially-updated state object, its new value as a single expression. Besides variables, this
        // also contains the automaton that contains the edge, together with its possibly-updated location. We don't
        // add the automaton if it only has a single location, since then the location is always the current location.
        Map<PositionObject, Expression> objToNewValueExpr = copy(varToNewValueExpr);
        Location targetLoc = CifEdgeUtils.getTarget(edge);
        if (aut.getLocations().size() > 1) {
            Expression targetRef = CifConstructors.newLocationExpression(targetLoc, null,
                    CifConstructors.newBoolType());
            objToNewValueExpr.put(aut, targetRef);
        }

        // Check which potentially-updated objects occur directly or indirectly in the state invariant predicate. If
        // none of them occur, this edge has no effect on the state invariant and can be skipped.
        Set<PositionObject> occurringObjs = stateObjectsCollector.collect(stateInv.getPredicate());
        if (Sets.isEmptyIntersection(occurringObjs, objToNewValueExpr.keySet())) {
            return;
        }

        // Create additional edge guard, by replacing updated variables in the state invariant by their new value
        // expressions. Derivatives and algebraic variables are recursively inlined if needed.
        Expression extraGuard = deepclone(stateInv.getPredicate());
        extraGuard = stateObjectsReplacer.replace(extraGuard, objToNewValueExpr);

        // If the additional edge guard is trivially 'true', skip it.
        if (CifValueUtils.isTriviallyTrue(extraGuard, false, true)) {
            return;
        }

        // Simplify the additional edge guard as much as possible.
        extraGuard = new SimplifyValues().transform(extraGuard);

        // Add the simplified extra guard.
        edge.getGuards().add(extraGuard);
    }

    /** Initial values obtainer. */
    private static class InitialValuesObtainer {
        /** Expression to denote 'no value. */
        private static final Expression NO_VALUE = CifValueUtils.makeFalse();

        /**
         * Cached mapping from state objects to their single initial values. Is extended upon request. For state objects
         * that do not have a single value in the initial state, {@link #NO_VALUE} may be used. Outside this class,
         * always use {@link #get} rather than accessing this field directly.
         */
        private final Map<PositionObject, Expression> cache = map();

        /**
         * Get the single initial value of the given state object. If the state object does not have a single initial
         * value, {@code null} is returned.
         *
         * @param stateObj The state object.
         * @return The single initial value of the state object, or {@code null}.
         */
        Expression get(PositionObject stateObj) {
            // Check cache.
            Expression initValue = cache.get(stateObj);
            if (initValue != null) {
                return (initValue == NO_VALUE) ? null : initValue;
            }

            // State object is not yet in the cache. Get its single initial value.
            if (stateObj instanceof DiscVariable discVar) {
                if (discVar.getValue() == null) {
                    // The initial value is not explicitly defined: use the implicit initial value.
                    List<InternalFunction> funcs = listc(0); // Optimized for no functions being created.
                    Expression defaultValue = CifValueUtils.getDefaultValue(discVar.getType(), funcs);
                    if (funcs.isEmpty()) {
                        initValue = defaultValue;
                    }
                } else if (discVar.getValue().getValues().size() == 1) {
                    // Variable has a single explicit initial value: use it.
                    initValue = discVar.getValue().getValues().get(0);
                }
            } else if (stateObj instanceof ContVariable contVar) {
                if (contVar.getValue() == null) {
                    // The initial value is not explicitly defined: use the implicit initial value.
                    initValue = CifValueUtils.makeReal(0);
                } else {
                    // Use the variable's explicit initial value.
                    initValue = contVar.getValue();
                }
            } else if (stateObj instanceof Automaton aut) {
                // Determine which location is the statically-determinable single initial location.
                for (Location loc: aut.getLocations()) {
                    // If location is nameless (which is then the only location), then we indicate we don't know, since
                    // we don't want a replacement to be an invalid reference expression (you can't refer to a nameless
                    // location).
                    if (loc.getName() == null) {
                        initValue = null;
                        break;
                    }

                    // Skip locations that are definitely not initial locations.
                    List<Expression> initPreds = loc.getInitials();
                    boolean isNotInitLoc = initPreds.isEmpty() || CifValueUtils.isTriviallyFalse(initPreds, true, true);
                    if (isNotInitLoc) {
                        continue;
                    }

                    // If location is definitely an initial location, update the single initial value.
                    boolean isInitLoc = !initPreds.isEmpty() && CifValueUtils.isTriviallyTrue(initPreds, true, true);
                    if (isInitLoc) {
                        if (initValue == null) {
                            // First initial location.
                            initValue = CifConstructors.newLocationExpression(loc, null, CifConstructors.newBoolType());
                            continue;
                        } else {
                            // Multiple initial locations.
                            initValue = null;
                            break;
                        }
                    }

                    // Location is potentially an initial location. We can't decide this statically.
                    initValue = null;
                    break;
                }
            } else {
                throw new AssertionError("Unexpected state object: " + stateObj);
            }

            // If no single initial value can't be determined, use the 'no value' expression.
            if (initValue == null) {
                initValue = NO_VALUE;
            }

            // Store the initial value in the cache.
            cache.put(stateObj, initValue);

            // Return the single initial value, if the state object has one.
            return (initValue == NO_VALUE) ? null : initValue;
        }
    }

    /** Collector that collects state objects that directly or indirectly occur in a given expression. */
    private static class StateObjectsCollector extends CifWithArgWalker<Set<PositionObject>> {
        /** Cache for derivatives of continuous variables to their state objects. */
        private Map<ContVariable, Set<PositionObject>> derivCache = map();

        /** Cache for algebraic variables to their state objects. */
        private Map<AlgVariable, Set<PositionObject>> algCache = map();

        /**
         * Collect the state objects that directly or indirectly occur in the given expression.
         *
         * @param expr The given expression.
         * @return The collected objects.
         */
        Set<PositionObject> collect(Expression expr) {
            Set<PositionObject> objects = setc(1); // Optimize for very few state objects.
            walkExpression(expr, objects);
            return objects;
        }

        @Override
        protected void preprocessLocationExpression(LocationExpression locRef, Set<PositionObject> objects) {
            Location loc = locRef.getLocation();
            Automaton aut = CifLocationUtils.getAutomaton(loc);
            objects.add(aut);
        }

        @Override
        protected void preprocessDiscVariableExpression(DiscVariableExpression varRef, Set<PositionObject> objects) {
            // We don't use the collector in a context where we can have function parameters/variables.
            objects.add(varRef.getVariable());
        }

        @Override
        protected void preprocessContVariableExpression(ContVariableExpression varRef, Set<PositionObject> objects) {
            if (varRef.isDerivative()) {
                ContVariable contVar = varRef.getVariable();
                Set<PositionObject> derivObjs = derivCache.get(contVar);
                if (derivObjs == null) {
                    derivObjs = setc(1); // Optimize for very few state objects.
                    Expression deriv = CifEquationUtils.getSingleDerivativeForContVar(contVar);
                    walkExpression(deriv, derivObjs);
                    derivCache.put(contVar, derivObjs);
                }
                objects.addAll(derivObjs);
            } else {
                objects.add(varRef.getVariable());
            }
        }

        @Override
        protected void walkAlgVariableExpression(AlgVariableExpression varRef, Set<PositionObject> objects) {
            AlgVariable algVar = varRef.getVariable();
            Set<PositionObject> algObjs = algCache.get(algVar);
            if (algObjs == null) {
                algObjs = setc(1); // Optimize for very few state objects.
                Expression value = CifEquationUtils.getSingleValueForAlgVar(algVar);
                walkExpression(value, algObjs);
                algCache.put(algVar, algObjs);
            }
            objects.addAll(algObjs);
        }

        @Override
        protected void preprocessComponentExpression(ComponentExpression compRef, Set<PositionObject> objects) {
            throw new AssertionError("Precondition violation: " + compRef);
        }

        @Override
        protected void preprocessSelfExpression(SelfExpression selfRef, Set<PositionObject> objects) {
            throw new AssertionError("Precondition violation: " + selfRef);
        }
    }

    /** Replacer that replaces state objects that directly or indirectly occur in a given expression. */
    private static class StateObjectsReplacer extends CifWalker {
        /** Reusable dummy invariant, used to hold expressions being replaced. */
        private final Invariant dummyInv = CifConstructors.newInvariant();

        /** The state objects collector to use. */
        private final StateObjectsCollector stateObjectsCollector;

        /** Per object that is to be replaced, the value expression by which to replace it. */
        private Map<PositionObject, Expression> replacements;

        /**
         * Constructor for the {@link StateObjectsReplacer} class.
         *
         * @param stateObjectsCollector The state objects collector to use.
         */
        StateObjectsReplacer(StateObjectsCollector stateObjectsCollector) {
            this.stateObjectsCollector = stateObjectsCollector;
        }

        /**
         * Replace the given state objects by their value expressions. Derivatives and algebraic variables are
         * recursively inlined if needed.
         *
         * @param expr The given expression.
         * @param replacements Per object that is to be replaced, the value expression by which to replace it.
         * @return The resulting expression.
         */
        Expression replace(Expression expr, Map<PositionObject, Expression> replacements) {
            // Store the replacements for later use.
            this.replacements = replacements;

            // Put the expression in a known parent, such that if is completely replaced, we can find it back.
            dummyInv.setPredicate(expr);

            // Perform replacement.
            walkExpression(expr);
            Expression rslt = dummyInv.getPredicate();

            // Cleanup.
            dummyInv.setPredicate(null);
            this.replacements = null;

            // Return replacement result.
            Assert.notNull(rslt);
            return rslt;
        }

        @Override
        protected void preprocessLocationExpression(LocationExpression locRef) {
            // Replace the location reference only if the referred location's automaton is in the replacement mapping.
            Location loc = locRef.getLocation();
            Automaton aut = CifLocationUtils.getAutomaton(loc);
            Expression replacement = replacements.get(aut);
            if (replacement != null) {
                // Replace the location reference by 'true' or 'false'.
                Location replacementLoc = ((LocationExpression)replacement).getLocation();
                EMFHelper.updateParentContainment(locRef, CifValueUtils.makeBool(loc == replacementLoc));
            }
        }

        @Override
        protected void preprocessDiscVariableExpression(DiscVariableExpression varRef) {
            // Replace the discrete variable reference only if the referred variable is in the replacement mapping.
            // We don't use the replacer in a context where we can have function parameters/variables.
            DiscVariable discVar = varRef.getVariable();
            Expression replacement = replacements.get(discVar);
            if (replacement != null) {
                // Replace the discrete variable reference by its replacement. We deepclone the replacement, since the
                // same variable may be replaced multiple times.
                EMFHelper.updateParentContainment(varRef, deepclone(replacement));
            }
        }

        @Override
        protected void preprocessContVariableExpression(ContVariableExpression varRef) {
            if (varRef.isDerivative()) {
                // Skip this reference if the derivative of the continuous variable doesn't directly or indirectly
                // contain any of the state objects we're replacing.
                ContVariable contVar = varRef.getVariable();
                Set<PositionObject> derivStateObjs = stateObjectsCollector.collect(varRef);
                if (Sets.isEmptyIntersection(derivStateObjs, replacements.keySet())) {
                    return;
                }

                // Inline the derivative of the continuous variable.
                Expression deriv = deepclone(CifEquationUtils.getSingleDerivativeForContVar(contVar));
                EMFHelper.updateParentContainment(varRef, deriv);

                // Recursively perform replacements.
                walkExpression(deriv);
            } else {
                // Replace the continuous variable reference only if the referred variable is in the replacement
                // mapping.
                ContVariable contVar = varRef.getVariable();
                Expression replacement = replacements.get(contVar);
                if (replacement != null) {
                    // Replace the continuous variable reference by its replacement. We deepclone the replacement, since
                    // the same variable may be replaced multiple times.
                    EMFHelper.updateParentContainment(varRef, deepclone(replacement));
                }
            }
        }

        @Override
        protected void walkAlgVariableExpression(AlgVariableExpression varRef) {
            // Skip this reference if the algebraic variable doesn't directly or indirectly contain any of the state
            // objects we're replacing.
            AlgVariable algVar = varRef.getVariable();
            Set<PositionObject> algStateObjs = stateObjectsCollector.collect(varRef);
            if (Sets.isEmptyIntersection(algStateObjs, replacements.keySet())) {
                return;
            }

            // Inline the value of the algebraic variable.
            Expression value = deepclone(CifEquationUtils.getSingleValueForAlgVar(algVar));
            EMFHelper.updateParentContainment(varRef, value);

            // Recursively perform replacements.
            walkExpression(value);
        }

        @Override
        protected void preprocessComponentExpression(ComponentExpression compRef) {
            throw new AssertionError("Precondition violation: " + compRef);
        }

        @Override
        protected void preprocessSelfExpression(SelfExpression selfRef) {
            throw new AssertionError("Precondition violation: " + selfRef);
        }
    }
}
