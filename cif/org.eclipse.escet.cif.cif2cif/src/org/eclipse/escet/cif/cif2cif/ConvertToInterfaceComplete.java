//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.cif2cif;

/**
 * In-place transformation that converts the specification to its interface. It generates a
 * {@link ConvertToInterface.InterfaceKind#COMPLETE} interface.
 */
public class ConvertToInterfaceComplete extends ConvertToInterface {
    /** Constructor for the {@link ConvertToInterfaceComplete} class. */
    public ConvertToInterfaceComplete() {
        super(InterfaceKind.COMPLETE);
    }
}
