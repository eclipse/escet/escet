//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.common;

import java.util.List;

import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;

/** Utility methods related to the execution scheme prescribed by the CIF controller properties checker. */
public class CifExecSchemeUtils {
    /** Constructor for the {@link CifExecSchemeUtils} class. */
    private CifExecSchemeUtils() {
        // Static class.
    }

    /**
     * Order the given events to the order defined in the execution scheme prescribed by the CIF controller properties
     * checker.
     *
     * @param events The events to order. They are re-ordered in-place.
     * @return The events, for chaining.
     */
    public static List<Event> orderEvents(List<Event> events) {
        CifSortUtils.sortCifObjects(events);
        return events;
    }

    /**
     * Order the given automata to the order defined in the execution scheme prescribed by the CIF controller properties
     * checker.
     *
     * @param automata The automata to order. They are re-ordered in-place.
     * @return The automata, for chaining.
     */
    public static List<Automaton> orderAutomata(List<Automaton> automata) {
        CifSortUtils.sortCifObjects(automata);
        return automata;
    }
}
