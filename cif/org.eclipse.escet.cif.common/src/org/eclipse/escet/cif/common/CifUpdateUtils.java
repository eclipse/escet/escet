//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.common;

import static org.eclipse.escet.cif.metamodel.java.CifConstructors.newAssignment;
import static org.eclipse.escet.cif.metamodel.java.CifConstructors.newContVariableExpression;
import static org.eclipse.escet.cif.metamodel.java.CifConstructors.newDiscVariableExpression;
import static org.eclipse.escet.cif.metamodel.java.CifConstructors.newElifExpression;
import static org.eclipse.escet.cif.metamodel.java.CifConstructors.newIfExpression;
import static org.eclipse.escet.cif.metamodel.java.CifConstructors.newInputVariableExpression;
import static org.eclipse.escet.cif.metamodel.java.CifConstructors.newRealType;
import static org.eclipse.escet.common.emf.EMFHelper.deepclone;
import static org.eclipse.escet.common.java.Lists.listc;
import static org.eclipse.escet.common.java.Maps.mapc;
import static org.eclipse.escet.common.java.Sets.set;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.escet.cif.common.CifAddressableUtils.DuplVarAsgnException;
import org.eclipse.escet.cif.metamodel.cif.InputParameter;
import org.eclipse.escet.cif.metamodel.cif.automata.Assignment;
import org.eclipse.escet.cif.metamodel.cif.automata.ElifUpdate;
import org.eclipse.escet.cif.metamodel.cif.automata.IfUpdate;
import org.eclipse.escet.cif.metamodel.cif.automata.Update;
import org.eclipse.escet.cif.metamodel.cif.declarations.ContVariable;
import org.eclipse.escet.cif.metamodel.cif.declarations.Declaration;
import org.eclipse.escet.cif.metamodel.cif.declarations.DiscVariable;
import org.eclipse.escet.cif.metamodel.cif.declarations.InputVariable;
import org.eclipse.escet.cif.metamodel.cif.expressions.CompInstWrapExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.CompParamWrapExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.ContVariableExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.DiscVariableExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.ElifExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.Expression;
import org.eclipse.escet.cif.metamodel.cif.expressions.IfExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.InputVariableExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.ProjectionExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.TupleExpression;
import org.eclipse.escet.common.java.Assert;

/** CIF updates utility methods. */
public class CifUpdateUtils {
    /** Constructor for the {@link CifUpdateUtils} class. */
    private CifUpdateUtils() {
        // Static class.
    }

    /**
     * Does any of the given updates (recursively) contain an assignment with an addressable that (recursively) contains
     * a projected addressable?
     *
     * @param updates The given updates.
     * @return {@code true} if the updates contain a projected addressable, {@code false} otherwise.
     */
    public static boolean hasProjectedAddressable(List<Update> updates) {
        return updates.stream().anyMatch(u -> hasProjectedAddressable(u));
    }

    /**
     * Does the given update (recursively) contain an assignment with an addressable that (recursively) contains a
     * projected addressable?
     *
     * @param update The given update.
     * @return {@code true} if the update contains a projected addressable, {@code false} otherwise.
     */
    public static boolean hasProjectedAddressable(Update update) {
        return switch (update) {
            case IfUpdate ifUpd -> hasProjectedAddressable(ifUpd.getThens())
                    || ifUpd.getElifs().stream().anyMatch(elif -> hasProjectedAddressable(elif.getThens()))
                    || hasProjectedAddressable(ifUpd.getElses());
            case Assignment asgn -> CifAddressableUtils.hasProjs(asgn.getAddressable());
            default -> throw new AssertionError("Unknown update: " + update);
        };
    }

    /**
     * Does any of the given updates (recursively) contain an assignment with a tuple addressable (a multi-assignment)?
     *
     * @param updates The given updates.
     * @return {@code true} if the updates contain a tuple addressable, {@code false} otherwise.
     */
    public static boolean hasTupleAddressable(List<Update> updates) {
        return updates.stream().anyMatch(u -> hasTupleAddressable(u));
    }

    /**
     * Does the given update (recursively) contain an assignment with a tuple addressable (a multi-assignment)?
     *
     * @param update The given update.
     * @return {@code true} if the update contains a tuple addressable, {@code false} otherwise.
     */
    public static boolean hasTupleAddressable(Update update) {
        return switch (update) {
            case IfUpdate ifUpd -> hasTupleAddressable(ifUpd.getThens())
                    || ifUpd.getElifs().stream().anyMatch(elif -> hasTupleAddressable(elif.getThens()))
                    || hasTupleAddressable(ifUpd.getElses());
            case Assignment asgn -> asgn.getAddressable() instanceof TupleExpression;
            default -> throw new AssertionError("Unknown update: " + update);
        };
    }

    /**
     * Transform updates to explicit assignments per variable.
     *
     * @param updates The updates to transform.
     * @return The assignments.
     * @throws UnsupportedUpdateException If the given updates recursively contain multi-assignments, partial variable
     *     assignments and/or addressables with input parameters or component instantiation/parameter wrapping
     *     expressions.
     */
    public static List<Assignment> updatesToAssignmentsPerVar(List<Update> updates) throws UnsupportedUpdateException {
        // Get variables assigned in the updates. Also checks the addressables of assignments, to ensure they are
        // supported.
        Set<Declaration> vars = set();
        for (Update update: updates) {
            vars.addAll(getVariables(update));
        }

        // Transform the updates to explicit assignments per variable.
        List<Assignment> assignments = listc(vars.size());
        for (Declaration var: vars) {
            // Create and add assignment.
            Assignment assignment = newAssignment();
            assignments.add(assignment);

            // Set addressable.
            assignment.setAddressable(varToAddr(var));

            // Set value.
            Expression value = updatesToValue(updates, var);
            assignment.setValue(value);
        }

        return assignments;
    }

    /**
     * Transform updates to an explicit new value expression per variable.
     *
     * @param updates The updates to transform.
     * @return Per potentially-assigned variable, its new value expression.
     * @throws UnsupportedUpdateException If the given updates recursively contain multi-assignments, partial variable
     *     assignments and/or addressables with input parameters or component instantiation/parameter wrapping
     *     expressions.
     */
    public static Map<Declaration, Expression> updatesToNewValueExprPerVar(List<Update> updates)
            throws UnsupportedUpdateException
    {
        // Get variables assigned in the updates. Also checks the addressables of assignments, to ensure they are
        // supported.
        Set<Declaration> vars = set();
        for (Update update: updates) {
            vars.addAll(getVariables(update));
        }

        // Transform the updates to an explicit new value expression per variable.
        Map<Declaration, Expression> newValues = mapc(vars.size());
        for (Declaration var: vars) {
            Expression newValue = updatesToValue(updates, var);
            newValues.put(var, newValue);
        }

        return newValues;
    }

    /**
     * Returns the variables (partially) assigned in the given update.
     *
     * @param update The update.
     * @return The variables (partially) assigned in the given update.
     * @throws UnsupportedUpdateException If the given update is or recursively contains a multi-assignment , partial
     *     variable assignment and/or addressables with input parameters or component instantiation/parameter wrapping
     *     expressions.
     */
    private static Set<Declaration> getVariables(Update update) throws UnsupportedUpdateException {
        // 'if' update.
        if (update instanceof IfUpdate) {
            IfUpdate iupdate = (IfUpdate)update;
            Set<Declaration> rslt = set();
            for (Update upd: iupdate.getThens()) {
                rslt.addAll(getVariables(upd));
            }
            for (ElifUpdate elif: iupdate.getElifs()) {
                for (Update upd: elif.getThens()) {
                    rslt.addAll(getVariables(upd));
                }
            }
            for (Update upd: iupdate.getElses()) {
                rslt.addAll(getVariables(upd));
            }
            return rslt;
        }

        // Assignment. Check supported addressable, and get the variables.
        Expression addr = ((Assignment)update).getAddressable();
        checkAddressable(addr);

        try {
            return CifAddressableUtils.getRefs(addr);
        } catch (DuplVarAsgnException e) {
            // Should not occur. We can only have this if in a single multi-assignment, we assign different
            // non-overlapping parts of the same variable. Since we don't support multi-assignments and partial variable
            // assignments here, this won't occur for this method.
            throw new RuntimeException("Precondition violated.");
        }
    }

    /**
     * Creates and returns an addressable expression for the given variable.
     *
     * @param var The variable.
     * @return The newly created addressable expression for the variable.
     */
    private static Expression varToAddr(Declaration var) {
        if (var instanceof DiscVariable dvar) {
            DiscVariableExpression daddr = newDiscVariableExpression();
            daddr.setVariable(dvar);
            daddr.setType(deepclone(dvar.getType()));
            return daddr;
        } else if (var instanceof ContVariable cvar) {
            ContVariableExpression caddr = newContVariableExpression();
            caddr.setVariable(cvar);
            caddr.setType(newRealType());
            return caddr;
        } else if (var instanceof InputVariable ivar) {
            InputVariableExpression iaddr = newInputVariableExpression();
            iaddr.setVariable(ivar);
            iaddr.setType(deepclone(ivar.getType()));
            return iaddr;
        } else {
            throw new RuntimeException("Unknown variable: " + var);
        }
    }

    /**
     * Transform a sequence of updates to a value for the given variable as a result of the updates.
     *
     * @param updates The sequence of updates.
     * @param var The variable.
     * @return The value of the variable as a result of the given updates.
     * @throws UnsupportedUpdateException If the given updates recursively contain multi-assignments, partial variable
     *     assignments and/or addressables with input parameters or component instantiation/parameter wrapping
     *     expressions.
     */
    private static Expression updatesToValue(List<Update> updates, Declaration var) throws UnsupportedUpdateException {
        // Note that for a sequence of updates, at most one of them can assign any given variable. This is due to the
        // 'Edge.uniqueVariables' and 'SvgIn.uniqueVariables' constraints, which can be found in the CIF metamodel
        // documentation, together with not supporting partial variable assignments in this method.
        for (Update update: updates) {
            if (getVariables(update).contains(var)) {
                return updateToValue(update, var);
            }
        }

        // None of the updates assigned the variable, so the variable keeps its value. Note that this is not possible
        // for the top level sequence of updates, but only for sequences of updates part of an 'if' update.
        return varToAddr(var);
    }

    /**
     * Transform an update to a value for the given variable as a result of that update.
     *
     * @param update The update.
     * @param var The variable.
     * @return The value of the variable as a result of the update.
     * @throws UnsupportedUpdateException If the given update is or recursively contains a multi-assignment , partial
     *     variable assignment and/or addressables with input parameters or component instantiation/parameter wrapping
     *     expressions.
     */
    private static Expression updateToValue(Update update, Declaration var) throws UnsupportedUpdateException {
        // Assignment.
        if (update instanceof Assignment) {
            // Get addressable.
            Assignment asgn = (Assignment)update;
            Expression addr = asgn.getAddressable();

            // Return value.
            Set<Declaration> addrVars;
            try {
                addrVars = CifAddressableUtils.getRefs(addr);
            } catch (DuplVarAsgnException e) {
                // Should not occur. We can only have this if in a single multi-assignment, we assign different
                // non-overlapping parts of the same variable. Since we don't support multi-assignments and partial
                // variable assignments here, this won't occur for this method.
                throw new RuntimeException("Precondition violated.");
            }
            Assert.check(addrVars.size() == 1);
            Declaration addrVar = addrVars.iterator().next();
            if (addrVar == var) {
                return deepclone(asgn.getValue());
            } else {
                return varToAddr(var);
            }
        }

        // 'if' update.
        IfUpdate iupdate = (IfUpdate)update;

        IfExpression ifExpr = newIfExpression();
        ifExpr.setType(deepclone(varToAddr(var).getType()));

        ifExpr.getGuards().addAll(deepclone(iupdate.getGuards()));
        ifExpr.setThen(updatesToValue(iupdate.getThens(), var));

        for (ElifUpdate elif: iupdate.getElifs()) {
            ElifExpression elifExpr = newElifExpression();
            elifExpr.getGuards().addAll(deepclone(elif.getGuards()));
            elifExpr.setThen(updatesToValue(elif.getThens(), var));

            ifExpr.getElifs().add(elifExpr);
        }

        ifExpr.setElse(updatesToValue(iupdate.getElses(), var));

        return ifExpr;
    }

    /**
     * Check the addressable, to see whether it is supported. We don't support tuple addressables, projected
     * addressables, input parameter addressables, and component instantiation/parameter wrapping expressions.
     *
     * <p>
     * One of the reasons we don't support them, is that {@link CifAddressableUtils#getRefs} can't handle them properly.
     * Another reason is that is more complex to handle them. There is however no fundamental reason for this class to
     * not support them.
     * </p>
     *
     * @param addr The addressable to check.
     * @throws UnsupportedUpdateException If the given addressable is a tuple, projection, input parameter or wrapping
     *     addressable.
     */
    private static void checkAddressable(Expression addr) throws UnsupportedUpdateException {
        if (addr instanceof TupleExpression || addr instanceof ProjectionExpression
                || addr instanceof CompInstWrapExpression || addr instanceof CompParamWrapExpression)
        {
            throw new UnsupportedUpdateException();
        } else if (addr instanceof InputVariableExpression iVarExpr) {
            if (iVarExpr.getVariable().eContainer() instanceof InputParameter) {
                // Input parameter addressable.
                throw new UnsupportedUpdateException();
            }
        }
    }

    /** Exception indicating that an unsupported update was encountered. */
    public static class UnsupportedUpdateException extends Exception {
        // Nothing here.
    }
}
