//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.codegen.options;

import static org.eclipse.escet.common.java.Strings.fmt;
import static org.eclipse.escet.common.java.Triple.triple;

import org.eclipse.escet.common.app.framework.options.Options;
import org.eclipse.escet.common.app.framework.options.StringOption;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.Triple;
import org.eclipse.escet.common.java.exceptions.InvalidOptionException;

/** HTML frequencies option. */
public class HtmlFrequenciesOption extends StringOption {
    /** The minimum frequency. */
    private static final int MINIMUM_FREQUENCY = 1;

    /** The maximum frequency. */
    private static final int MAXIMUM_FREQUENCY = 100_000;

    /** The option description. */
    private static final String DESCRIPTION = fmt("The minimum, default, and maximum frequencies in number of times "
            + "per second that the JavaScript code for the model is executed in generated HTML pages. First the "
            + "minimum frequency is given (must be at least %,d), then a colon (:), then the default frequency (must "
            + "be at least the minimum frequency and at most the maximum frequency), then another colon (:), and then "
            + "the maximum frequency (must be greater than the minimum frequency, and at most %,d).", MINIMUM_FREQUENCY,
            MAXIMUM_FREQUENCY);

    /** The default value of the option. */
    private static final String DEFAULT_VALUE = "1:60:120";

    /** Constructor for the {@link HtmlFrequenciesOption} class. */
    public HtmlFrequenciesOption() {
        super(
                // name
                "HTML frequencies",

                // description
                fmt("%s [DEFAULT=%s]", DESCRIPTION, DEFAULT_VALUE),

                // cmdShort
                null,

                // cmdLong
                "html-frequencies",

                // cmdValue
                "FREQS",

                // defaultValue
                DEFAULT_VALUE,

                // emptyAsNull
                false,

                // showInDialog
                true,

                // optDialogDescr
                DESCRIPTION,

                // optDialogLabelText
                "Frequencies:");
    }

    /**
     * Returns the minimum, default, and maximum frequencies.
     *
     * @return The minimum, default, and maximum frequencies.
     */
    public static Triple<Integer, Integer, Integer> getFrequencies() {
        // Get option parts.
        String optionTxt = Options.get(HtmlFrequenciesOption.class);
        if (!optionTxt.matches("[0-9,]+:[0-9,]+:[0-9,]+")) {
            throw new InvalidOptionException(fmt("Invalid HTML frequencies \"%s\": expected three non-negative " +
                    "integer numbers separated by colons.", optionTxt));
        }
        String[] optionParts = optionTxt.replace(",", "").split(":");
        Assert.areEqual(optionParts.length, 3);

        // Get min/default/max frequencies.
        int min;
        try {
            min = Integer.parseInt(optionParts[0]);
        } catch (NumberFormatException ex) {
            throw new InvalidOptionException(fmt("Minimum HTML frequency \"%s\" can't be parsed as an integer number.",
                    optionParts[0]), ex);
        }

        int def;
        try {
            def = Integer.parseInt(optionParts[1]);
        } catch (NumberFormatException ex) {
            throw new InvalidOptionException(fmt("Default HTML frequency \"%s\" can't be parsed as an integer number.",
                    optionParts[1]), ex);
        }

        int max;
        try {
            max = Integer.parseInt(optionParts[2]);
        } catch (NumberFormatException ex) {
            throw new InvalidOptionException(fmt("Maximum HTML frequency \"%s\" can't be parsed as an integer number.",
                    optionParts[2]), ex);
        }

        // Check frequency constraints.
        if (min < MINIMUM_FREQUENCY) {
            throw new InvalidOptionException(fmt("Minimum frequency HTML \"%s\" is less than %,d.", min,
                    MINIMUM_FREQUENCY));
        }
        if (max > MAXIMUM_FREQUENCY) {
            throw new InvalidOptionException(fmt("Maximum frequency HTML \"%s\" is greater than %,d.", max,
                    MAXIMUM_FREQUENCY));
        }
        if (min >= max) {
            throw new InvalidOptionException(fmt("Minimum frequency HTML \"%s\" is greater than or equal to the "
                    + "maximum frequency (\"%s\").", min, max));
        }
        if (def < min) {
            throw new InvalidOptionException(fmt("Default frequency HTML \"%s\" is less than the minimum frequency "
                    + "\"%s\".", def, min));
        }
        if (def > max) {
            throw new InvalidOptionException(fmt("Default frequency HTML \"%s\" is greater than the maximum frequency "
                    + "(\"%s\").", def, max));
        }

        // Return min/default/max frequencies.
        return triple(min, def, max);
    }
}
