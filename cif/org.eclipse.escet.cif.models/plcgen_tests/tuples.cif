//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

enum E = L1, L2, L3;

// Tuple types of different length, and with different types.
type tbb1 = tuple(bool a, b);
type tbb2 = tuple(bool c, d);
type tbi = tuple(bool a; int b);
type trrr = tuple(real c, d, e);
type teeee = tuple(E e1, e2, e3, e4);
type ttiitrr = tuple(tuple(int a, b) x; tuple(real c, d) y);
type tcomplex = tuple(tuple(bool a, b; int c, d) x; tuple(real e1, e2; E f) y; tuple(trrr g, h) z);

// Tuple types in group, for type compatibility with ones from root of the specification.
// Some have different field names.
group g:
  type tbb1 = tuple(bool x, y);
  type tbb2 = tuple(bool f, g);
  type tbi = tuple(bool b; int a);
  type trrr = tuple(real g, h, i);
  type teeee = tuple(E e1, e2, e3, e5);
  type ttiitrr = tuple(tuple(int a, b) x; tuple(real c, d) y);
  type tcomplex = tuple(tuple(bool a, b; int c, d) x; tuple(real e1, e2; E f) y; tuple(trrr g, h) z);
end

const g.trrr const1 = (3.3, 4.4, 5.5);
alg g.trrr alg1 = (6.6, 7.7, 8.8);

automaton a:
  controllable evt;

  // Variables.
  disc bool vtrue = true;
  disc bool vfalse = false;

  disc g.tbb1 init1 = (true, false);
  disc g.tbb2 init2 = (false, true);
  disc g.tbi init3 = (true, 3);
  disc g.trrr init4 = (1.0, -2.0, 3e9);
  disc g.teeee init5 = (L1, L2, L3, L1);
  disc g.ttiitrr init6 = ((1, 2), (3.0, 4.0));
  disc g.tcomplex init7 = ((true, false, 98, 99), (55.67, 66.78, L1), ((1.1, 1.2, 1.3), (2.1, 2.2, 2.3)));

  disc tuple(bool a, b) lit1;
  disc tuple(bool c, d) lit2;
  disc tuple(bool a; int b) lit3;
  disc tuple(real c, d, e) lit4;
  disc tuple(E e1, e2, e3, e4) lit5;
  disc ttiitrr lit6;
  disc tuple(tuple(bool a, b; int c, d) x; tuple(real e1, e2; E f) y; tuple(trrr g, h) z) lit7;

  disc bool proj01, proj02, proj03, proj04, proj05, proj06, proj07, proj08;
  disc int proj09;
  disc real proj10;
  disc E proj11;
  disc real proj12, proj13, proj14, proj15, proj16, proj17, proj18, proj19;
  disc real proj20, proj21;

  disc tbb1 asgn01a;
  disc tuple(bool a, b) asgn01b;
  disc tbb2 asgn02a;
  disc tuple(bool c, d) asgn02b;
  disc tbi asgn03a;
  disc tuple(bool a; int b) asgn03b;
  disc trrr asgn04a;
  disc tuple(real c, d, e) asgn04b;
  disc teeee asgn05a;
  disc tuple(E e1, e2, e3, e4) asgn05b;
  disc ttiitrr asgn06a;
  disc tuple(tuple(int a, b) x; tuple(real c, d) y) asgn06b;
  disc tcomplex asgn07a;
  disc tuple(tuple(bool a, b; int c, d) x; tuple(real e1, e2; E f) y; tuple(trrr g, h) z) asgn07b;

  disc tuple(int p, q) asgn08a, asgn08b;
  disc tuple(real f1, f2) asgn09a, asgn09b;
  disc int asgn10a, asgn10b, asgn10c, asgn10d;
  disc real asgn11a, asgn11b, asgn11c, asgn11d;

  disc ttiitrr asgn12a, asgn12b;
  disc ttiitrr asgn13a, asgn13b;

  disc int asgn14a, asgn14b, asgn14c, asgn14d, asgn14e, asgn14f, asgn14g;
  disc tuple(int a, b) asgn14h = (8, 9);
  disc int asgn15a, asgn15b, asgn15c, asgn15d, asgn15e, asgn15f, asgn15g;
  disc tuple(int a; tuple(int b, c; tuple(int d, e) f, g) h) asgn15h = (17, (18, 19, (20, 21), (22, 23)));
  disc int asgn16a, asgn16b, asgn16c, asgn16d, asgn16e;
  disc tuple(int a, b; tuple(int c, d) e) asgn16f = (22, 23, (24, 25));
  disc int asgn17a, asgn17b;
  disc tuple(int a, b) asgn17c = (31, 32);

  disc tuple(real a, b) asgn18a, asgn18b, asgn18c, asgn18d;
  disc tuple(real a; tuple(real c, d) b) asgn18e, asgn18f;
  disc int asgn19;

  disc tuple(real a, b) asgn20a, asgn20b;
  disc int asgn21a, asgn21b, asgn21c;

  disc tuple(int a, b) asgn22 = (221, 220);
  disc tuple(int a, b) asgn23 = (240, 241);
  disc tuple(int a, b) asgn24 = (230, 231);

  disc tuple(real a, b) asgn25a, asgn25b, asgn25c, asgn25d;
  disc tuple(real a; tuple(real c, d) b) asgn25e, asgn25f;
  disc int asgn26a, asgn26b, asgn26c = 26;

  location loc1:
    initial;
    edge evt do
          // Assign literals, with different field types, and also nested.
          lit1 := (true, false),
          lit2 := (true, true),
          lit3 := (true, 9),
          lit4 := (-1.23, 2.34, 4e12),
          lit5 := (L1, L3, L2, L2),
          lit6 := ((5, 7), (12.34, 56.78)),
          lit7 := ((true, false, 5, 6), (11.22, 22.33, L3), ((1.2, 2.3, 3.4), (4.5, 5.6, 6.7))),

          // Projections of different objects, with numeric values and field names, for various types.
          proj01 := init1[0],
          proj02 := init1[1],
          proj03 := init2[0],
          proj04 := init2[1],
          proj05 := init1[x],
          proj06 := init1[y],
          proj07 := init2[f],
          proj08 := init2[g],
          proj09 := init3[1],
          proj10 := init4[h],
          proj11 := init5[3],
          proj12 := init7[2][0][1],
          proj13 := init7[2][0][g],
          proj14 := init7[2][g][1],
          proj15 := init7[2][g][g],
          proj16 := init7[z][0][1],
          proj17 := init7[z][0][g],
          proj18 := init7[z][g][1],
          proj19 := init7[z][g][g],
          proj20 := const1[2],
          proj21 := alg1[g],

          // Assign complete tuples, of different types (with type compatibility and type declarations), including
          // nested tuple types.
          asgn01a := init1,
          asgn01b := init1,
          asgn02a := init2,
          asgn02b := init2,
          asgn03a := init3,
          asgn03b := init3,
          asgn04a := init4,
          asgn04b := init4,
          asgn05a := init5,
          asgn05b := init5,
          asgn06a := init6,
          asgn06b := init6,
          asgn07a := init7,
          asgn07b := init7,

          // Assign with projection at RHS.
          asgn08a := init6[0],
          asgn08b := init6[x],
          asgn09a := init6[1],
          asgn09b := init6[y],
          asgn10a := init6[0][0],
          asgn10b := init6[x][a],
          asgn10c := init6[0][1],
          asgn10d := init6[x][b],
          asgn11a := init6[1][0],
          asgn11b := init6[y][c],
          asgn11c := init6[1][1],
          asgn11d := init6[y][d],

          // Assign inner level, with projection at LHS.
          asgn12a[0][0] := 1,
          asgn12a[0][1] := 2,
          asgn12a[1][0] := 3,
          asgn12a[1][1] := 4,
          asgn12b[x][a] := 1,
          asgn12b[x][b] := 2,
          asgn12b[y][c] := 3,
          asgn12b[y][d] := 4,

          // Assign intermediate level, with projection at LHS.
          asgn13a[0] := (1, 2),
          asgn13a[1] := (3, 4),
          asgn13b[x] := (1, 2),
          asgn13b[y] := (3, 4),

          // Multi-assignment and unfolding.
          (asgn14a, (asgn14b, asgn14c, (asgn14d, asgn14e), (asgn14f, asgn14g))) := (5, (6, 7, asgn14h, (10, 11))),
          (asgn15a, (asgn15b, asgn15c, (asgn15d, asgn15e), (asgn15f, asgn15g))) := asgn15h,
          (asgn16a, (asgn16b, asgn16c, (asgn16d, asgn16e))) := (21, asgn16f),
          (asgn17a, asgn17b) := (asgn17c[1], asgn17c[0]),

          // 'if' expression with tuple result.
          asgn18a := if vtrue: (18.0, 18.1) else (18.2, 18.3) end,
          asgn18b := if vfalse: (18.0, 18.1) else (18.2, 18.3) end,
          asgn18c := if vtrue: ((18.0, 18.1), (18.2, 18.3))[0] else ((18.0, 18.1), (18.2, 18.3))[1] end,
          asgn18d := if vfalse: ((18.0, 18.1), (18.2, 18.3))[0] else ((18.0, 18.1), (18.2, 18.3))[1] end,
          asgn18e := if vtrue: (18.0, (18.1, 18.2)) else (18.3, (18.4, 18.5)) end,
          asgn18f := if vfalse: (18.0, (18.1, 18.2)) else (18.3, (18.4, 18.5)) end,

          // 'if' expression with tuple in guard/result.
          asgn19 := if (vtrue, vfalse)[0]: (19, 99)[0] else 99 end,

          // 'switch' expression with tuple result.
          asgn20a := switch vtrue: case true: (20.0, 20.1) case false: (20.2, 20.3) end,
          asgn20b := switch vfalse: case true: (20.0, 20.1) case false: (20.2, 20.3) end,

          // 'switch' expression with tuple in switch/case value.
          asgn21a := switch (vtrue, vfalse)[0]: case (vtrue, vfalse)[0]: 21 case false: 99 else 99 end,
          asgn21b := switch (vtrue, vfalse)[1]: case (vtrue, vfalse)[0]: 99 case false: 21 else 99 end,
          asgn21c := switch (vtrue, vfalse)[1]: case (vtrue, vfalse)[0]: 99 case true: 99 else 21 end,

          // Old and new values.
          asgn22 := (asgn22[1], asgn22[0]),
          (asgn24, asgn23) := (asgn23, asgn24),

          // 'if' updates with tuple assignments.
          if vtrue:  asgn25a := (25.0, 25.1)                    else asgn25a := (25.2, 25.3) end,
          if vfalse: asgn25b := (25.0, 25.1)                    else asgn25b := (25.2, 25.3) end,
          if vtrue:  asgn25c := ((25.0, 25.1), (25.2, 25.3))[0] else asgn25c := ((25.0, 25.1), (25.2, 25.3))[1] end,
          if vfalse: asgn25d := ((25.0, 25.1), (25.2, 25.3))[0] else asgn25d := ((25.0, 25.1), (25.2, 25.3))[1] end,
          if vtrue:  asgn25e := (25.0, (25.1, 25.2))            else asgn25e := (25.3, (25.4, 25.5)) end,
          if vfalse: asgn25f := (25.0, (25.1, 25.2))            else asgn25f := (25.3, (25.4, 25.5)) end,

          // 'if' updates with tuple in guard and assigned value.
          if (vtrue, vfalse)[0]: asgn26a := (26, 99)[0] else asgn26a := 99 end,
          if (vtrue, vfalse)[1]: asgn26b := (99, 99)[0] else asgn26b := 26 end,
          if (vtrue, vfalse)[1]: asgn26c := (99, 99)[0]                    end

      goto loc2;

  location loc2;
end

// Channels with tuple values.

controllable tuple(int a, b) chan;

automaton sender:
  location loc1:
    initial;
    marked;
    edge chan!(1, 2) goto loc2;

  location loc2;
end

automaton receiver:
  disc tuple(int c, d) v;

  location loc1:
    initial;
    marked;
    edge chan? do v := ? goto loc2;

  location loc2;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.
//   In case a '->' is given on a line, additionally accepted alternative
//   values are indicated, surrounded by single quotes (') and separated
//   by forward slashes (/).
// - If enumerations are eliminated, the value may be a number instead of
//   an enumeration literal. Depending on how the number is encoded, it
//   may be displayed differently in the PLC development environment.
// - Real numbers that are not whole numbers may have different precision and
//   rounding in the PLC. Still, their values should match, except for the
//   differences in precision (number of digits after the decimal point) and
//   rounding (last digit after the decimal point).

// ##############################################
// Expected result.
// ##############################################
//
// a_init1.field1 = true
// a_init1.field2 = false
// a_init2.field1 = false
// a_init2.field2 = true
// a_init3.field1 = true
// a_init3.field2 = 3
// a_init4.field1 = 1.0
// a_init4.field2 = -2.0
// a_init4.field3 = 3.0e9    -> '3.0e+09' / '3000000000.0'
// a_init5.field1 = L1    -> '0' / '16#00'
// a_init5.field2 = L2    -> '1' / '16#01'
// a_init5.field3 = L3    -> '2' / '16#02'
// a_init5.field4 = L1    -> '0' / '16#00'
// a_init6.field1.field1 = 1
// a_init6.field1.field2 = 2
// a_init6.field2.field1 = 3.0
// a_init6.field2.field2 = 4.0
// a_init7.field1.field1 = true
// a_init7.field1.field2 = false
// a_init7.field1.field3 = 98
// a_init7.field1.field4 = 99
// a_init7.field2.field1 = 55.67
// a_init7.field2.field2 = 66.78
// a_init7.field2.field3 = L1    -> '0' / '16#00'
// a_init7.field3.field1.field1 = 1.1
// a_init7.field3.field1.field2 = 1.2
// a_init7.field3.field1.field3 = 1.3
// a_init7.field3.field2.field1 = 2.1
// a_init7.field3.field2.field2 = 2.2
// a_init7.field3.field2.field3 = 2.3
//
// a_lit1.field1 = true
// a_lit1.field2 = false
// a_lit2.field1 = true
// a_lit2.field2 = true
// a_lit3.field1 = true
// a_lit3.field2 = 9
// a_lit4.field1 = -1.23
// a_lit4.field2 = 2.34
// a_lit4.field3 = 4.0e12    -> '4.0e+12' / '4000000000000.0'
// a_lit5.field1 = L1    -> '0' / '16#00'
// a_lit5.field2 = L3    -> '2' / '16#02'
// a_lit5.field3 = L2    -> '1' / '16#01'
// a_lit5.field4 = L2    -> '1' / '16#01'
// a_lit6.field1.field1 = 5
// a_lit6.field1.field2 = 7
// a_lit6.field2.field1 = 12.34
// a_lit6.field2.field2 = 56.78
// a_lit7.field1.field1 = true
// a_lit7.field1.field2 = false
// a_lit7.field1.field3 = 5
// a_lit7.field1.field4 = 6
// a_lit7.field2.field1 = 11.22
// a_lit7.field2.field2 = 22.33
// a_lit7.field2.field3 = L3    -> '2' / '16#02'
// a_lit7.field3.field1.field1 = 1.2
// a_lit7.field3.field1.field2 = 2.3
// a_lit7.field3.field1.field3 = 3.4
// a_lit7.field3.field2.field1 = 4.5
// a_lit7.field3.field2.field2 = 5.6
// a_lit7.field3.field2.field3 = 6.7
//
// a_proj01 = true
// a_proj02 = false
// a_proj03 = false
// a_proj04 = true
// a_proj05 = true
// a_proj06 = false
// a_proj07 = false
// a_proj08 = true
// a_proj09 = 3
// a_proj10 = -2.0
// a_proj11 = L1    -> '0' / '16#00'
// a_proj12 = 1.2
// a_proj13 = 1.1
// a_proj14 = 1.2
// a_proj15 = 1.1
// a_proj16 = 1.2
// a_proj17 = 1.1
// a_proj18 = 1.2
// a_proj19 = 1.1
// a_proj20 = 5.5
// a_proj21 = 6.6
//
// a_asgn01a.field1 = true
// a_asgn01a.field2 = false
// a_asgn01b.field1 = true
// a_asgn01b.field2 = false
// a_asgn02a.field1 = false
// a_asgn02a.field2 = true
// a_asgn02b.field1 = false
// a_asgn02b.field2 = true
// a_asgn03a.field1 = true
// a_asgn03a.field2 = 3
// a_asgn03b.field1 = true
// a_asgn03b.field2 = 3
// a_asgn04a.field1 = 1.0
// a_asgn04a.field2 = -2.0
// a_asgn04a.field3 = 3.0e9    -> '3.0e+09' / '3000000000.0'
// a_asgn04b.field1 = 1.0
// a_asgn04b.field2 = -2.0
// a_asgn04b.field3 = 3.0e9    -> '3.0e+09' / '3000000000.0'
// a_asgn05a.field1 = L1    -> '0' / '16#00'
// a_asgn05a.field2 = L2    -> '1' / '16#01'
// a_asgn05a.field3 = L3    -> '2' / '16#02'
// a_asgn05a.field4 = L1    -> '0' / '16#00'
// a_asgn05b.field1 = L1    -> '0' / '16#00'
// a_asgn05b.field2 = L2    -> '1' / '16#01'
// a_asgn05b.field3 = L3    -> '2' / '16#02'
// a_asgn05b.field4 = L1    -> '0' / '16#00'
// a_asgn06a.field1.field1 = 1
// a_asgn06a.field1.field2 = 2
// a_asgn06a.field2.field1 = 3.0
// a_asgn06a.field2.field2 = 4.0
// a_asgn06b.field1.field1 = 1
// a_asgn06b.field1.field2 = 2
// a_asgn06b.field2.field1 = 3.0
// a_asgn06b.field2.field2 = 4.0
// a_asgn07a.field1.field1 = true
// a_asgn07a.field1.field2 = false
// a_asgn07a.field1.field3 = 98
// a_asgn07a.field1.field4 = 99
// a_asgn07a.field2.field1 = 55.67
// a_asgn07a.field2.field2 = 66.78
// a_asgn07a.field2.field3 = L1    -> '0' / '16#00'
// a_asgn07a.field3.field1.field1 = 1.1
// a_asgn07a.field3.field1.field2 = 1.2
// a_asgn07a.field3.field1.field3 = 1.3
// a_asgn07a.field3.field2.field1 = 2.1
// a_asgn07a.field3.field2.field2 = 2.2
// a_asgn07a.field3.field2.field3 = 2.3
// a_asgn07b.field1.field1 = true
// a_asgn07b.field1.field2 = false
// a_asgn07b.field1.field3 = 98
// a_asgn07b.field1.field4 = 99
// a_asgn07b.field2.field1 = 55.67
// a_asgn07b.field2.field2 = 66.78
// a_asgn07b.field2.field3 = L1    -> '0' / '16#00'
// a_asgn07b.field3.field1.field1 = 1.1
// a_asgn07b.field3.field1.field2 = 1.2
// a_asgn07b.field3.field1.field3 = 1.3
// a_asgn07b.field3.field2.field1 = 2.1
// a_asgn07b.field3.field2.field2 = 2.2
// a_asgn07b.field3.field2.field3 = 2.3
//
// a_asgn08a.field1 = 1
// a_asgn08a.field2 = 2
// a_asgn08b.field1 = 1
// a_asgn08b.field2 = 2
// a_asgn09a.field1 = 3.0
// a_asgn09a.field2 = 4.0
// a_asgn09b.field1 = 3.0
// a_asgn09b.field2 = 4.0
// a_asgn10a = 1
// a_asgn10b = 1
// a_asgn10c = 2
// a_asgn10d = 2
// a_asgn11a = 3.0
// a_asgn11b = 3.0
// a_asgn11c = 4.0
// a_asgn11d = 4.0
//
// a_asgn12a.field1.field1 = 1
// a_asgn12a.field1.field2 = 2
// a_asgn12a.field2.field1 = 3.0
// a_asgn12a.field2.field2 = 4.0
// a_asgn12b.field1.field1 = 1
// a_asgn12b.field1.field2 = 2
// a_asgn12b.field2.field1 = 3.0
// a_asgn12b.field2.field2 = 4.0
// a_asgn13a.field1.field1 = 1
// a_asgn13a.field1.field2 = 2
// a_asgn13a.field2.field1 = 3.0
// a_asgn13a.field2.field2 = 4.0
// a_asgn13b.field1.field1 = 1
// a_asgn13b.field1.field2 = 2
// a_asgn13b.field2.field1 = 3.0
// a_asgn13b.field2.field2 = 4.0
//
// a_asgn14a = 5
// a_asgn14b = 6
// a_asgn14c = 7
// a_asgn14d = 8
// a_asgn14e = 9
// a_asgn14f = 10
// a_asgn14g = 11
// a_asgn14h.field1 = 8
// a_asgn14h.field2 = 9
//
// a_asgn15a = 17
// a_asgn15b = 18
// a_asgn15c = 19
// a_asgn15d = 20
// a_asgn15e = 21
// a_asgn15f = 22
// a_asgn15g = 23
// a_asgn15h.field1 = 17
// a_asgn15h.field2.field1 = 18
// a_asgn15h.field2.field2 = 19
// a_asgn15h.field2.field3.field1 = 20
// a_asgn15h.field2.field3.field2 = 21
// a_asgn15h.field2.field4.field1 = 22
// a_asgn15h.field2.field4.field2 = 23
//
// a_asgn16a = 21
// a_asgn16b = 22
// a_asgn16c = 23
// a_asgn16d = 24
// a_asgn16e = 25
// a_asgn16f.field1 = 22
// a_asgn16f.field2 = 23
// a_asgn16f.field3.field1 = 24
// a_asgn16f.field3.field2 = 25
//
// a_asgn17a = 32
// a_asgn17b = 31
// a_asgn17c.field1 = 31
// a_asgn17c.field2 = 32
//
// a_asgn18a.field1 = 18.0
// a_asgn18a.field2 = 18.1
// a_asgn18b.field1 = 18.2
// a_asgn18b.field2 = 18.3
// a_asgn18c.field1 = 18.0
// a_asgn18c.field2 = 18.1
// a_asgn18d.field1 = 18.2
// a_asgn18d.field2 = 18.3
// a_asgn18e.field1 = 18.0
// a_asgn18e.field2.field1 = 18.1
// a_asgn18e.field2.field2 = 18.2
// a_asgn18f.field1 = 18.3
// a_asgn18f.field2.field1 = 18.4
// a_asgn18f.field2.field2 = 18.5
//
// a_asgn19 = 19
//
// a_asgn20a.field1 = 20.0
// a_asgn20a.field2 = 20.1
// a_asgn20b.field1 = 20.2
// a_asgn20b.field2 = 20.3
//
// a_asgn21a = 21
// a_asgn21b = 21
// a_asgn21c = 21
//
// a_asgn22.field1 = 220
// a_asgn22.field2 = 221
// a_asgn23.field1 = 230
// a_asgn23.field2 = 231
// a_asgn24.field1 = 240
// a_asgn24.field2 = 241
//
// a_asgn25a.field1 = 25.0
// a_asgn25a.field2 = 25.1
// a_asgn25b.field1 = 25.2
// a_asgn25b.field2 = 25.3
// a_asgn25c.field1 = 25.0
// a_asgn25c.field2 = 25.1
// a_asgn25d.field1 = 25.2
// a_asgn25d.field2 = 25.3
// a_asgn25e.field1 = 25.0
// a_asgn25e.field2.field1 = 25.1
// a_asgn25e.field2.field2 = 25.2
// a_asgn25f.field1 = 25.3
// a_asgn25f.field2.field1 = 25.4
// a_asgn25f.field2.field2 = 25.5
//
// a_asgn26a = 26
// a_asgn26b = 26
// a_asgn26c = 26
//
// receiver = receiver_loc2    -> 'a_loc2' / '1' / 'true'
// receiver_v.field1 = 1
// receiver_v.field2 = 2
// sender = sender_loc2    -> 'a_loc2' / '1' / 'true'
