//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

plant a:
  controllable e;
  disc int c;

  location:
    initial;
    edge e when c < 50 * 1000 do c := c + 1;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - It may take some time for the value of 'loopsExhausted' to reach its
//   final value. This depends on the number of cycles that are executed per
//   second. Once the variable reaches its expected value, it should remain
//   unchanged at that value.
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.
//   In case a '->' is given on a line, additionally accepted alternative
//   values are indicated, surrounded by single quotes (') and separated
//   by forward slashes (/).
// - Large integer numbers may have '_' characters as thousand separators.
//   These may be ignored.

// ##############################################
// Expected result.
// ##############################################
//
// loopsExhausted = 9999    -> '9_999'
