//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

automaton a:
  controllable evt;

  // Values that are not statically evaluable.
  disc bool vtrue = true;
  disc bool vfalse = false;

  disc int[-3..3] vneg3 = -3;
  disc int[-3..3] vzero = 0;
  disc int[-3..3] vpos3 = 3;

  disc real vrneg3 = -3.0;
  disc real vrzero = 0.0;
  disc real vrpos3 = 3.0;

  // Variables.
  disc bool inverse1, inverse2, inverse3;
  disc int[-3..3] negate01, negate02, negate03;
  disc int negate04, negate05, negate06;
  disc real negate07, negate08, negate09;
  disc int[-3..3] negate11, negate12, negate13;
  disc int negate14, negate15, negate16;
  disc real negate17, negate18, negate19;
  disc int[-3..3] plus01, plus02, plus03;
  disc int plus04, plus05, plus06;
  disc real plus07, plus08, plus09;
  disc int[-3..3] plus11, plus12, plus13;
  disc int plus14, plus15, plus16;
  disc real plus17, plus18, plus19;

  location loc1:
    initial;
    edge evt do
          // not
          inverse1 := not vtrue,
          inverse2 := not vfalse,
          inverse3 := not not vfalse,

          // -
          negate01 := -vneg3,
          negate02 := -vzero,
          negate03 := -vpos3,
          negate04 := -vneg3,
          negate05 := -vzero,
          negate06 := -vpos3,
          negate07 := -vrneg3,
          negate08 := -vrzero,
          negate09 := -vrpos3,

          negate11 := --vneg3,
          negate12 := --vzero,
          negate13 := --vpos3,
          negate14 := --vneg3,
          negate15 := --vzero,
          negate16 := --vpos3,
          negate17 := --vrneg3,
          negate18 := --vrzero,
          negate19 := --vrpos3,

          // +
          plus01 := +vneg3,
          plus02 := +vzero,
          plus03 := +vpos3,
          plus04 := +vneg3,
          plus05 := +vzero,
          plus06 := +vpos3,
          plus07 := +vrneg3,
          plus08 := +vrzero,
          plus09 := +vrpos3,

          plus11 := ++vneg3,
          plus12 := ++vzero,
          plus13 := ++vpos3,
          plus14 := ++vneg3,
          plus15 := ++vzero,
          plus16 := ++vpos3,
          plus17 := ++vrneg3,
          plus18 := ++vrzero,
          plus19 := ++vrpos3

      goto loc2;

    location loc2;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.

// ##############################################
// Expected result.
// ##############################################
//
// a_inverse1 = false
// a_inverse2 = true
// a_inverse3 = false
//
// a_negate01 = 3
// a_negate02 = 0
// a_negate03 = -3
// a_negate04 = 3
// a_negate05 = 0
// a_negate06 = -3
// a_negate07 = 3.0
// a_negate08 = 0.0
// a_negate09 = -3.0
//
// a_negate11 = -3
// a_negate12 = 0
// a_negate13 = 3
// a_negate14 = -3
// a_negate15 = 0
// a_negate16 = 3
// a_negate17 = -3.0
// a_negate18 = 0.0
// a_negate19 = 3.0
//
// a_plus01 = -3
// a_plus02 = 0
// a_plus03 = 3
// a_plus04 = -3
// a_plus05 = 0
// a_plus06 = 3
// a_plus07 = -3.0
// a_plus08 = 0.0
// a_plus09 = 3.0
//
// a_plus11 = -3
// a_plus12 = 0
// a_plus13 = 3
// a_plus14 = -3
// a_plus15 = 0
// a_plus16 = 3
// a_plus17 = -3.0
// a_plus18 = 0.0
// a_plus19 = 3.0
