//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

automaton a:
  controllable evt;

  // Values that are not statically evaluable.
  disc int vneg3 = -3;
  disc int vneg1 = -1;
  disc int vzero = 0;
  disc int vpos3 = 3;
  disc int vpos200 = 200;
  disc int vpos499 = 499;
  disc int vpos500 = 500;
  disc int vpos501 = 501;
  disc int vpos600 = 600;
  disc int vpos1000 = 1000;
  disc int vpos1001 = 1001;

  disc real vrneg9 = -9.0;
  disc real vrneg3 = -3.0;
  disc real vrneg1 = -1.0;
  disc real vrneg04 = -0.4;
  disc real vrneg05 = -0.5;
  disc real vrneg06 = -0.6;
  disc real vrzero = 0.0;
  disc real vrpos04 = 0.4;
  disc real vrpos05 = 0.5;
  disc real vrpos06 = 0.6;
  disc real vrone = 1.0;
  disc real vrpos3 = 3.0;
  disc real vrpos9 = 9.0;
  disc real vrpos200 = 200.0;
  disc real vrpos499 = 499.0;
  disc real vrpos500 = 500.0;
  disc real vrpos501 = 501.0;
  disc real vrpos600 = 600.0;
  disc real vrpos1000 = 1000.0;
  disc real vrpos1001 = 1001.0;

  // Variables.
  disc int abs1, abs2, abs3;
  disc real abs4, abs5, abs6;
  disc real exp1, exp2, exp3, exp4;
  disc real ln1, ln2;
  disc real log1, log2;
  disc int max1, max5;
  disc real max2, max3, max4, max6, max7;
  disc int min1, min5;
  disc real min2, min3, min4, min6, min7;
  disc real sqrt1, sqrt2, sqrt3;
  disc real acos1, acos2;
  disc real asin1, asin2;
  disc real atan1, atan2, atan3, atan4;
  disc real cos1, cos2, cos3, cos4;
  disc real sin1, sin2, sin3, sin4;
  disc real tan1, tan2, tan3, tan4;
  disc int ceil1, ceil2, ceil3, ceil4, ceil5;
  disc int ceil6, ceil7, ceil8, ceil9;
  disc int floor1, floor2, floor3, floor4, floor5;
  disc int floor6, floor7, floor8, floor9;
  disc int round1, round2, round3, round4, round5;
  disc int round6, round7, round8, round9;
  disc real scale01, scale02, scale03, scale04, scale05;
  disc real scale06, scale07, scale08, scale09, scale10;
  disc real scale11, scale12, scale13, scale14, scale15;
  disc real scale16, scale17, scale18, scale19, scale20;
  disc real scale21, scale22, scale23, scale24, scale25;
  disc real scale26, scale27, scale28, scale29, scale30;
  disc real scale31, scale32, scale33, scale34, scale35;
  disc real scale36, scale37, scale38, scale39, scale40;
  disc real scale41, scale42, scale43, scale44, scale45;
  disc real scale46;
  disc int sign1, sign2, sign3;
  disc int sign4, sign5, sign6;

  location loc1:
    initial;
    edge evt do
          // abs
          abs1 := abs(vneg3),
          abs2 := abs(vzero),
          abs3 := abs(vpos3),
          abs4 := abs(vrneg3),
          abs5 := abs(vrzero),
          abs6 := abs(vrpos3),

          // exp
          exp1 := exp(vrneg9),
          exp2 := exp(vrzero),
          exp3 := exp(vrone),
          exp4 := exp(vrpos9),

          // ln
          ln1 := ln(vrone),
          ln2 := ln(vrpos9),

          // log
          log1 := log(vrone),
          log2 := log(vrpos9),

          // max
          max1 := max(vpos3, vneg3), // int, int
          max2 := max(vpos3, vrneg3), // int, real
          max3 := max(vrpos3, vneg3), // real, int
          max4 := max(vrpos3, vrneg3), // real, real
          max5 := max(max(vpos3, vpos3), vpos3), // ints
          max6 := max(max(vrpos3, vrpos3), vrpos3), // reals
          max7 := max(max(vpos3, vrpos3), vpos3), // ints and reals

          // min
          min1 := min(vpos3, vneg3), // int, int
          min2 := min(vpos3, vrneg3), // int, real
          min3 := min(vrpos3, vneg3), // real, int
          min4 := min(vrpos3, vrneg3), // real, real
          min5 := min(min(vpos3, vpos3), vpos3), // ints
          min6 := min(min(vrpos3, vrpos3), vrpos3), // reals
          min7 := min(min(vpos3, vrpos3), vpos3), // ints and reals

          // sqrt
          sqrt1 := sqrt(vrzero),
          sqrt2 := sqrt(vrone),
          sqrt3 := sqrt(vrpos9),

          // acos
          acos1 := acos(vrzero),
          acos2 := acos(vrone),

          // asin
          asin1 := asin(vrzero),
          asin2 := asin(vrone),

          // atan
          atan1 := atan(vrneg3),
          atan2 := atan(vrzero),
          atan3 := atan(vrone),
          atan4 := atan(vrpos3),

          // cos
          cos1 := cos(vrneg3),
          cos2 := cos(vrzero),
          cos3 := cos(vrone),
          cos4 := cos(vrpos3),

          // sin
          sin1 := sin(vrneg3),
          sin2 := sin(vrzero),
          sin3 := sin(vrone),
          sin4 := sin(vrpos3),

          // tan
          tan1 := tan(vrneg3),
          tan2 := tan(vrzero),
          tan3 := tan(vrone),
          tan4 := tan(vrpos3),

          // ceil
          ceil1 := ceil(vrneg3),
          ceil2 := ceil(vrneg06),
          ceil3 := ceil(vrneg05),
          ceil4 := ceil(vrneg04),
          ceil5 := ceil(vrzero),
          ceil6 := ceil(vrpos04),
          ceil7 := ceil(vrpos05),
          ceil8 := ceil(vrpos06),
          ceil9 := ceil(vrpos3),

          // floor
          floor1 := floor(vrneg3),
          floor2 := floor(vrneg06),
          floor3 := floor(vrneg05),
          floor4 := floor(vrneg04),
          floor5 := floor(vrzero),
          floor6 := floor(vrpos04),
          floor7 := floor(vrpos05),
          floor8 := floor(vrpos06),
          floor9 := floor(vrpos3),

          // round
          round1 := round(vrneg3),
          round2 := round(vrneg06),
          round3 := round(vrneg05),
          round4 := round(vrneg04),
          round5 := round(vrzero),
          round6 := round(vrpos04),
          round7 := round(vrpos05),
          round8 := round(vrpos06),
          round9 := round(vrpos3),

          // scale
          scale01 := scale(vneg1,     vzero,  vpos1000,  vpos200,  vpos600),
          scale02 := scale(vzero,     vzero,  vpos1000,  vpos200,  vpos600),
          scale03 := scale(vpos499,   vzero,  vpos1000,  vpos200,  vpos600),
          scale04 := scale(vpos500,   vzero,  vpos1000,  vpos200,  vpos600),
          scale05 := scale(vpos501,   vzero,  vpos1000,  vpos200,  vpos600),
          scale06 := scale(vpos1000,  vzero,  vpos1000,  vpos200,  vpos600),
          scale07 := scale(vpos1001,  vzero,  vpos1000,  vpos200,  vpos600),

          scale08 := scale(vrneg1,    vrzero, vrpos1000, vrpos600, vrpos200),
          scale09 := scale(vrzero,    vrzero, vrpos1000, vrpos600, vrpos200),
          scale10 := scale(vrpos499,  vrzero, vrpos1000, vrpos600, vrpos200),
          scale11 := scale(vrpos500,  vrzero, vrpos1000, vrpos600, vrpos200),
          scale12 := scale(vrpos501,  vrzero, vrpos1000, vrpos600, vrpos200),
          scale13 := scale(vrpos1000, vrzero, vrpos1000, vrpos600, vrpos200),
          scale14 := scale(vrpos1001, vrzero, vrpos1000, vrpos600, vrpos200),

          scale15 := scale(vpos500,   vzero,  vpos1000,  vpos200,  vpos600),
          scale16 := scale(vpos500,   vzero,  vpos1000,  vpos200,  vrpos600),
          scale17 := scale(vpos500,   vzero,  vpos1000,  vrpos200, vpos600),
          scale18 := scale(vpos500,   vzero,  vpos1000,  vrpos200, vrpos600),
          scale19 := scale(vpos500,   vzero,  vrpos1000, vpos200,  vpos600),
          scale20 := scale(vpos500,   vzero,  vrpos1000, vpos200,  vrpos600),
          scale21 := scale(vpos500,   vzero,  vrpos1000, vrpos200, vpos600),
          scale22 := scale(vpos500,   vzero,  vrpos1000, vrpos200, vrpos600),

          scale23 := scale(vpos500,   vrzero, vpos1000,  vpos200,  vpos600),
          scale24 := scale(vpos500,   vrzero, vpos1000,  vpos200,  vrpos600),
          scale25 := scale(vpos500,   vrzero, vpos1000,  vrpos200, vpos600),
          scale26 := scale(vpos500,   vrzero, vpos1000,  vrpos200, vrpos600),
          scale27 := scale(vpos500,   vrzero, vrpos1000, vpos200,  vpos600),
          scale28 := scale(vpos500,   vrzero, vrpos1000, vpos200,  vrpos600),
          scale29 := scale(vpos500,   vrzero, vrpos1000, vrpos200, vpos600),
          scale30 := scale(vpos500,   vrzero, vrpos1000, vrpos200, vrpos600),

          scale31 := scale(vrpos500,  vzero,  vpos1000,  vpos200,  vpos600),
          scale32 := scale(vrpos500,  vzero,  vpos1000,  vpos200,  vrpos600),
          scale33 := scale(vrpos500,  vzero,  vpos1000,  vrpos200, vpos600),
          scale34 := scale(vrpos500,  vzero,  vpos1000,  vrpos200, vrpos600),
          scale35 := scale(vrpos500,  vzero,  vrpos1000, vpos200,  vpos600),
          scale36 := scale(vrpos500,  vzero,  vrpos1000, vpos200,  vrpos600),
          scale37 := scale(vrpos500,  vzero,  vrpos1000, vrpos200, vpos600),
          scale38 := scale(vrpos500,  vzero,  vrpos1000, vrpos200, vrpos600),

          scale39 := scale(vrpos500,  vrzero, vpos1000,  vpos200,  vpos600),
          scale40 := scale(vrpos500,  vrzero, vpos1000,  vpos200,  vrpos600),
          scale41 := scale(vrpos500,  vrzero, vpos1000,  vrpos200, vpos600),
          scale42 := scale(vrpos500,  vrzero, vpos1000,  vrpos200, vrpos600),
          scale43 := scale(vrpos500,  vrzero, vrpos1000, vpos200,  vpos600),
          scale44 := scale(vrpos500,  vrzero, vrpos1000, vpos200,  vrpos600),
          scale45 := scale(vrpos500,  vrzero, vrpos1000, vrpos200, vpos600),
          scale46 := scale(vrpos500,  vrzero, vrpos1000, vrpos200, vrpos600),

          // sign
          sign1 := sign(vneg3),
          sign2 := sign(vzero),
          sign3 := sign(vpos3),

          sign4 := sign(vrneg3),
          sign5 := sign(vrzero),
          sign6 := sign(vrpos3)

      goto loc2;

    location loc2;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.
//   In case a '->' is given on a line, additionally accepted alternative
//   values are indicated, surrounded by single quotes (') and separated
//   by forward slashes (/).
// - Real numbers that are not whole numbers may have different precision and
//   rounding in the PLC. Still, their values should match, except for the
//   differences in precision (number of digits after the decimal point) and
//   rounding (last digit after the decimal point).

// ##############################################
// Expected result.
// ##############################################
//
// a_abs1 = 3
// a_abs2 = 0
// a_abs3 = 3
// a_abs4 = 3.0
// a_abs5 = 0.0
// a_abs6 = 3.0
//
// a_exp1 = 1.2340980408667956e-4    -> '0.0001234098' / '0.00012340980408668'
// a_exp2 = 1.0
// a_exp3 = 2.718281828459045    -> '2.718282' / '2.71828182845905'
// a_exp4 = 8103.083927575384    -> '8103.081' / '8103.084' / '8103.08392757538'
//
// a_ln1 = 0.0
// a_ln2 = 2.1972245773362196    -> '2.197225' / '2.19722457733622'
//
// a_log1 = 0.0
// a_log2 = 0.9542425094393249    -> '0.9542425' / '0.954242509439325'
//
// a_max1 = 3
// a_max2 = 3.0
// a_max3 = 3.0
// a_max4 = 3.0
// a_max5 = 3
// a_max6 = 3.0
// a_max7 = 3.0
//
// a_min1 = -3
// a_min2 = -3.0
// a_min3 = -3.0
// a_min4 = -3.0
// a_min5 = 3
// a_min6 = 3.0
// a_min7 = 3.0
//
// a_sqrt1 = 0.0
// a_sqrt2 = 1.0
// a_sqrt3 = 3.0
//
// a_acos1 = 1.5707963267948966    -> '1.570796' / '1.5707963267949'
// a_acos2 = 0.0
//
// a_asin1 = 0.0
// a_asin2 = 1.5707963267948966    -> '1.570796' / '1.5707963267949'
//
// a_atan1 = -1.2490457723982544    -> '-1.249046' / '-1.24904577239825'
// a_atan2 = 0.0
// a_atan3 = 0.7853981633974483    -> '0.7853982' / '0.785398163397448'
// a_atan4 = 1.2490457723982544    -> '1.249046' / '1.24904577239825'
//
// a_cos1 = -0.9899924966004454    -> '-0.9899924' / '-0.9899925' / '-0.989992496600445'
// a_cos2 = 1.0
// a_cos3 = 0.5403023058681398    -> '0.5403023' / '0.54030230586814'
// a_cos4 = -0.9899924966004454    -> '-0.9899924' / '-0.9899925' / '-0.989992496600445'
//
// a_sin1 = -0.1411200080598672    -> '-0.14112' / '-0.1411202' / '-0.141120008059867'
// a_sin2 = 0.0
// a_sin3 = 0.8414709848078965    -> '0.841471' / '0.8414709' / '0.841470984807897'
// a_sin4 = 0.1411200080598672    -> '0.14112' / '0.1411202' / '0.141120008059867'
//
// a_tan1 = 0.1425465430742778    -> '0.1425465' / '0.142546543074278'
// a_tan2 = 0.0
// a_tan3 = 1.5574077246549023    -> '1.557408' / '1.5574077246549'
// a_tan4 = -0.1425465430742778    -> '-0.1425465' / '-0.142546543074278'
//
// a_ceil1 = -3
// a_ceil2 = 0
// a_ceil3 = 0
// a_ceil4 = 0
// a_ceil5 = 0
// a_ceil6 = 1
// a_ceil7 = 1
// a_ceil8 = 1
// a_ceil9 = 3
//
// a_floor1 = -3
// a_floor2 = -1
// a_floor3 = -1
// a_floor4 = -1
// a_floor5 = 0
// a_floor6 = 0
// a_floor7 = 0
// a_floor8 = 0
// a_floor9 = 3
//
// a_round1 = -3
// a_round2 = -1
// a_round3 = 0
// a_round4 = 0
// a_round5 = 0
// a_round6 = 0
// a_round7 = 1
// a_round8 = 1
// a_round9 = 3
//
// a_scale01 = 199.6
// a_scale02 = 200.0
// a_scale03 = 399.6
// a_scale04 = 400.0
// a_scale05 = 400.4
// a_scale06 = 600.0
// a_scale07 = 600.4
//
// a_scale08 = 600.4
// a_scale09 = 600.0
// a_scale10 = 400.4
// a_scale11 = 400.0
// a_scale12 = 399.6
// a_scale13 = 200.0
// a_scale14 = 199.6
//
// a_scale15 = 400.0
// a_scale16 = 400.0
// a_scale17 = 400.0
// a_scale18 = 400.0
// a_scale19 = 400.0
// a_scale20 = 400.0
// a_scale21 = 400.0
// a_scale22 = 400.0
//
// a_scale23 = 400.0
// a_scale24 = 400.0
// a_scale25 = 400.0
// a_scale26 = 400.0
// a_scale27 = 400.0
// a_scale28 = 400.0
// a_scale29 = 400.0
// a_scale30 = 400.0
//
// a_scale31 = 400.0
// a_scale32 = 400.0
// a_scale33 = 400.0
// a_scale34 = 400.0
// a_scale35 = 400.0
// a_scale36 = 400.0
// a_scale37 = 400.0
// a_scale38 = 400.0
//
// a_scale39 = 400.0
// a_scale40 = 400.0
// a_scale41 = 400.0
// a_scale42 = 400.0
// a_scale43 = 400.0
// a_scale44 = 400.0
// a_scale45 = 400.0
// a_scale46 = 400.0
//
// a_sign1 = -1
// a_sign2 = 0
// a_sign3 = 1
// a_sign4 = -1
// a_sign5 = 0
// a_sign6 = 1
