//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

from "lib:cif" import *;

list string excludes = [
    "array_tuple_combi.cif", // Code generation for arrays not supported for S7.
    "arrays.cif",            // Code generation for arrays not supported for S7.
    "io_real.cif",           // Can't connect real-typed value to analog input/output in TIA Portal.
];

rmdir("generated", force=true);
mkdir("generated");

for f in find(".", "*.cif", files=true, dirs=false, recursive=false):
    if contains(excludes, f):: continue;

    outln(f);

    string outpath = replace(pathjoin("generated", chfileext(f, "cif", null)), "\\", "/");

    list string extra_options;
    string options_path = chfileext(f, "cif", "options.txt");
    if isfile(options_path):: extra_options = readlines(options_path);
    int i = 0;
    while i < size(extra_options):
        if size(trim(extra_options[i])) == 0 or startswith(trim(extra_options[i]), "//"):
            extra_options = delidx(extra_options, i);
        else
            i = i + 1;
        end
    end

    list string options = [f, "--output=" + outpath, "--target-type=s7-300 --simplify-values=no --warn-rename=yes"];
    options = options + extra_options;
    outln("with options: %s", join(options, " "));
    cifplcgen(options);

    outln();
end

outln("DONE!");
