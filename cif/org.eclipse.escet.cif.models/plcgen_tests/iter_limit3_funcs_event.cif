//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

@@controller:properties(boundedResponse: true, uncontrollablesBound: 3, controllablesBound: 3, confluence: true, finiteResponse: true, nonBlockingUnderControl: true)

plant a:
  uncontrollable u;
  controllable c;
  disc int x, y = 8;

  location:
    initial;
    edge u when x < 3 and y > 0 do x := x + 1, y := y - 1;
    edge c when x > 0 and y > 0 do x := x - 1, y := y - 1;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.

// ##############################################
// Expected result.
// ##############################################
//
// a_x = 2
// a_y = 0
//
// loopsExhausted = 0
