//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.datasynth;

import static org.eclipse.escet.common.app.framework.output.OutputProvider.dbg;
import static org.eclipse.escet.common.java.Lists.list;

import java.util.List;

import org.eclipse.escet.cif.bdd.settings.AllowNonDeterminism;
import org.eclipse.escet.cif.datasynth.options.BddAdvancedVariableOrderOption;
import org.eclipse.escet.cif.datasynth.options.BddDcshVarOrderOption;
import org.eclipse.escet.cif.datasynth.options.BddDebugMaxNodesOption;
import org.eclipse.escet.cif.datasynth.options.BddDebugMaxPathsOption;
import org.eclipse.escet.cif.datasynth.options.BddForceVarOrderOption;
import org.eclipse.escet.cif.datasynth.options.BddHyperEdgeAlgoOption;
import org.eclipse.escet.cif.datasynth.options.BddInitNodeTableSizeOption;
import org.eclipse.escet.cif.datasynth.options.BddOpCacheRatioOption;
import org.eclipse.escet.cif.datasynth.options.BddOpCacheSizeOption;
import org.eclipse.escet.cif.datasynth.options.BddOutputNamePrefixOption;
import org.eclipse.escet.cif.datasynth.options.BddOutputOption;
import org.eclipse.escet.cif.datasynth.options.BddSimplifyOption;
import org.eclipse.escet.cif.datasynth.options.BddSlidingWindowSizeOption;
import org.eclipse.escet.cif.datasynth.options.BddSlidingWindowVarOrderOption;
import org.eclipse.escet.cif.datasynth.options.BddVariableOrderOption;
import org.eclipse.escet.cif.datasynth.options.ContinuousPerformanceStatisticsFileOption;
import org.eclipse.escet.cif.datasynth.options.EdgeGranularityOption;
import org.eclipse.escet.cif.datasynth.options.EdgeOrderBackwardOption;
import org.eclipse.escet.cif.datasynth.options.EdgeOrderDuplicateEventsOption;
import org.eclipse.escet.cif.datasynth.options.EdgeOrderForwardOption;
import org.eclipse.escet.cif.datasynth.options.EdgeOrderOption;
import org.eclipse.escet.cif.datasynth.options.EdgeWorksetAlgoOption;
import org.eclipse.escet.cif.datasynth.options.EventWarnOption;
import org.eclipse.escet.cif.datasynth.options.ExplorationStrategyOption;
import org.eclipse.escet.cif.datasynth.options.FixedPointComputationsOrderOption;
import org.eclipse.escet.cif.datasynth.options.ForwardReachOption;
import org.eclipse.escet.cif.datasynth.options.PlantsRefReqsWarnOption;
import org.eclipse.escet.cif.datasynth.options.StateReqInvEnforceOption;
import org.eclipse.escet.cif.datasynth.options.SupervisorNameOption;
import org.eclipse.escet.cif.datasynth.options.SupervisorNamespaceOption;
import org.eclipse.escet.cif.datasynth.options.SynthesisStatisticsOption;
import org.eclipse.escet.cif.datasynth.settings.CifDataSynthesisSettings;
import org.eclipse.escet.cif.datasynth.settings.SynthesisStatistics;
import org.eclipse.escet.cif.io.CifReader;
import org.eclipse.escet.cif.io.CifWriter;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.common.app.framework.Application;
import org.eclipse.escet.common.app.framework.Paths;
import org.eclipse.escet.common.app.framework.io.AppStreams;
import org.eclipse.escet.common.app.framework.options.InputFileOption;
import org.eclipse.escet.common.app.framework.options.Option;
import org.eclipse.escet.common.app.framework.options.OptionCategory;
import org.eclipse.escet.common.app.framework.options.Options;
import org.eclipse.escet.common.app.framework.options.OutputFileOption;
import org.eclipse.escet.common.app.framework.output.IOutputComponent;
import org.eclipse.escet.common.app.framework.output.OutputProvider;
import org.eclipse.escet.common.java.PathPair;
import org.eclipse.escet.common.java.Termination;

/** CIF data-based supervisory controller synthesis application. */
public class CifDataSynthesisApp extends Application<IOutputComponent> {
    /**
     * Application main method.
     *
     * @param args The command line arguments supplied to the application.
     */
    public static void main(String[] args) {
        CifDataSynthesisApp app = new CifDataSynthesisApp();
        app.run(args, true);
    }

    /** Constructor for the {@link CifDataSynthesisApp} class. */
    public CifDataSynthesisApp() {
        // Nothing to do here.
    }

    /**
     * Constructor for the {@link CifDataSynthesisApp} class.
     *
     * @param streams The streams to use for input, output, warning, and error streams.
     */
    public CifDataSynthesisApp(AppStreams streams) {
        super(streams);
    }

    @Override
    public String getAppName() {
        return "CIF data-based supervisory controller synthesis tool";
    }

    @Override
    public String getAppDescription() {
        return "Synthesizes a supervisory controller for a CIF specification with data.";
    }

    @Override
    protected int runInternal() {
        // Construct settings. Do it early, to validate settings early.
        //
        // Do not allow non-determinism for controllable events. An external supervisor can't force the correct edge to
        // be taken, if only the updates (includes location pointer variable assignment for target location) are
        // different. For uncontrollable events non-determinism is not a problem, as the supervisor won't restrict edges
        // for uncontrollable events.
        CifDataSynthesisSettings settings = new CifDataSynthesisSettings();

        settings.setTermination(() -> isTerminationRequested());
        settings.setDebugOutput(OutputProvider.getDebugOutputStream());
        settings.setNormalOutput(OutputProvider.getNormalOutputStream());
        settings.setWarnOutput(OutputProvider.getWarningOutputStream());
        settings.setIndentAmount(4);
        settings.setDoPlantsRefReqsWarn(PlantsRefReqsWarnOption.isEnabled());
        settings.setAllowNonDeterminism(AllowNonDeterminism.UNCONTROLLABLE);
        settings.setAdhereToExecScheme(false);
        settings.setBddNumberOfExtraVarDomains(0);
        settings.setBddInitNodeTableSize(BddInitNodeTableSizeOption.getInitialSize());
        settings.setBddOpCacheRatio(BddOpCacheRatioOption.getCacheRatio());
        settings.setBddOpCacheSize(BddOpCacheSizeOption.getCacheSize());
        settings.setBddVarOrderInit(BddVariableOrderOption.getOrder());
        settings.setBddDcshEnabled(BddDcshVarOrderOption.isEnabled());
        settings.setBddForceEnabled(BddForceVarOrderOption.isEnabled());
        settings.setBddSlidingWindowEnabled(BddSlidingWindowVarOrderOption.isEnabled());
        settings.setBddSlidingWindowMaxLen(BddSlidingWindowSizeOption.getMaxLen());
        settings.setBddVarOrderAdvanced(BddAdvancedVariableOrderOption.getOrder());
        settings.setBddHyperEdgeAlgo(BddHyperEdgeAlgoOption.getAlgo());
        settings.setBddDebugMaxNodes(BddDebugMaxNodesOption.getMaximum());
        settings.setBddDebugMaxPaths(BddDebugMaxPathsOption.getMaximum());
        settings.setEdgeGranularity(EdgeGranularityOption.getGranularity());
        settings.setEdgeOrderBackward(EdgeOrderBackwardOption.getOrder());
        settings.setEdgeOrderForward(EdgeOrderForwardOption.getOrder());
        settings.setEdgeOrderAllowDuplicateEvents(EdgeOrderDuplicateEventsOption.getAllowance());
        settings.setCifBddStatistics(SynthesisStatistics.toCifBdd(SynthesisStatisticsOption.getStatistics()));

        settings.setDoNeverEnabledEventsWarn(EventWarnOption.isEnabled());
        settings.setStateReqInvEnforceMode(StateReqInvEnforceOption.getMode());
        settings.setFixedPointComputationsOrder(FixedPointComputationsOrderOption.getOrder());
        settings.setExplorationStrategy(ExplorationStrategyOption.getStrategy());
        settings.setDoForwardReach(ForwardReachOption.isEnabled());
        settings.setSupervisorName(SupervisorNameOption.getSupervisorName());
        settings.setSupervisorNamespace(SupervisorNamespaceOption.getNamespace());
        settings.setBddOutputMode(BddOutputOption.getMode());
        settings.setBddOutputNamePrefix(BddOutputNamePrefixOption.getPrefix());
        settings.setBddSimplifications(BddSimplifyOption.getSimplifications());
        settings.setSynthesisStatistics(SynthesisStatisticsOption.getStatistics());
        settings.setContinuousPerformanceStatisticsFilePath(ContinuousPerformanceStatisticsFileOption.getPath());
        settings.setContinuousPerformanceStatisticsFileAbsPath(
                Paths.resolve(ContinuousPerformanceStatisticsFileOption.getPath()));

        settings.setModificationAllowed(false);

        // Initialize timing statistics.
        boolean doTiming = settings.getSynthesisStatistics().contains(SynthesisStatistics.TIMING);
        CifDataSynthesisTiming timing = new CifDataSynthesisTiming();

        // Do synthesis.
        if (doTiming) {
            timing.total.start();
        }
        try {
            String inputPath = InputFileOption.getPath();
            String absInputPath = Paths.resolve(inputPath);
            doSynthesis(new PathPair(inputPath, absInputPath), settings, timing);
        } finally {
            // Print timing statistics.
            if (doTiming) {
                timing.total.stop();
                timing.print(settings.getIndentAmount(), settings.getDebugOutput(), settings.getNormalOutput());
            }
        }

        // All done.
        return 0;
    }

    /**
     * Perform synthesis.
     *
     * @param inputPair Path pair of the CIF specification on which to perform synthesis.
     * @param settings The settings to use.
     * @param timing The timing statistics data. Is modified in-place.
     */
    private void doSynthesis(PathPair inputPair, CifDataSynthesisSettings settings, CifDataSynthesisTiming timing) {
        // Setup commonly needed settings.
        boolean dbgEnabled = settings.getDebugOutput().isEnabled();
        boolean doTiming = settings.getSynthesisStatistics().contains(SynthesisStatistics.TIMING);
        Termination termination = settings.getTermination();

        // Read CIF specification.
        Specification spec = loadCifSpec(inputPair.userPath, inputPair.systemPath, timing, doTiming, dbgEnabled);

        if (termination.isRequested()) {
            return;
        }

        // Perform synthesis on the input specification.
        String absInputDir = Paths.getAbsFilePathDir(inputPair.systemPath);
        Specification resultSpec = CifDataSynthesis.doSynthesisOnSpec(spec, inputPair.systemPath, absInputDir, settings,
                timing);

        if (termination.isRequested()) {
            return;
        }

        // Write output CIF specification.
        String outPath = OutputFileOption.getDerivedPath(".cif", ".ctrlsys.cif");
        String absOutPath = Paths.resolve(outPath);
        writeCifSpec(resultSpec, absInputDir, timing, doTiming, dbgEnabled, new PathPair(outPath, absOutPath));

        if (termination.isRequested()) {
            return;
        }
    }

    /**
     * Read a CIF file from the file system.
     *
     * @param inputPath Absolute or relative path to the CIF file to read.
     * @param absInputPath Absolute path to the CIF file to read.
     * @param timing Synthesis process timing management tracking and storage.
     * @param doTiming Whether to enable timing the synthesis process.
     * @param dbgEnabled Whether to produce debugging output.
     * @return The read CIF specification.
     */
    private Specification loadCifSpec(String inputPath, String absInputPath, CifDataSynthesisTiming timing,
            boolean doTiming, boolean dbgEnabled)
    {
        if (dbgEnabled) {
            dbg("Reading CIF file \"%s\".", inputPath);
        }

        CifReader cifReader = new CifReader().init(inputPath, absInputPath, false);
        Specification spec;
        if (doTiming) {
            timing.inputRead.start();
        }
        try {
            spec = cifReader.read();
        } finally {
            if (doTiming) {
                timing.inputRead.stop();
            }
        }
        return spec;
    }

    /**
     * Write the resulting CIF specification.
     *
     * @param resultSpec Specification to write.
     * @param absInputDir Absolute path to the directory containing the original input specification.
     * @param timing Synthesis process timing management tracking and storage.
     * @param doTiming Whether to enable timing the synthesis process.
     * @param dbgEnabled Whether to produce debugging output.
     * @param outputPair Path pair for writing the result CIF specification to the destination.
     */
    private void writeCifSpec(Specification resultSpec, String absInputDir, CifDataSynthesisTiming timing,
            boolean doTiming, boolean dbgEnabled, PathPair outputPair)
    {
        if (dbgEnabled) {
            dbg();
            dbg("Writing output CIF file \"%s\".", outputPair.userPath);
        }

        if (doTiming) {
            timing.outputWrite.start();
        }
        try {
            CifWriter.writeCifSpec(resultSpec, outputPair, absInputDir);
        } finally {
            if (doTiming) {
                timing.outputWrite.stop();
            }
        }
    }

    @Override
    protected OutputProvider<IOutputComponent> getProvider() {
        return new OutputProvider<>();
    }

    @Override
    @SuppressWarnings("rawtypes")
    protected OptionCategory getAllOptions() {
        OptionCategory generalCat = getGeneralOptionCategory();

        List<Option> bddOpts = list();
        bddOpts.add(Options.getInstance(BddOutputOption.class));
        bddOpts.add(Options.getInstance(BddOutputNamePrefixOption.class));
        bddOpts.add(Options.getInstance(BddVariableOrderOption.class));
        bddOpts.add(Options.getInstance(BddHyperEdgeAlgoOption.class));
        bddOpts.add(Options.getInstance(BddDcshVarOrderOption.class));
        bddOpts.add(Options.getInstance(BddForceVarOrderOption.class));
        bddOpts.add(Options.getInstance(BddSlidingWindowVarOrderOption.class));
        bddOpts.add(Options.getInstance(BddSlidingWindowSizeOption.class));
        bddOpts.add(Options.getInstance(BddAdvancedVariableOrderOption.class));
        bddOpts.add(Options.getInstance(BddSimplifyOption.class));
        bddOpts.add(Options.getInstance(BddInitNodeTableSizeOption.class));
        bddOpts.add(Options.getInstance(BddOpCacheSizeOption.class));
        bddOpts.add(Options.getInstance(BddOpCacheRatioOption.class));
        bddOpts.add(Options.getInstance(BddDebugMaxNodesOption.class));
        bddOpts.add(Options.getInstance(BddDebugMaxPathsOption.class));
        OptionCategory bddCat = new OptionCategory("BDD", "BDD options.", list(), bddOpts);

        List<Option> synthOpts = list();
        synthOpts.add(Options.getInstance(InputFileOption.class));
        synthOpts.add(Options.getInstance(OutputFileOption.class));
        synthOpts.add(Options.getInstance(SupervisorNameOption.class));
        synthOpts.add(Options.getInstance(SupervisorNamespaceOption.class));
        synthOpts.add(Options.getInstance(ForwardReachOption.class));
        synthOpts.add(Options.getInstance(FixedPointComputationsOrderOption.class));
        synthOpts.add(Options.getInstance(ExplorationStrategyOption.class));
        synthOpts.add(Options.getInstance(EdgeGranularityOption.class));
        synthOpts.add(Options.getInstance(EdgeOrderOption.class)); // No longer supported.
        synthOpts.add(Options.getInstance(EdgeOrderBackwardOption.class));
        synthOpts.add(Options.getInstance(EdgeOrderForwardOption.class));
        synthOpts.add(Options.getInstance(EdgeOrderDuplicateEventsOption.class));
        synthOpts.add(Options.getInstance(EdgeWorksetAlgoOption.class)); // No longer supported.
        synthOpts.add(Options.getInstance(StateReqInvEnforceOption.class));
        synthOpts.add(Options.getInstance(SynthesisStatisticsOption.class));
        synthOpts.add(Options.getInstance(ContinuousPerformanceStatisticsFileOption.class));
        synthOpts.add(Options.getInstance(EventWarnOption.class));
        synthOpts.add(Options.getInstance(PlantsRefReqsWarnOption.class));
        OptionCategory synthCat = new OptionCategory("Synthesis", "Synthesis options.", list(bddCat), synthOpts);

        List<OptionCategory> cats = list(generalCat, synthCat);
        OptionCategory options = new OptionCategory("CIF Data-based Synthesis Options",
                "All options for the CIF data-based supervisory controller synthesis tool.", cats, list());

        return options;
    }
}
