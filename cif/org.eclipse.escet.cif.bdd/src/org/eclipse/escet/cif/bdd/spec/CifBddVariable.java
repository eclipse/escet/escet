//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.bdd.spec;

import static org.eclipse.escet.common.java.Strings.fmt;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.eclipse.escet.cif.bdd.utils.BddUtils;
import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.position.metamodel.position.PositionObject;

import com.github.javabdd.BDDDomain;
import com.github.javabdd.BDDVarSet;

/** A CIF/BDD variable. Represents CIF state in a BDD representation. */
public abstract class CifBddVariable {
    /** The name of the CIF/BDD variable. */
    public final String name;

    /** The name of the CIF/BDD variable, without escaping. */
    public final String rawName;

    /**
     * The 0-based group index number. CIF/BDD variables are in the same group if their {@link #domain domains} are
     * interleaved. Is {@code -1} until actual value is set.
     */
    public int group = -1;

    /** The number of potential values of the variable. */
    public final int count;

    /** The lower bound (minimum value) of the variable. Is at least one. */
    public final int lower;

    /**
     * The upper bound (maximum value) of the variable. Is less than {@link Integer#MAX_VALUE}, and greater or equal to
     * {@link #lower}.
     */
    public final int upper;

    /**
     * The BDD domain of the variable. For updates, this represents the pre/old domain. Is {@code null} until actual
     * domain is set.
     */
    public BDDDomain domain;

    /**
     * The BDD domain of the variable. For updates, this represents the post/new domain. Is {@code null} until actual
     * domain is set.
     */
    public BDDDomain domainNew;

    /**
     * The extra BDD domains of the variable, besides the pre/old and post/new domains. Is {@code null} until actual
     * domains are set. May be empty.
     */
    public BDDDomain[] domainsExtra;

    /**
     * Constructor for the {@link CifBddVariable} class.
     *
     * @param obj The CIF object that corresponds to this CIF/BDD variable. Must be a {@link CifTextUtils#getName named}
     *     CIF object.
     * @param count The number of potential values of the variable.
     * @param lower The lower bound (minimum value) of the variable. Must be at least one.
     * @param upper The upper bound (maximum value) of the variable. Must be less than {@link Integer#MAX_VALUE}, and
     *     greater or equal to {@code lower}.
     */
    public CifBddVariable(PositionObject obj, int count, int lower, int upper) {
        Assert.check(lower <= upper);
        Assert.check(upper < Integer.MAX_VALUE);
        Assert.check(count > 0);
        Assert.check(count == upper - lower + 1);

        this.name = CifTextUtils.getAbsName(obj);
        this.rawName = CifTextUtils.getAbsName(obj, false);
        this.count = count;
        this.lower = lower;
        this.upper = upper;
    }

    /**
     * Returns the size of the {@link #domain}, {@link #domainNew} and each of the {@link #domainsExtra} to create for
     * this CIF/BDD variable. This is the not the number of BDD variables, but the number of actual distinct values that
     * can be represented by the BDD variables. For a variable with {@code n} BDD variables, the size is {@code 1 << n}.
     *
     * @return The size of the BDD domains to create for this CIF/BDD variable.
     */
    public BigInteger getBddDomainSize() {
        return BigInteger.ONE.shiftLeft(getBddVarCount());
    }

    /**
     * Returns the number of BDD variables to use to represent this CIF/BDD variable. Is obtained from {@link #domain}
     * if present, and is computed otherwise.
     *
     * @return The number of BDD variables to use to represent this CIF/BDD variable.
     */
    public int getBddVarCount() {
        // Ask the domain, if it is present.
        if (domain != null) {
            return domain.varNum();
        }

        // Compute it.
        Assert.check(lower >= 0);
        return BddUtils.getMinimumBits(upper);
    }

    /**
     * Returns the variable support for this CIF/BDD variable, covering its {@link #domain} and {@link #domainNew}
     * domains, and optionally its {@link #domainsExtra} domains.
     *
     * @param includeExtraDomains Whether to include the extra domains.
     * @return The BDD variable support for this CIF/BDD variable.
     */
    public BDDVarSet getSupport(boolean includeExtraDomains) {
        BDDVarSet support = domain.set().unionWith(domainNew.set());
        if (includeExtraDomains) {
            for (BDDDomain domainExtra: domainsExtra) {
                support = support.unionWith(domainExtra.set());
            }
        }
        return support;
    }

    @Override
    public String toString() {
        return toString("Variable: ");
    }

    /**
     * Returns a textual representation of the kind of the CIF/BDD variable, relating to the kind of original CIF object
     * it corresponds to.
     *
     * @return The textual representation.
     */
    public abstract String getKindText();

    /**
     * Returns a textual representation of the type of the CIF/BDD variable. Returns {@code null} if the variable has no
     * type.
     *
     * @return The textual representation of the type, or {@code null}.
     */
    public abstract String getTypeText();

    /**
     * Returns a textual representation of the CIF/BDD variable.
     *
     * @param prefix The prefix to use, e.g. {@code "Variable: "} or {@code ""}.
     * @return The textual representation.
     */
    public String toString(String prefix) {
        String domainsExtraTxt = domainsExtra.length == 0 ? "" : ("+" + Arrays.stream(domainsExtra)
                .map(d -> fmt("%,d", d.getIndex())).collect(Collectors.joining("+")));
        return fmt("%s%s (group: %,d, domain: %,d+%,d%s, BDD variables: %,d, CIF/BDD values: %,d/%,d)", prefix,
                toStringInternal(), group, domain.getIndex(), domainNew.getIndex(), domainsExtraTxt, domain.varNum(),
                count, getBddDomainSize());
    }

    /**
     * Returns a textual representation of the CIF/BDD variable, to use as part of the output for {@link #toString}.
     *
     * @return The textual representation.
     */
    protected abstract String toStringInternal();
}
