//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.bdd.utils;

import static org.eclipse.escet.cif.bdd.conversion.BddToCif.bddToCifPred;
import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Lists.listc;
import static org.eclipse.escet.common.java.Strings.fmt;
import static org.eclipse.escet.common.java.Strings.str;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.escet.cif.bdd.conversion.CifBddBitVector;
import org.eclipse.escet.cif.bdd.settings.CifBddSettings;
import org.eclipse.escet.cif.bdd.settings.CifBddStatistics;
import org.eclipse.escet.cif.bdd.spec.CifBddLocPtrVariable;
import org.eclipse.escet.cif.bdd.spec.CifBddSpec;
import org.eclipse.escet.cif.bdd.spec.CifBddTypedVariable;
import org.eclipse.escet.cif.bdd.spec.CifBddVariable;
import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.cif.metamodel.cif.automata.Location;
import org.eclipse.escet.cif.metamodel.cif.declarations.EnumLiteral;
import org.eclipse.escet.cif.metamodel.cif.expressions.Expression;
import org.eclipse.escet.cif.metamodel.cif.types.BoolType;
import org.eclipse.escet.cif.metamodel.cif.types.EnumType;
import org.eclipse.escet.cif.metamodel.cif.types.IntType;
import org.eclipse.escet.common.box.GridBox;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.FileSizes;
import org.eclipse.escet.common.java.ListProductIterator;
import org.eclipse.escet.common.java.Strings;
import org.eclipse.escet.common.java.exceptions.InputOutputException;
import org.eclipse.escet.common.java.output.DebugNormalOutput;

import com.github.javabdd.BDD;
import com.github.javabdd.BDD.AllSatIterator;
import com.github.javabdd.BDDDomain;
import com.github.javabdd.BDDFactory;
import com.github.javabdd.BDDFactory.CacheStats;
import com.github.javabdd.BDDFactory.GCStats;
import com.github.javabdd.BDDFactory.MaxMemoryStats;
import com.github.javabdd.BDDFactory.MaxUsedBddNodesStats;
import com.github.javabdd.BDDVarSet;

/** BDD utility methods. */
public class BddUtils {
    /** Constructor for the {@link BddUtils} class. */
    private BddUtils() {
        // Static class.
    }

    /**
     * Free the given BDD if it is not {@code null}.
     *
     * @param bdd The BDD. May be {@code null}.
     * @return {@code null}, to allow using this method as: {@code var = BddUtils.free(var);}.
     */
    public static BDD free(BDD bdd) {
        if (bdd != null) {
            bdd.free();
        }
        return null;
    }

    /**
     * Free the BDDs in the given list, if the list is not {@code null}.
     *
     * @param bdds The list of BDDs. Both the list and entries in the list may be {@code null}.
     * @return {@code null}, to allow using this method as: {@code var = BddUtils.free(var);}.
     */
    public static List<BDD> free(List<BDD> bdds) {
        if (bdds != null) {
            for (BDD bdd: bdds) {
                if (bdd != null) {
                    bdd.free();
                }
            }
        }
        return null;
    }

    /**
     * Free the BDDs in the keys and/or values of the given map, if the map is not {@code null}.
     *
     * @param <K> The type of the keys of the map.
     * @param <V> The type of the values of the map.
     * @param map The map with BDDs. May be {@code null}.
     * @param getBddFunc Function to get BDDs to free, from a non-{@code null} map entry. The function may return
     *     {@code null}, or a collection of BDDs that may contain {@code null} entries.
     * @return {@code null}, to allow using this method as: {@code var = BddUtils.free(var, ...);}.
     */
    public static <K, V> Map<K, V> free(Map<K, V> map, Function<Entry<K, V>, Collection<BDD>> getBddFunc) {
        if (map != null) {
            for (Entry<K, V> entry: map.entrySet()) {
                Collection<BDD> bdds = getBddFunc.apply(entry);
                for (BDD bdd: bdds) {
                    if (bdd != null) {
                        bdd.free();
                    }
                }
            }
        }
        return null;
    }

    /**
     * Free the given BDD variable set if it is not {@code null}.
     *
     * @param varSet The BDD variable set. May be {@code null}.
     * @return {@code null}, to allow using this method as: {@code var = BddUtils.free(var);}.
     */
    public static BDDVarSet free(BDDVarSet varSet) {
        if (varSet != null) {
            varSet.free();
        }
        return null;
    }

    /**
     * Free the BDD variable sets in the given list, if the list is not {@code null}.
     *
     * @param varSets The list of BDD variable sets. Both the list and entries in the list may be {@code null}.
     * @return {@code null}, to allow using this method as: {@code varSets = BddUtils.free(varSets);}.
     */
    public static List<BDDVarSet> freeVarSets(List<BDDVarSet> varSets) {
        if (varSets != null) {
            for (BDDVarSet varSet: varSets) {
                if (varSet != null) {
                    varSet.free();
                }
            }
        }
        return null;
    }

    /**
     * Returns the minimum number of bits need to represent the given decimal value in a binary representation.
     *
     * @param value The decimal value. Must be non-negative.
     * @return The minimum number of bits.
     */
    public static int getMinimumBits(int value) {
        int count = 0;
        while (value > 0) {
            count++;
            value = value >> 1;
        }
        return count;
    }

    /**
     * Returns the domain predicate of the given variable. That is, a predicate that is 'true' if and only if the
     * variable has a valid value.
     *
     * <p>
     * The result of this method includes only the values of the CIF domain of the variable, not the BDD domain of the
     * variable. For a variable 'x' of type 'int[1..2]', two BDD variables are created, which can represent values of
     * type 'int[0..3]'. Values '0' and '3' are not valid values of variable 'x'. This method returns 'x = 1 or x = 2'.
     * </p>
     *
     * @param variable The CIF/BDD variable.
     * @param newDomain Whether to return a predicate for the pre/old domain of the variable ({@code false}) or the
     *     post/new domain of the variable ({@code true}).
     * @param factory The BDD factory to use.
     * @return The possible values of the variable being assigned.
     */
    public static BDD getVarDomain(CifBddVariable variable, boolean newDomain, BDDFactory factory) {
        // Get minimum and maximum value of the domain.
        int min = variable.lower;
        int max = variable.upper;

        // Add possible values to the result.
        BDDDomain domain = newDomain ? variable.domainNew : variable.domain;
        BDD rslt = factory.zero();
        for (int i = min; i <= max; i++) {
            rslt = rslt.orWith(domain.ithVar(i));
        }
        return rslt;
    }

    /**
     * Converts a BDD to a textual representation that closely resembles CIF ASCII syntax.
     *
     * <p>
     * This method only supports predicates with pre/old domain BDD variables.
     * </p>
     *
     * @param bdd The BDD.
     * @param cifBddSpec The CIF/BDD specification.
     * @return The textual representation of the BDD.
     */
    public static String bddToStr(BDD bdd, CifBddSpec cifBddSpec) {
        // If one of the specific maximum counts is exceeded, don't actually
        // convert the BDD to a CNF/DNF predicate, for performance reasons.
        if (cifBddSpec.settings.getBddDebugMaxNodes() != null || cifBddSpec.settings.getBddDebugMaxPaths() != null) {
            // Get node count and true path count.
            int nc = bdd.nodeCount();
            BigInteger tpc = bdd.pathCount();

            boolean skip = (cifBddSpec.settings.getBddDebugMaxNodes() != null
                    && nc > cifBddSpec.settings.getBddDebugMaxNodes())
                    || (cifBddSpec.settings.getBddDebugMaxPaths() != null
                            && tpc.compareTo(cifBddSpec.settings.getBddDebugMaxPaths()) > 0);

            if (skip) {
                return fmt("<bdd %,dn %,dp>", nc, tpc);
            }
        }

        // Convert BDD to CNF/DNF predicate.
        Expression pred = bddToCifPred(bdd, cifBddSpec);
        return CifTextUtils.exprToStr(pred);
    }

    /**
     * Convert a BDD to CIF states, for all BDD valuations that satisfy the given BDD. Only valid CIF states are
     * returned. Only '{@link CifBddSpec#varSetOld old}' variables are considered.
     *
     * @param bdd The BDD.
     * @param cifBddSpec The CIF/BDD specification.
     * @param inclDontCares Whether to include variables that are entirely don't care for a true path of the BDD,
     *     leading to multiple states for them ({@code true}), or exclude these variables from the state representations
     *     ({@code false}).
     * @param maxStateCount The maximum number of states to include in the result.
     * @return The sorted CIF states. The types of the values are as for {@link #valuationToValue}.
     */
    public static List<Map<CifBddVariable, Object>> bddToStates(BDD bdd, CifBddSpec cifBddSpec, boolean inclDontCares,
            int maxStateCount)
    {
        // Get number of satisfying valuations. May include valuations with don't cares.
        BigInteger satCount = bdd.satCount(cifBddSpec.varSetOld);

        // Get expected number of states. In case of don't cares, it may be too low. Still, we only need an
        // approximation.
        int expectedCount = satCount.min(BigInteger.valueOf(maxStateCount)).intValue();

        // Generate states.
        List<Map<CifBddVariable, Object>> states = listc(expectedCount);
        for (AllSatIterator iter = bdd.allsat(); iter.hasNext();) {
            // Get next valuation.
            byte[] valuation = iter.next();

            // Add states for the valuation.
            int maxStateRemaining = maxStateCount - states.size();
            List<Map<CifBddVariable, Object>> valuationStates = valuationToStates(valuation, cifBddSpec, inclDontCares,
                    maxStateRemaining);
            Assert.check(valuationStates.size() <= maxStateRemaining);
            states.addAll(valuationStates);

            // If we've reached the maximum number of states, we're done.
            if (states.size() == maxStateCount) {
                break;
            }
        }

        // Sort the states.
        sortStates(states);

        // Return the sorted states.
        Assert.check(states.size() <= maxStateCount);
        return states;
    }

    /**
     * Convert a BDD valuation to CIF states. Only valid CIF states are returned. Only '{@link CifBddSpec#varSetOld
     * old}' variables are considered.
     *
     * @param valuation The BDD valuation, with per BDD variable a {@code -1} for don't care, {@code 0} for
     *     {@code false}, or {@code 1} for {@code true}.
     * @param cifBddSpec The CIF/BDD specification.
     * @param inclDontCares Whether to include variables that are entirely don't care for a true path of the BDD,
     *     leading to multiple states for them ({@code true}), or exclude these variables from the state representations
     *     ({@code false}).
     * @param maxStateCount The maximum number of states to include in the result.
     * @return The sorted CIF states. The types of the values are as for {@link #valuationToValue}.
     */
    public static List<Map<CifBddVariable, Object>> valuationToStates(byte[] valuation, CifBddSpec cifBddSpec,
            boolean inclDontCares, int maxStateCount)
    {
        // Special case for no variables. There is then one state, not zero states. Whether this state is returned
        // depends on the given maximum number of states.
        if (cifBddSpec.variables.length == 0) {
            return (maxStateCount <= 0) ? List.of() : List.of(new TreeMap<>());
        }

        // Get possible values per variable.
        List<CifBddVariable> variables = listc(cifBddSpec.variables.length); // Include variables.
        List<List<Object>> valuesPerVar = listc(cifBddSpec.variables.length); // Per included variable, its values.
        for (CifBddVariable cifBddVar: cifBddSpec.variables) {
            // Get valuation for the 'old' domain of this variable.
            BDDDomain domain = cifBddVar.domain;
            byte[] domainValuation = new byte[domain.varNum()];
            int[] varIdxs = domain.vars();
            for (int i = 0; i < varIdxs.length; i++) {
                int varIdx = varIdxs[i];
                byte bit = valuation[varIdx];
                domainValuation[i] = bit;
            }

            // Analyze don't care bits.
            boolean anyDontCare = false;
            boolean onlyDontCare = true;
            for (int i = 0; i < domainValuation.length; i++) {
                boolean bitIsDontCare = domainValuation[i] == -1;
                anyDontCare |= bitIsDontCare;
                onlyDontCare &= bitIsDontCare;
            }

            // If requested, skip variable if it is not relevant (all bits are don't cares).
            if (onlyDontCare && !inclDontCares) {
                continue;
            }

            // Add values of the variable, for all concrete valuations for the variable.
            variables.add(cifBddVar);
            if (!anyDontCare) {
                // Single concrete valuation.
                Object value = valuationToValue(cifBddVar, domainValuation, cifBddSpec.factory);
                if (value == null) {
                    // Single invalid value for this variable, so no states.
                    return List.of();
                }
                valuesPerVar.add(List.of(value));
            } else {
                // Multiple concrete valuations. Get possible values per BDD variable (bit).
                List<List<Boolean>> valuesPerBit = listc(domainValuation.length);
                for (int i = 0; i < domainValuation.length; i++) {
                    byte bit = domainValuation[i];
                    List<Boolean> bitValues = switch (bit) {
                        case -1 -> List.of(false, true);
                        case 0 -> List.of(false);
                        case 1 -> List.of(true);
                        default -> throw new AssertionError("Unexpected bit: " + bit);
                    };
                    valuesPerBit.add(bitValues);
                }

                // Get possible values of the variable, for the concrete valuations. Limit it to the maximum number of
                // states, as more values won't be used.
                List<Object> varValues = list();
                Iterator<List<Boolean>> iter = new ListProductIterator<>(valuesPerBit);
                while (iter.hasNext() && varValues.size() < maxStateCount) {
                    // Get concrete valuation as byte array.
                    List<Boolean> nextValuation = iter.next();
                    byte[] byteValuation = new byte[nextValuation.size()];
                    for (int i = 0; i < byteValuation.length; i++) {
                        byteValuation[i] = (byte)(nextValuation.get(i) ? 1 : 0);
                    }

                    // Add variable value, if it is a valid value for the variable.
                    Object value = valuationToValue(cifBddVar, byteValuation, cifBddSpec.factory);
                    if (value != null) {
                        varValues.add(value);
                    }
                }
                valuesPerVar.add(varValues);
            }
        }

        // Get list to store resulting states, with a decently-approximated initial size.
        ListProductIterator<Object> stateIter = new ListProductIterator<>(valuesPerVar);
        Optional<Long> stateCountOrOverflow = stateIter.getResultSize();
        long cappedStateCount = stateCountOrOverflow.orElse(Long.MAX_VALUE);
        int approxStateCount = (int)cappedStateCount;
        List<Map<CifBddVariable, Object>> states = listc(Math.min(approxStateCount, maxStateCount));

        // Return the states, capped at the maximum number of states.
        while (stateIter.hasNext() && states.size() < maxStateCount) {
            List<Object> stateValues = stateIter.next();
            Map<CifBddVariable, Object> stateMap = new TreeMap<>(Comparator.comparing(v -> v.name));
            for (int i = 0; i < stateValues.size(); i++) {
                stateMap.put(variables.get(i), stateValues.get(i));
            }
            states.add(stateMap);
        }

        // Sort the states.
        sortStates(states);

        // Return the sorted states.
        return states;
    }

    /**
     * Get the value of the given CIF/BDD variable, for its given concrete valuation. If the valuation represents a
     * value outside of the range of the CIF/BDD variable, {@code null} is returned instead.
     *
     * <p>
     * Depending on the kind of variable, different types of values are returned:
     * <ul>
     * <li>For location pointers, a {@link Location}.</li>
     * <li>For boolean variables, a {@link Boolean}.</li>
     * <li>For integer variables, an {@link Integer}.</li>
     * <li>For enum-typed variables, an {@link EnumLiteral}.</li>
     * </ul>
     * </p>
     *
     * @param cifBddVar The CIF/BDD variable.
     * @param valuation The valuation, with per bit {@code 0} for {@code false} or {@code 1} for {@code true}.
     * @param factory The BDD factory.
     * @return The value, or {@code null}.
     */
    public static Object valuationToValue(CifBddVariable cifBddVar, byte[] valuation, BDDFactory factory) {
        // Create BDD bit vector for the valuation.
        CifBddBitVector vector = CifBddBitVector.create(factory, valuation.length);
        for (int i = 0; i < valuation.length; i++) {
            byte bit = valuation[i];
            Assert.check(bit == 0 || bit == 1, bit);
            vector.setBit(i, bit == 1);
        }

        // Get the value of the vector.
        int value = vector.getInt();
        Assert.check(value >= 0);

        // Check for an invalid value for the CIF/BDD variable.
        if (value < cifBddVar.lower || value > cifBddVar.upper) {
            return null;
        }

        // Return the CIF textual representation of the value.
        return switch (cifBddVar) {
            case CifBddLocPtrVariable lpVar -> lpVar.aut.getLocations().get(value);
            case CifBddTypedVariable typedVar -> switch (typedVar.type) {
                case BoolType btype -> value == 1;
                case IntType itype -> value;
                case EnumType etype -> etype.getEnum().getLiterals().get(value);
                default -> throw new AssertionError("Unexpected type: " + typedVar.type);
            };
            default -> throw new AssertionError("Unexpected variable: " + cifBddVar);
        };
    }

    /**
     * Sort the given states.
     *
     * @param states The states to sort. Is sorted in-place.
     */
    static void sortStates(List<Map<CifBddVariable, Object>> states) {
        Collections.sort(states, (s1, s2) -> {
            // All states are TreeMaps, sorted on variable names.
            Iterator<CifBddVariable> keyIter1 = s1.keySet().iterator();
            Iterator<CifBddVariable> keyIter2 = s2.keySet().iterator();

            // Go through all the variables.
            while (keyIter1.hasNext() || keyIter2.hasNext()) {
                if (!keyIter1.hasNext() && keyIter2.hasNext()) {
                    return -1; // s1 has less variables, so it goes first.
                } else if (keyIter1.hasNext() && !keyIter2.hasNext()) {
                    return 1; // s2 has less variables, so it goes first.
                } else {
                    // Check which variable goes first.
                    CifBddVariable var1 = keyIter1.next();
                    CifBddVariable var2 = keyIter2.next();
                    int result = var1.name.compareTo(var2.name);
                    if (result != 0) {
                        return result;
                    }

                    // Same variable. Get values.
                    Assert.check(var1 == var2);
                    Object value1 = s1.get(var1);
                    Object value2 = s2.get(var2);

                    // Compare values.
                    if (value1 instanceof Location l1 && value2 instanceof Location l2) {
                        result = l1.getName().compareTo(l2.getName());
                    } else if (value1 instanceof Boolean b1 && value2 instanceof Boolean b2) {
                        result = b1.compareTo(b2);
                    } else if (value1 instanceof Integer i1 && value2 instanceof Integer i2) {
                        result = i1.compareTo(i2);
                    } else if (value1 instanceof EnumLiteral l1 && value2 instanceof EnumLiteral l2) {
                        result = l1.getName().compareTo(l2.getName());
                    } else {
                        throw new AssertionError("Unexpected combination of values: " + value1 + " / " + value2);
                    }
                    if (result != 0) {
                        return result;
                    }
                }
            }
            return 0;
        });
    }

    /**
     * Converts a state returned by {@link #bddToStates} or {@link #valuationToStates} to a textual representation,
     * where the variable values closely resembles the CIF ASCII syntax.
     *
     * @param state The state.
     * @param emptyText The text to return for states without any variables.
     * @return The textual representation.
     */
    public static String stateToStr(Map<CifBddVariable, Object> state, String emptyText) {
        if (state.isEmpty()) {
            return emptyText;
        } else {
            return state.entrySet().stream().map(e -> e.getKey().name + "=" + BddUtils.stateValueToStr(e.getValue()))
                    .collect(Collectors.joining(", "));
        }
    }

    /**
     * Converts a state value returned by {@link #valuationToValue} to a textual representation that closely resembles
     * the CIF ASCII syntax.
     *
     * @param value The value.
     * @return The textual representation.
     */
    public static String stateValueToStr(Object value) {
        return switch (value) {
            case Location loc -> loc.getName();
            case Boolean bool -> bool ? "true" : "false";
            case Integer i -> Integer.toString(i);
            case EnumLiteral lit -> lit.getName();
            default -> throw new AssertionError("Unexpected value: " + value);
        };
    }

    /**
     * If requested, register BDD factory callbacks that print some statistics.
     *
     * @param factory The BDD factory for which to register the callbacks.
     * @param doGcStats Whether to output BDD GC statistics, if normal output is enabled.
     * @param doResizeStats Whether to output BDD resize statistics, if normal output is enabled.
     * @param doContinuousPerformanceStats Whether to output continuous BDD performance statistics.
     * @param normalOutput Callback for normal output.
     * @param continuousOpMisses The list into which to collect continuous operation misses samples.
     * @param continuousUsedBddNodes The list into which to collect continuous used BDD nodes statistics samples.
     */
    public static void registerBddCallbacks(BDDFactory factory, boolean doGcStats, boolean doResizeStats,
            boolean doContinuousPerformanceStats, DebugNormalOutput normalOutput, List<Long> continuousOpMisses,
            List<Integer> continuousUsedBddNodes)
    {
        // Register BDD garbage collection callback.
        if (doGcStats && normalOutput.isEnabled()) {
            factory.registerGcStatsCallback((stats, pre) -> bddGcStatsCallback(stats, pre, normalOutput));
        }

        // Register BDD internal node array resize callback.
        if (doResizeStats && normalOutput.isEnabled()) {
            factory.registerResizeStatsCallback(
                    (oldSize, newSize) -> bddResizeStatsCallback(oldSize, newSize, normalOutput));
        }

        // Register continuous BDD performance statistics callback.
        if (doContinuousPerformanceStats) {
            factory.registerContinuousStatsCallback((n, o) -> {
                continuousOpMisses.add(o);
                continuousUsedBddNodes.add(n);
            });
        }
    }

    /**
     * Callback invoked when the BDD library performs garbage collection on its internal data structures. Prints
     * statistics.
     *
     * @param stats The garbage collection statistics.
     * @param pre Whether the callback is invoked just before garbage collection ({@code true}) or just after it
     *     ({@code false}).
     * @param normalOutput Callback for normal output.
     */
    private static void bddGcStatsCallback(GCStats stats, boolean pre, DebugNormalOutput normalOutput) {
        StringBuilder txt = new StringBuilder();
        txt.append("BDD ");
        txt.append(pre ? "pre " : "post");
        txt.append(" garbage collection: #");
        txt.append(fmt("%,d", stats.num + 1 - (pre ? 0 : 1)));
        txt.append(", ");
        txt.append(fmt("%,13d", stats.freenodes));
        txt.append(" of ");
        txt.append(fmt("%,13d", stats.nodes));
        txt.append(" nodes free");
        if (!pre) {
            txt.append(", ");
            txt.append(fmt("%,13d", stats.time));
            txt.append(" ms, ");
            txt.append(fmt("%,13d", stats.sumtime));
            txt.append(" ms total");
        }
        normalOutput.line(txt.toString());
    }

    /**
     * Callback invoked when the BDD library resizes its internal node array. Prints statistics.
     *
     * @param oldSize The old size of the internal node array.
     * @param newSize The new size of the internal node array.
     * @param normalOutput Callback for normal output.
     */
    private static void bddResizeStatsCallback(int oldSize, int newSize, DebugNormalOutput normalOutput) {
        normalOutput.line("BDD node table resize: from %,13d nodes to %,13d nodes", oldSize, newSize);
    }

    /**
     * Prints the BDD cache statistics, maximum used BDD nodes statistics, and maximum memory usage statistics, if
     * enabled in the settings. Also writes the continuous BDD performance statistics to a file, if enabled in the
     * settings.
     *
     * @param factory The BDD factory.
     * @param settings The settings to use.
     * @param continuousOpMisses The list into which to collect continuous operation misses samples.
     * @param continuousUsedBddNodes The list into which to collect continuous used BDD nodes statistics samples.
     * @param continuousPerformanceStatisticsFilePath The absolute or relative path to the continuous performance
     *     statistics output file.
     * @param continuousPerformanceStatisticsFileAbsPath The absolute path to the continuous performance statistics
     *     output file.
     */
    public static void printStats(BDDFactory factory, CifBddSettings settings, List<Long> continuousOpMisses,
            List<Integer> continuousUsedBddNodes, String continuousPerformanceStatisticsFilePath,
            String continuousPerformanceStatisticsFileAbsPath)
    {
        DebugNormalOutput debugOutput = settings.getDebugOutput();
        boolean dbgEnabled = debugOutput.isEnabled();

        // Check what statistics to print.
        boolean doCacheStats = settings.getCifBddStatistics().contains(CifBddStatistics.BDD_PERF_CACHE);
        boolean doContinuousPerformanceStats = settings.getCifBddStatistics().contains(CifBddStatistics.BDD_PERF_CONT);
        boolean doMaxBddNodesStats = settings.getCifBddStatistics().contains(CifBddStatistics.BDD_PERF_MAX_NODES);
        boolean doMaxMemoryStats = settings.getCifBddStatistics().contains(CifBddStatistics.MAX_MEMORY);

        // Print the statistics.
        if (doCacheStats) {
            if (dbgEnabled) {
                debugOutput.line();
            }
            BddUtils.printBddCacheStats(factory.getCacheStats(), settings.getNormalOutput());
        }

        if (doContinuousPerformanceStats) {
            if (dbgEnabled) {
                debugOutput.line();
                debugOutput.line("Writing continuous BDD performance statistics file \"%s\".",
                        continuousPerformanceStatisticsFilePath);
            }
            BddUtils.writeBddContinuousPerformanceStatsFile(continuousOpMisses, continuousUsedBddNodes,
                    continuousPerformanceStatisticsFilePath, continuousPerformanceStatisticsFileAbsPath);
        }

        if (doMaxBddNodesStats) {
            if (dbgEnabled) {
                debugOutput.line();
            }
            BddUtils.printBddMaxUsedBddNodesStats(factory.getMaxUsedBddNodesStats(), settings.getNormalOutput());
        }

        if (doMaxMemoryStats) {
            if (dbgEnabled) {
                debugOutput.line();
            }
            BddUtils.printMaxMemoryStats(factory.getMaxMemoryStats(), settings.getNormalOutput());
        }
    }

    /**
     * Print the BDD cache statistics.
     *
     * @param stats The BDD cache statistics.
     * @param normalOutput Callback for normal output.
     */
    public static void printBddCacheStats(CacheStats stats, DebugNormalOutput normalOutput) {
        // Create grid.
        GridBox grid = new GridBox(7, 2, 0, 1);

        grid.set(0, 0, "Node creation requests:");
        grid.set(1, 0, "Node creation chain accesses:");
        grid.set(2, 0, "Node creation cache hits:");
        grid.set(3, 0, "Node creation cache misses:");
        grid.set(4, 0, "Operation count:");
        grid.set(5, 0, "Operation cache hits:");
        grid.set(6, 0, "Operation cache misses:");

        grid.set(0, 1, str(stats.uniqueAccess));
        grid.set(1, 1, str(stats.uniqueChain));
        grid.set(2, 1, str(stats.uniqueHit));
        grid.set(3, 1, str(stats.uniqueMiss));
        grid.set(4, 1, str(stats.opAccess));
        grid.set(5, 1, str(stats.opHit));
        grid.set(6, 1, str(stats.opMiss));

        // Print statistics.
        normalOutput.line("BDD cache statistics:");
        normalOutput.inc();
        for (String line: grid.getLines()) {
            normalOutput.line(line);
        }
        normalOutput.dec();
    }

    /**
     * Print the BDD maximum used BDD nodes statistics.
     *
     * @param stats The BDD maximum used BDD nodes statistics.
     * @param normalOutput Callback for normal output.
     */
    public static void printBddMaxUsedBddNodesStats(MaxUsedBddNodesStats stats, DebugNormalOutput normalOutput) {
        normalOutput.line(fmt("Maximum used BDD nodes: %d.", stats.getMaxUsedBddNodes()));
    }

    /**
     * Print the maximum memory usage statistics.
     *
     * @param stats The maximum memory usage statistics.
     * @param normalOutput Callback for normal output.
     */
    public static void printMaxMemoryStats(MaxMemoryStats stats, DebugNormalOutput normalOutput) {
        long maxMemoryBytes = stats.getMaxMemoryBytes();
        normalOutput.line(fmt("Maximum used memory: %d bytes = %s.", maxMemoryBytes,
                FileSizes.formatFileSize(maxMemoryBytes, false)));
    }

    /**
     * Write the continuous BDD performance statistics to a file.
     *
     * @param operationsSamples The collected continuous operation misses samples.
     * @param nodesSamples The collected continuous used BDD nodes statistics samples.
     * @param filePath The absolute or relative path to the continuous performance statistics output file.
     * @param absFilePath The absolute path to the continuous performance statistics output file.
     */
    public static void writeBddContinuousPerformanceStatsFile(List<Long> operationsSamples, List<Integer> nodesSamples,
            String filePath, String absFilePath)
    {
        Assert.notNull(filePath);
        Assert.notNull(absFilePath);

        // Get number of data points.
        Assert.areEqual(operationsSamples.size(), nodesSamples.size());
        int numberOfDataPoints = operationsSamples.size();

        // Write the data to a file.
        try (OutputStream stream = new BufferedOutputStream(new FileOutputStream(absFilePath));
             Writer writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8))
        {
            writer.write("Operations,Used BBD nodes");
            writer.write(Strings.NL);
            long lastOperations = -1;
            int lastNodes = -1;
            for (int i = 0; i < numberOfDataPoints; i++) {
                // Only print new data points.
                long nextOperations = operationsSamples.get(i);
                int nextNodes = nodesSamples.get(i);
                if (nextOperations != lastOperations || nextNodes != lastNodes) {
                    lastOperations = nextOperations;
                    lastNodes = nextNodes;
                    writer.write(fmt("%d,%d", lastOperations, lastNodes));
                    writer.write(Strings.NL);
                }
            }
        } catch (IOException e) {
            throw new InputOutputException(
                    fmt("Failed to write continuous BDD performance statistics file \"%s\".", filePath), e);
        }
    }
}
