//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.checks.boundedresponse;

import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Lists.slice;
import static org.eclipse.escet.common.java.Pair.pair;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.eclipse.escet.cif.bdd.settings.CifBddSettings;
import org.eclipse.escet.cif.bdd.settings.EdgeGranularity;
import org.eclipse.escet.cif.bdd.spec.CifBddEdge;
import org.eclipse.escet.cif.bdd.spec.CifBddEdgeApplyDirection;
import org.eclipse.escet.cif.bdd.spec.CifBddEdgeKind;
import org.eclipse.escet.cif.bdd.spec.CifBddSpec;
import org.eclipse.escet.cif.bdd.spec.CifBddVariable;
import org.eclipse.escet.cif.bdd.utils.BddUtils;
import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.cif.controllercheck.ControllerCheckerSettings;
import org.eclipse.escet.cif.controllercheck.checks.ControllerCheckerBddBasedCheck;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.Lists;
import org.eclipse.escet.common.java.Maps;
import org.eclipse.escet.common.java.Pair;
import org.eclipse.escet.common.java.Termination;
import org.eclipse.escet.common.java.exceptions.UnsupportedException;
import org.eclipse.escet.common.java.output.DebugNormalOutput;

import com.github.javabdd.BDD;

/** Class for checking a CIF specification has bounded response. */
public class BoundedResponseCheck extends ControllerCheckerBddBasedCheck<BoundedResponseCheckConclusion> {
    /** The name of the property being checked. */
    public static final String PROPERTY_NAME = "bounded response";

    /** Whether to produce verbose debug output. */
    private static final boolean VERBOSE_DEBUG_OUTPUT = false;

    /**
     * The maximum number of 'printed cycle states' to print. Value is non-negative and at most {@code Integer.MAX_VALUE
     * - 10}.
     */
    private final int maxPrintedCycleStatesCount;

    /**
     * Constructor for the {@link BoundedResponseCheck} class.
     *
     * @param maxPrintedCycleStatesCount The maximum number of 'printed cycle states' to print. Value must be
     *     non-negative and at most {@code Integer.MAX_VALUE - 10}.
     */
    public BoundedResponseCheck(int maxPrintedCycleStatesCount) {
        this.maxPrintedCycleStatesCount = maxPrintedCycleStatesCount;
    }

    @Override
    public String getPropertyName() {
        return PROPERTY_NAME;
    }

    @Override
    protected CifBddSettings createCifBddSettings(ControllerCheckerSettings checkerSettings) {
        // Create default settings.
        CifBddSettings cifBddSettings = super.createCifBddSettings(checkerSettings);

        // Enable adherence to the execution scheme.
        cifBddSettings.setEdgeGranularity(EdgeGranularity.PER_EDGE);
        cifBddSettings.setEdgeOrderForward("sorted");
        cifBddSettings.setAdhereToExecScheme(true);

        // Return settings for this check.
        return cifBddSettings;
    }

    @Override
    public BoundedResponseCheckConclusion performCheck(CifBddSpec cifBddSpec) {
        // The algorithm works as follows:
        // - If there are no initial states, the bounds are trivially zero.
        // - Otherwise, we compute the possible start states for the uncontrollable and controllable event loops.
        // - Then we compute for each event loop its bound, using the possible start states as input.

        // Get settings.
        Termination termination = cifBddSpec.settings.getTermination();
        DebugNormalOutput dbg = cifBddSpec.settings.getDebugOutput();

        // If there are no initial states, the bounds are trivial. Note that preconditions forbid state invariants, so
        // they don't need to be taken into account when determining the possible initial states.
        if (cifBddSpec.initial.isZero()) {
            dbg.line("No initial states.");
            return new BoundedResponseCheckConclusion(
                    new Bound(false, true, 0, null, null), new Bound(false, true, 0, null, null),
                    maxPrintedCycleStatesCount);
        }

        // Get input variable edges, uncontrollable event edges, and controllable event edges.
        List<CifBddEdge> orderedEdges = cifBddSpec.orderedEdgesForward;

        List<CifBddEdge> inputEdges = orderedEdges.stream()
                .filter(edge -> edge.getEdgeKind() == CifBddEdgeKind.INPUT_VARIABLE).toList();
        List<CifBddEdge> uncontrollableEdges = orderedEdges.stream()
                .filter(edge -> edge.getEdgeKind() == CifBddEdgeKind.UNCONTROLLABLE).toList();
        List<CifBddEdge> controllableEdges = orderedEdges.stream()
                .filter(edge -> edge.getEdgeKind() == CifBddEdgeKind.CONTROLLABLE).toList();

        Map<Event, List<CifBddEdge>> edgesPerInputEvent = inputEdges.stream()
                .collect(Collectors.groupingBy(edge -> edge.event, Maps::map, Lists.toList()));
        Map<Event, List<CifBddEdge>> edgesPerUnctrlEvent = uncontrollableEdges.stream()
                .collect(Collectors.groupingBy(edge -> edge.event, Maps::map, Lists.toList()));
        Map<Event, List<CifBddEdge>> edgesPerCtrlEvent = controllableEdges.stream()
                .collect(Collectors.groupingBy(edge -> edge.event, Maps::map, Lists.toList()));

        // Compute pre (un)controllable event loops states.
        dbg.line("Computing states we can be in at the start of uncontrollable and controllable event loops:");
        dbg.inc();

        Pair<BDD, BDD> preStates = computeLoopsStartStates(cifBddSpec, edgesPerInputEvent, edgesPerUnctrlEvent,
                edgesPerCtrlEvent);

        dbg.dec();
        termination.throwIfRequested();

        BDD preUnctrlStates = preStates.left;
        BDD preCtrlStates = preStates.right;

        // Compute uncontrollables bounds.
        dbg.line();
        dbg.line("Computing bound for uncontrollable events:");
        dbg.inc();

        Bound uncontrollablesBound = computeLoopBound(cifBddSpec, preUnctrlStates, edgesPerUnctrlEvent,
                "uncontrollable");

        termination.throwIfRequested();

        preUnctrlStates.free();

        dbg.line();
        dbg.line("Bound: %s.", uncontrollablesBound.isBounded() ? uncontrollablesBound.getBound() : "n/a (cycle)");
        dbg.dec();
        termination.throwIfRequested();

        // Compute controllables bounds.
        dbg.line();
        dbg.line("Computing bound for controllable events:");
        dbg.inc();

        Bound controllablesBound = computeLoopBound(cifBddSpec, preCtrlStates, edgesPerCtrlEvent, "controllable");

        termination.throwIfRequested();

        preCtrlStates.free();

        dbg.line();
        dbg.line("Bound: %s.", controllablesBound.isBounded() ? controllablesBound.getBound() : "n/a (cycle)");
        dbg.dec();
        termination.throwIfRequested();

        // Done.
        dbg.line();
        dbg.line("Bounded response check completed.");

        // Return check result.
        return new BoundedResponseCheckConclusion(uncontrollablesBound, controllablesBound, maxPrintedCycleStatesCount);
    }

    /**
     * Compute the start states that we can be in before event loops, for uncontrollable and controllable events, taking
     * into account the execution scheme prescribed by the controller properties checker.
     *
     * @param cifBddSpec The CIF/BDD specification.
     * @param edgesPerInputEvent The edges to consider, per input variable event.
     * @param edgesPerUnctrlEvent The edges to consider, per uncontrollable event. The events and edges are in the order
     *     prescribed by the controller properties checker.
     * @param edgesPerCtrlEvent The edges to consider, per controllable event. The events and edges are in the order
     *     prescribed by the controller properties checker.
     * @return The states that we can be in before the event loops for uncontrollable and controllable events,
     *     respectively.
     */
    private Pair<BDD, BDD> computeLoopsStartStates(CifBddSpec cifBddSpec,
            Map<Event, List<CifBddEdge>> edgesPerInputEvent, Map<Event, List<CifBddEdge>> edgesPerUnctrlEvent,
            Map<Event, List<CifBddEdge>> edgesPerCtrlEvent)
    {
        // The algorithm works as follows:
        // - We have three event loops, one for the input variable events (which can change before the 'program code' is
        //   executed), the uncontrollable events, and the controllable events.
        // - We initialize the states we can be in at the start of the input variables events loop to the initial states
        //   of the system. We initialize the states we can be in at the start of the uncontrollable and controllable
        //   event loops to 'false'.
        // - We execute as many rounds as needed to reach a fixed point for the three types of 'start states', by in
        //   each round executing the 'program code' once.
        // - In a round, we first do an event loop for the input variable events, starting from the currently known
        //   possible start states for that loop. This allows input variables to change value. The result of executing
        //   that loop are the new possible states that the uncontrollable events loop can start in.
        // - We then similarly perform an event loop for the uncontrollable events, from its known possible start
        //   states. We then update the start states of the controllable events loop based on the states we can be in
        //   at the end of the uncontrollable events loop.
        // - We then similarly perform an event loop for the controllable events, from its known possible start states.
        //   We then update the start states of the input variable events loop based on the states we can be in at the
        //   end of the controllable events loop.

        // Get settings.
        Termination termination = cifBddSpec.settings.getTermination();
        DebugNormalOutput dbg = cifBddSpec.settings.getDebugOutput();

        // Initialize the start states, the states that we can be in before specific event loops. Note that
        // preconditions forbid state invariants, so they don't need to be taken into account when determining the
        // possible initial states.
        BDD startStatesInput = cifBddSpec.initial.id();
        BDD startStatesUnctrl = cifBddSpec.factory.zero();
        BDD startStatesCtrl = cifBddSpec.factory.zero();

        // Compute more reachable states, per round. Each round constitutes an execution of the 'program code'.
        int round = 0;
        while (true) {
            // Start of new round.
            round++;

            if (round > 1) {
                dbg.line();
            }
            dbg.line("Round %,d:", round);
            dbg.inc();

            dbg.line("Pre input states: " + BddUtils.bddToStr(startStatesInput, cifBddSpec));
            dbg.line("Pre uncontrollables states: " + BddUtils.bddToStr(startStatesUnctrl, cifBddSpec));
            dbg.line("Pre controllables states: " + BddUtils.bddToStr(startStatesCtrl, cifBddSpec));

            termination.throwIfRequested();

            // Save the current start states for later, to detect fixed point after the round.
            BDD prevStartStatesInput = startStatesInput.id();
            BDD prevStartStatesUnctrl = startStatesUnctrl.id();
            BDD prevStartStatesCtrl = startStatesCtrl.id();

            termination.throwIfRequested();

            // Perform transitions for input variables, allowing them to change value.
            dbg.line();
            dbg.line("Applying event loop for input variable events:");
            dbg.inc();

            BDD endStatesInput = computeLoopEndStates(cifBddSpec, startStatesInput, edgesPerInputEvent,
                    "input variable");

            dbg.dec();
            termination.throwIfRequested();

            // Update start states for uncontrollable events loop.
            BDD newStartStatesUnctrl = startStatesUnctrl.id().orWith(endStatesInput);
            if (!newStartStatesUnctrl.equals(startStatesUnctrl)) {
                dbg.line();
                dbg.line("Pre uncontrollables states: %s -> %s", BddUtils.bddToStr(startStatesUnctrl, cifBddSpec),
                        BddUtils.bddToStr(newStartStatesUnctrl, cifBddSpec));
            }

            startStatesUnctrl.free();
            startStatesUnctrl = newStartStatesUnctrl;

            termination.throwIfRequested();

            // Perform transitions for uncontrollable events.
            dbg.line();
            dbg.line("Applying event loop for uncontrollable events:");
            dbg.inc();

            BDD endStatesUnctrl = computeLoopEndStates(cifBddSpec, startStatesUnctrl, edgesPerUnctrlEvent,
                    "uncontrollable");

            dbg.dec();
            termination.throwIfRequested();

            // Update start states for controllable events loop.
            BDD newStartStatesCtrl = startStatesCtrl.id().orWith(endStatesUnctrl);
            if (!newStartStatesCtrl.equals(startStatesCtrl)) {
                dbg.line();
                dbg.line("Pre controllables states: %s -> %s", BddUtils.bddToStr(startStatesCtrl, cifBddSpec),
                        BddUtils.bddToStr(newStartStatesCtrl, cifBddSpec));
            }

            startStatesCtrl.free();
            startStatesCtrl = newStartStatesCtrl;

            termination.throwIfRequested();

            // Perform transitions for controllable events.
            dbg.line();
            dbg.line("Applying event loop for controllable events:");
            dbg.inc();

            BDD endStatesCtrl = computeLoopEndStates(cifBddSpec, startStatesCtrl, edgesPerCtrlEvent, "controllable");

            dbg.dec();
            termination.throwIfRequested();

            // Update start states for input variable events loop.
            BDD newStartStatesInput = startStatesInput.id().orWith(endStatesCtrl);
            if (!newStartStatesInput.equals(startStatesInput)) {
                dbg.line();
                dbg.line("Pre input states: %s -> %s", BddUtils.bddToStr(startStatesInput, cifBddSpec),
                        BddUtils.bddToStr(newStartStatesInput, cifBddSpec));
            }

            startStatesInput.free();
            startStatesInput = newStartStatesInput;

            termination.throwIfRequested();

            // Done with round.
            dbg.dec();

            // Check for fixed point.
            boolean fixedPointReached = prevStartStatesInput.equals(startStatesInput)
                    && prevStartStatesUnctrl.equals(startStatesUnctrl) && prevStartStatesCtrl.equals(startStatesCtrl);

            prevStartStatesInput.free();
            prevStartStatesUnctrl.free();
            prevStartStatesCtrl.free();

            if (fixedPointReached) {
                break;
            }
        }

        // Cleanup.
        startStatesInput.free();

        // Return result.
        return pair(startStatesUnctrl, startStatesCtrl);
    }

    /**
     * Compute the end states we can be in after executing a single full event loop from the given start states, taking
     * into account the execution scheme prescribed by the controller properties checker, and considering only the given
     * edges. The end states may or may not include the start states.
     *
     * @param cifBddSpec The CIF/BDD specification.
     * @param startStates The start states. Predicate is not freed by this method.
     * @param edgesPerEvent The edges to consider, per event. The events and edges are in the order prescribed by the
     *     controller properties checker, where applicable.
     * @param eventKindTxt Description of the kinds of events being considered.
     * @return The end states.
     */
    private BDD computeLoopEndStates(CifBddSpec cifBddSpec, BDD startStates, Map<Event, List<CifBddEdge>> edgesPerEvent,
            String eventKindTxt)
    {
        // The algorithm works as follows:
        // - The algorithm is similar to that of computing the bound, but with a small difference. Besides tracking
        //   which states we can currently be in, at each point of the execution, we also track the states we can be in
        //   at the ends of different iterations. This is required due the possibility of having cycles in the event
        //   sequences of the specification.
        // - If for example in one iteration a variable 'x' is inverted from 'true' to 'false', and in the next
        //   iteration it is inverted again from 'false' to 'true', then each iteration ends in different states than
        //   the previous one. We thus can't determine a fixed point that way.
        // - This is not a problem when computing the bound, as then we know that every state in the cycle is present,
        //   and thus they all go to the next state in the cycle, and we still get a fixed point. But here, we are
        //   computing the loop start states, so we don't  have them yet, and we thus can't rely on them yet.
        // - For this method, we compute all possible end states of all iterations of the loop, and detect a fixed point
        //   for that. This fixed point will be reached, since we can't forever reach new end states for next
        //   iterations.
        // - Note that we use the 'all iterations end states' to detect the fixed point, but still we return only the
        //   end states we reached after the last iteration as the result of this method.

        // Get settings.
        Termination termination = cifBddSpec.settings.getTermination();
        DebugNormalOutput dbg = cifBddSpec.settings.getDebugOutput();

        // Special case for no events.
        if (edgesPerEvent.isEmpty()) {
            dbg.line("No %s events.", eventKindTxt);
            return startStates.id();
        }

        termination.throwIfRequested();

        // Initialize computation.
        BDD curStates = startStates.id(); // States we can currently be in.
        BDD allItersEndStates = cifBddSpec.factory.zero(); // States we can be in at the ends of different iterations.

        // Consider repeated iterations of the event loop.
        int iteration = 0; // Number of iterations done so far.
        while (true) {
            // Start a new iteration.
            iteration++;

            if (iteration > 1) {
                dbg.line();
            }
            dbg.line("Iteration %,d (states before iteration: %s):", iteration,
                    BddUtils.bddToStr(curStates, cifBddSpec));
            dbg.inc();

            termination.throwIfRequested();

            // Save the 'all iterations end states' for later, to detect fixed point after the iteration.
            BDD preIterAllItersEndStates = allItersEndStates.id();

            // For this iteration, consider each event, in the correct order.
            Pair<BDD, List<CifBddEdge>> iterResults = computeIterEndStates(cifBddSpec, curStates, edgesPerEvent);

            termination.throwIfRequested();

            curStates.free();
            curStates = iterResults.left;

            // Update 'all iterations end states', the states we can be in after the event loop, for all the different
            // iterations.
            allItersEndStates = allItersEndStates.orWith(curStates.id());
            dbg.line("All iterations end states: " + BddUtils.bddToStr(allItersEndStates, cifBddSpec));

            termination.throwIfRequested();

            // End of iteration.
            dbg.dec();

            // Detect fixed point.
            boolean fixedPoint = allItersEndStates.equalsBDD(preIterAllItersEndStates);
            preIterAllItersEndStates.free();
            if (fixedPoint) {
                break;
            }
        }

        // Cleanup.
        allItersEndStates.free();

        // Return the states we can be in after the last iteration of the event loop.
        return curStates;
    }

    /**
     * Compute the end states we can be in after executing a single iteration of an event loop from the given start
     * states, taking into account the execution scheme prescribed by the controller properties checker, and considering
     * only the given edges. The end states may or may not include the start states.
     *
     * @param cifBddSpec The CIF/BDD specification.
     * @param startStates The start states. Predicate is not freed by this method.
     * @param edgesPerEvent The edges to consider, per event. The events and edges are in the order prescribed by the
     *     controller properties checker, where applicable.
     * @return The end states and the edges that were enabled during this iteration.
     */
    private Pair<BDD, List<CifBddEdge>> computeIterEndStates(CifBddSpec cifBddSpec, BDD startStates,
            Map<Event, List<CifBddEdge>> edgesPerEvent)
    {
        // The algorithm works as follows:
        // - To match the execution scheme prescribed by the controller properties checker, we try each event in the
        //   prescribed order, and execute at most one transition for it per iteration.
        // - The states in which an event can start, are:
        //   - For the first event: the given start states.
        //   - For all other events: the states in which the previous event could end.
        // - To take an edge for an event, we consider the states we can be in before taking the edge. We compute the
        //   states in which we can end up by taking the edge. We also consider the states we can remain in, if the
        //   guard of the edge is not enabled. We combine them, to get the states after possibly taking a transition for
        //   the edge. We then continue with the next edge for the same event, but only from the states in which the
        //   previous edges for that same event were not enabled. The result of possibly applying at most one transition
        //   for the event is the combination of all the possible target states in which we can end up by applying the
        //   different edges (for different source states), and the states in which we may remain because none of the
        //   guards of the edges for the event is enabled.

        // Get settings.
        Termination termination = cifBddSpec.settings.getTermination();
        DebugNormalOutput dbg = cifBddSpec.settings.getDebugOutput();

        // Initialize computation.
        BDD curStates = startStates.id(); // States we can currently be in.

        // For this iteration, consider each event, in the correct order.
        List<CifBddEdge> enabledEdges = list();
        boolean anyChange = false; // Did 'current states' change? Only updated if verbose debug output is disabled.
        for (Entry<Event, List<CifBddEdge>> entry: edgesPerEvent.entrySet()) {
            // Get event and its possible edges.
            Event event = entry.getKey();
            List<CifBddEdge> edges = entry.getValue();

            // Show user what event is being considered.
            if (VERBOSE_DEBUG_OUTPUT) {
                dbg.line("Event: " + CifTextUtils.getAbsName(event, false));
                dbg.inc();
            }

            // Initialize for trying edges for this event.
            BDD preEventCurStates = curStates.id();
            BDD eventPreStates = curStates;
            BDD eventPostStates = cifBddSpec.factory.zero();

            // Try each edge for the event, stopping after the first one that can be applied for that state. So, if we
            // have three states before considering the event, and the first edge applies to one of the states, we still
            // try the second edge for the remaining two states. If that edge also applies to one of the states, then we
            // still try the third edge for the remaining one state. And so on.
            for (CifBddEdge edge: edges) {
                // Show user what edge is being considered.
                if (VERBOSE_DEBUG_OUTPUT) {
                    dbg.line("Edge: " + edge.toString());
                    dbg.inc();
                }

                termination.throwIfRequested();

                // Determine the pre-event states where this edge is enabled. Consider only those pre-event states where
                // none of the previous edges for this event were enabled.
                BDD edgeEnabledStates = eventPreStates.and(edge.guard);

                if (VERBOSE_DEBUG_OUTPUT) {
                    dbg.line("Enabled in states: " + BddUtils.bddToStr(edgeEnabledStates, cifBddSpec));
                }

                termination.throwIfRequested();

                // Add edge to enabled edges of this iteration, if it is enabled.
                boolean isEdgeEnabled = !edgeEnabledStates.isZero();
                if (isEdgeEnabled) {
                    enabledEdges.add(edge);
                }

                // Get states we can be in by taking the edge.
                BDD restriction = null;
                BDD edgeTakenStates = edge.apply(edgeEnabledStates, CifBddEdgeApplyDirection.FORWARD, restriction);

                if (VERBOSE_DEBUG_OUTPUT) {
                    dbg.line("States after taking this edge: " + BddUtils.bddToStr(edgeTakenStates, cifBddSpec));
                }

                termination.throwIfRequested();

                // Get states we can be in, and thus remain in, if the edge is not enabled.
                BDD notGuard = edge.guard.not();
                BDD edgeNotTakenStates = eventPreStates.id().andWith(notGuard);

                if (VERBOSE_DEBUG_OUTPUT) {
                    dbg.line("States after not taking this edge: " + BddUtils.bddToStr(edgeNotTakenStates, cifBddSpec));
                }

                termination.throwIfRequested();

                // Update states we can be in after taking edges for this event.
                eventPostStates = eventPostStates.orWith(edgeTakenStates);

                if (VERBOSE_DEBUG_OUTPUT) {
                    dbg.line("States after taking any edge for the event: "
                            + BddUtils.bddToStr(eventPostStates, cifBddSpec));
                }

                termination.throwIfRequested();

                // Update states we can be in before taking this event, that are still to be considered by next edges
                // for this event.
                eventPreStates.free();
                eventPreStates = edgeNotTakenStates;

                if (VERBOSE_DEBUG_OUTPUT) {
                    dbg.line("States still to consider for taking edges for the event: "
                            + BddUtils.bddToStr(eventPreStates, cifBddSpec));
                }

                // Done with this edge.
                if (VERBOSE_DEBUG_OUTPUT) {
                    dbg.dec();
                }
            }

            // Compute states we can be in after possibly taking any edge for this event, so the possible target states
            // reached by taking a single transition for the event, as well as the states we remain in if none of the
            // edges for the event are enabled.
            curStates = eventPreStates.orWith(eventPostStates);

            if (VERBOSE_DEBUG_OUTPUT) {
                dbg.line("States after considering, and thus possibly taking an edge, for the event: "
                        + BddUtils.bddToStr(curStates, cifBddSpec));
            } else if (!curStates.equals(preEventCurStates)) {
                dbg.line("Event %s: %s -> %s", CifTextUtils.getAbsName(event, false),
                        BddUtils.bddToStr(preEventCurStates, cifBddSpec), BddUtils.bddToStr(curStates, cifBddSpec));
                anyChange = true;
            }

            preEventCurStates.free();

            // Done with this event.
            if (VERBOSE_DEBUG_OUTPUT) {
                dbg.dec();
            }
        }

        // Ensure something is printed.
        if (!VERBOSE_DEBUG_OUTPUT && !anyChange) {
            dbg.line("%,d edge%s enabled. No state changes.", enabledEdges.size(),
                    (enabledEdges.size() == 1) ? "" : "s");
        }

        // Return the states we can be in after the iteration, and the edges that were enabled during this iteration.
        return pair(curStates, enabledEdges);
    }

    /**
     * Compute bounded for an event loop, taking into account the execution scheme prescribed by the controller
     * properties checker, by considering only the given edges.
     *
     * @param cifBddSpec The CIF/BDD specification.
     * @param startStates The start states. Predicate is not freed by this method.
     * @param edgesPerEvent The edges to consider, per event. The events and edges are in the order prescribed by the
     *     controller properties checker, where applicable.
     * @param eventKindTxt Description of the kinds of events being considered.
     * @return The computed bound.
     * @throws UnsupportedException If the bound is so high, it can't be represented as a integer.
     */
    private Bound computeLoopBound(CifBddSpec cifBddSpec, BDD startStates, Map<Event, List<CifBddEdge>> edgesPerEvent,
            String eventKindTxt)
    {
        // The algorithm works as follows:
        // - We start with all states we can be in before the event loop, to account for execution starting in any of
        //   those states.
        // - If there are no events, the bounds are trivially '0'. Otherwise, the current states are those states where
        //   we can be after applying zero transitions from the start of the execution (bound zero).
        // - We find the bound by in each iteration taking at most one transition for the relevant events. In each
        //   iteration, we thus try to take a transition per event, and we end up with the states we can be in after
        //   that iteration.
        // - If the specification has bounded response, all sequences are of finite length, and at some point they end.
        //   That is, after enough iterations, given the length of the sequence and the transition execution order, no
        //   more transitions can be executed for the events. If in iteration 'n' no more transitions can be executed,
        //   then the possible states we can be in before and after that iteration are the same, and we have reached a
        //   fixed point. Since no transitions were executed in iteration 'n', 'n - 1' iterations is then enough, and
        //   the bound is thus 'n - 1'.
        // - If the specification does not have bounded response (assuming the execution scheme prescribed by the
        //   controller properties checker), then there is at least one cycle of states. The states within a cycle will
        //   with one transition go to the next state in their cycle. Cycles are thus preserved with each iteration. If
        //   all bounded sequences at some point end, then only the cycles (there could be more of them) remain. Then a
        //   fixed point is reached, but since transitions can still be executed, there is no bounded response.

        // Get settings.
        Termination termination = cifBddSpec.settings.getTermination();
        DebugNormalOutput dbg = cifBddSpec.settings.getDebugOutput();

        // Special case for no events.
        if (edgesPerEvent.isEmpty()) {
            dbg.line("No %s events.", eventKindTxt);
            return new Bound(true, true, 0, null, null);
        }

        termination.throwIfRequested();

        // Initialize computation.
        BDD curStates = startStates.id(); // States we can currently be in.

        // Consider repeated iterations of the event loop.
        Integer iteration = 0; // Number of iterations done so far. Becomes 'null' if no bounded response.
        List<CifBddEdge> enabledEdges = Collections.emptyList(); // The enabled edges of the last iteration.
        while (true) {
            // Start a new iteration. Detect too many iterations (integer overflow).
            iteration++;
            if (iteration < 0) {
                throw new UnsupportedException("Failed to compute bounded response, as the bound is too high.");
            }

            if (iteration > 1) {
                dbg.line();
            }
            dbg.line("Iteration %,d (states before iteration: %s):", iteration,
                    BddUtils.bddToStr(curStates, cifBddSpec));
            dbg.inc();

            termination.throwIfRequested();

            // Save the current states for later, to detect fixed point after the iteration.
            BDD preIterCurStates = curStates.id();

            // For this iteration, consider each event, in the correct order.
            Pair<BDD, List<CifBddEdge>> iterResults = computeIterEndStates(cifBddSpec, curStates, edgesPerEvent);

            termination.throwIfRequested();

            curStates.free();
            curStates = iterResults.left;
            enabledEdges = iterResults.right;

            // End of iteration.
            dbg.dec();

            // Detect fixed point.
            boolean fixedPoint = curStates.equalsBDD(preIterCurStates);
            preIterCurStates.free();
            if (fixedPoint) {
                break;
            }
        }

        // Determine the result.
        Bound result;
        if (enabledEdges.isEmpty()) {
            // No cycles, so bounded response.
            result = new Bound(true, true, iteration - 1, null, null);
        } else {
            // Cycles found, so no bounded response.

            // Get enabled edges texts.
            Assert.check(!enabledEdges.isEmpty());
            List<String> enabledEdgesTexts = enabledEdges.stream().map(e -> e.toString("", true)).toList();

            // Get 'printed cycle states' texts. Ask for one more 'printed cycle state' than we want, to be able to
            // detect whether we have all 'printed cycle states' or not.
            int askedPrintedCycleStatesCount = maxPrintedCycleStatesCount + 1;
            List<Map<CifBddVariable, Object>> printedCycleStatesMaps = BddUtils.bddToStates(curStates, cifBddSpec,
                    false, askedPrintedCycleStatesCount);
            termination.throwIfRequested();

            boolean complete = printedCycleStatesMaps.size() < askedPrintedCycleStatesCount;
            if (!complete) {
                // Remove the one extra 'printed cycle state'.
                printedCycleStatesMaps = slice(printedCycleStatesMaps, 0, -1);
            }

            List<String> printCycleStatesTxts = printedCycleStatesMaps.stream()
                    .map(s -> BddUtils.stateToStr(s, "(empty state; no variables)")).collect(Lists.toList());
            termination.throwIfRequested();
            if (!complete) {
                printCycleStatesTxts.add("...");
            }

            // Set result.
            result = new Bound(true, false, null, enabledEdgesTexts, printCycleStatesTxts);
        }

        // Cleanup.
        curStates.free();

        // Return the result.
        return result;
    }
}
