//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.checks;

import java.util.Map;
import java.util.function.Function;

import org.eclipse.escet.cif.cif2cif.ElimAlgVariables;
import org.eclipse.escet.cif.cif2cif.ElimConsts;
import org.eclipse.escet.cif.cif2cif.ElimIfUpdates;
import org.eclipse.escet.cif.cif2cif.ElimLocRefExprs;
import org.eclipse.escet.cif.cif2cif.ElimMonitors;
import org.eclipse.escet.cif.cif2cif.ElimSelf;
import org.eclipse.escet.cif.cif2cif.ElimStateEvtExclInvs;
import org.eclipse.escet.cif.cif2cif.ElimTypeDecls;
import org.eclipse.escet.cif.cif2cif.EnumsToInts;
import org.eclipse.escet.cif.cif2cif.SimplifyValues;
import org.eclipse.escet.cif.controllercheck.ControllerCheckerSettings;
import org.eclipse.escet.cif.controllercheck.mdd.CifMddSpec;
import org.eclipse.escet.cif.controllercheck.mdd.MddDeterminismChecker;
import org.eclipse.escet.cif.controllercheck.mdd.MddPreChecker;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.automata.Location;
import org.eclipse.escet.cif.metamodel.cif.declarations.DiscVariable;
import org.eclipse.escet.common.emf.EMFHelper;
import org.eclipse.escet.common.java.Termination;
import org.eclipse.escet.common.java.output.DebugNormalOutput;

/**
 * An MDD-based check that can be performed by the controller properties checker.
 *
 * @param <T> The type of the conclusion of the check.
 */
public abstract class ControllerCheckerMddBasedCheck<T extends CheckConclusion> implements ControllerCheckerCheck<T> {
    @Override
    public T performCheck(Specification spec, String specAbsPath, ControllerCheckerSettings checkerSettings) {
        Termination termination = checkerSettings.getTermination();
        DebugNormalOutput debugOutput = checkerSettings.getDebugOutput();

        // Convert specification to an MDD representation.
        debugOutput.line("Converting CIF specification to an MDD representation:");
        debugOutput.inc();

        CifMddSpec cifMddSpec = convertToMdd(spec, specAbsPath, termination, checkerSettings.getNormalOutput(),
                checkerSettings.getDebugOutput());

        debugOutput.dec();
        debugOutput.line();
        termination.throwIfRequested();

        // Perform the check.
        return performCheck(cifMddSpec);
    }

    /**
     * Performs the check.
     *
     * @param cifMddSpec The MDD representation of the specification to check.
     * @return The check result.
     */
    protected abstract T performCheck(CifMddSpec cifMddSpec);

    /**
     * Convert a CIF specification to an MDD representation. Also performs some MDD-specific checks on the input
     * specification.
     *
     * @param spec The specification to convert. Must not be modified.
     * @param specAbsPath The absolute local file system path to the CIF specification to check.
     * @param termination Cooperative termination query function.
     * @param normalOutput Callback to send normal output to the user.
     * @param debugOutput Callback to send debug output to the user.
     * @return The CIF/BDD specification.
     */
    private static CifMddSpec convertToMdd(Specification spec, String specAbsPath, Termination termination,
            DebugNormalOutput normalOutput, DebugNormalOutput debugOutput)
    {
        // Use a copy of the specification.
        spec = EMFHelper.deepclone(spec);
        termination.throwIfRequested();

        // Pre-processing.
        // CIF automata structure normalization.
        new ElimStateEvtExclInvs().transform(spec);
        new ElimMonitors().transform(spec);
        new ElimSelf().transform(spec);
        new ElimTypeDecls().transform(spec);

        final Function<Automaton, String> varNamingFunction = a -> "LP_" + a.getName();
        final Function<Automaton, String> enumNamingFunction = a -> "LOCS_" + a.getName();
        final Function<Location, String> litNamingFunction = l -> "LOC_" + l.getName();
        final boolean considerLocsForRename = true;
        final boolean addInitPreds = true;
        final boolean optimized = false;
        final Map<DiscVariable, String> lpVarToAbsAutNameMap = null;
        final boolean optInits = true;
        final boolean addEdgeGuards = true;
        final boolean copyAutAnnosToEnum = false;
        final boolean copyLocAnnosToEnumLits = false;
        new ElimLocRefExprs(varNamingFunction, enumNamingFunction, litNamingFunction, considerLocsForRename,
                addInitPreds, optimized, lpVarToAbsAutNameMap, optInits, addEdgeGuards, copyAutAnnosToEnum,
                copyLocAnnosToEnumLits).transform(spec);

        new EnumsToInts().transform(spec);
        termination.throwIfRequested();

        // Simplify expressions.
        new ElimAlgVariables().transform(spec);
        new ElimConsts().transform(spec);
        new SimplifyValues().transform(spec);
        termination.throwIfRequested();

        // Check preconditions.
        new MddPreChecker(termination)
                .reportPreconditionViolations(spec, specAbsPath, "CIF controller properties checker");
        termination.throwIfRequested();

        // Eliminate if updates. This does not support multi-assignments or partial variable assignments.
        new ElimIfUpdates().transform(spec);
        termination.throwIfRequested();

        // Non-determinism check.
        new MddDeterminismChecker(termination)
                .reportPreconditionViolations(spec, specAbsPath, "CIF controller properties checker");
        termination.throwIfRequested();

        // Create MDD representation.
        CifMddSpec cifMddSpec = new CifMddSpec(termination, normalOutput, debugOutput);
        cifMddSpec.compute(spec);
        termination.throwIfRequested();

        // Return MDD representation.
        return cifMddSpec;
    }
}
