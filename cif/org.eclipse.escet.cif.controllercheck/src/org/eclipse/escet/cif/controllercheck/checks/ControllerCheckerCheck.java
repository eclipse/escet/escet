//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.checks;

import org.eclipse.escet.cif.controllercheck.ControllerCheckerSettings;
import org.eclipse.escet.cif.metamodel.cif.Specification;

/**
 * A check that can be performed by the controller properties checker.
 *
 * @param <T> The type of the conclusion of the check.
 */
public interface ControllerCheckerCheck<T extends CheckConclusion> {
    /**
     * Returns the name of the property to check.
     *
     * @return The property name. Starts with a lower-case letter and does not end with a period.
     */
    public String getPropertyName();

    /**
     * Performs the check.
     *
     * @param spec The CIF specification on which to perform the check. Must not be modified.
     * @param specAbsPath The absolute local file system path to the CIF file.
     * @param checkerSettings The controller properties checker settings.
     * @return The check result.
     */
    public T performCheck(Specification spec, String specAbsPath, ControllerCheckerSettings checkerSettings);
}
