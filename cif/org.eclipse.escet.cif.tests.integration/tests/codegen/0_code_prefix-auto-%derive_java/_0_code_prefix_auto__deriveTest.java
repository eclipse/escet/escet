package 0_code_prefix-auto-%derive_java;

import static 0_code_prefix-auto-%derive_java._0_code_prefix_auto__derive._0_code_prefix_auto__deriveUtils.*;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/** Test class for the {@link _0_code_prefix_auto__derive} class. */
@SuppressWarnings("unused")
public class _0_code_prefix_auto__deriveTest extends _0_code_prefix_auto__derive {
    /**
     * Main method for the {@link _0_code_prefix_auto__deriveTest} class.
     *
     * @param args The command line arguments. Ignored.
     */
    public static void main(String[] args) {
        _0_code_prefix_auto__derive code = new _0_code_prefix_auto__deriveTest();
        code.exec(100);
    }

    @Override
    protected void updateInputs() {
        // No input variables.
    }

    @Override
    protected void infoExec(long duration, long cycleTime) {
        if (duration > cycleTime) {
            String text = fmt("Cycle time exceeded: %,d ns > %,d ns.", duration, cycleTime);
            System.err.println(text);
        }
    }

    @Override
    protected void infoEvent(int idx, boolean pre) {
        // Print the executed event, for testing.
        if (pre) {
            System.out.println("EVENT pre: " + getEventName(idx));
        } else {
            System.out.println("EVENT post: " + getEventName(idx));
        }
    }

    @Override
    protected void infoPrintOutput(String text, String target) {
        if (target.equals(":stdout")) {
            System.out.println(text);
            System.out.flush();

        } else if (target.equals(":stderr")) {
            System.err.println(text);
            System.err.flush();

        } else {
            System.out.println(normalizePrintTarget(target) + ": " + text);
            System.out.flush();
        }
    }

    @Override
    protected void preExec() {
        // Ignore.
    }

    @Override
    protected void postExec() {
        // Could for instance write outputs to the environment.
    }
}
