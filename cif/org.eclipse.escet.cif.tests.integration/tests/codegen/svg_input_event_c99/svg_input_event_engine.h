/* Headers for the CIF to C translation of svg_input_event.cif
 * Generated file, DO NOT EDIT
 */

#ifndef CIF_C_SVG_INPUT_EVENT_ENGINE_H
#define CIF_C_SVG_INPUT_EVENT_ENGINE_H

#include "svg_input_event_library.h"

/* Types of the specification.
 * Note that integer ranges are ignored in C.
 */
enum Enumsvg_input_event_ {
    /** Literal "l1". */
    _svg_input_event_l1,

    /** Literal "l2". */
    _svg_input_event_l2,
};
typedef enum Enumsvg_input_event_ svg_input_eventEnum;

extern const char *enum_names[];
extern int EnumTypePrint(svg_input_eventEnum value, char *dest, int start, int end);


/* Event declarations. */
enum svg_input_eventEventEnum_ {
    /** Initial step. */
    EVT_INITIAL_,

    /** Delay step. */
    EVT_DELAY_,

    /** Event "a". */
    a_,

    /** Event "b1". */
    b1_,

    /** Event "b2". */
    b2_,

    /** Event "c". */
    c_,

    /** Event "d". */
    d_,

    /** Event "e". */
    e_,

    /** Event "f". */
    f_,

    /** Event "g.z". */
    g_z_,
};
typedef enum svg_input_eventEventEnum_ svg_input_event_Event_;

/** Names of all the events. */
extern const char *svg_input_event_event_names[];

/* Constants. */


/* Input variables. */




/* Declaration of internal functions. */


/* State variables (use for output only). */
extern RealType model_time; /**< Current model time. */

/** Discrete variable "E aut_b". */
extern svg_input_eventEnum aut_b_;

/** Discrete variable "int aut_d.vd". */
extern IntType aut_d_vd_;

/** Discrete variable "bool aut_f.b". */
extern BoolType aut_f_b_;

/** Discrete variable "bool g.a.b". */
extern BoolType g_a_b_;

/** Discrete variable "real x6.x". */
extern RealType x6_x_;

/* Algebraic and derivative functions (use for output only). */






/* Code entry points. */
void svg_input_event_EngineFirstStep(void);
void svg_input_event_EngineTimeStep(double delta);

#if EVENT_OUTPUT
/**
 * External callback function reporting about the execution of an event.
 * @param event Event being executed.
 * @param pre If \c TRUE, event is about to be executed. If \c FALSE, event has been executed.
 * @note Function must be implemented externally.
 */
extern void svg_input_event_InfoEvent(svg_input_event_Event_ event, BoolType pre);
#endif

#if PRINT_OUTPUT
/**
 * External callback function to output the given text-line to the given filename.
 * @param text Text to print (does not have a EOL character).
 * @param fname Name of the file to print to.
 */
extern void svg_input_event_PrintOutput(const char *text, const char *fname);
#endif

#endif

