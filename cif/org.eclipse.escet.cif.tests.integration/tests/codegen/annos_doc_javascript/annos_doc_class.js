/** Tuples. */


/**
 * annos_doc code generated from a CIF specification.
 *
 * <p>
 * First doc
 * with multiple lines.
 * </p>
 *
 * <p>
 * Second doc line 1.
 * Second doc line 2.
 * </p>
 */
class annos_doc_class {
    /** annos_docEnum declaration. It contains the single merged enum from the CIF model. */
    annos_docEnum = Object.freeze({
        /** Literal "l1". */
        _l1: Symbol("l1"),

        /**
         * Literal "l2".
         *
         * <p>
         * single line doc
         * </p>
         */
        _l2: Symbol("l2"),

        /**
         * Literal "l3".
         *
         * <p>
         * doc with multiple
         * lines of
         *  text
         * </p>
         */
        _l3: Symbol("l3"),

        /**
         * Literal "l4".
         *
         * <p>
         * some doc
         * </p>
         */
        _l4: Symbol("l4"),

        /**
         * Literal "l5".
         *
         * <p>
         * First doc.
         * </p>
         *
         * <p>
         * Second doc line 1.
         * Second doc line 2.
         * </p>
         */
        _l5: Symbol("l5")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "e",
        "events.e1",
        "events.e2",
        "events.e3",
        "events.e4",
        "events.e5"
    ];

    /** Constant "constants.c1". */
    constants_c1_;

    /**
     * Constant "constants.c2".
     *
     * <p>
     * single line doc
     * </p>
     */
    constants_c2_;

    /**
     * Constant "constants.c3".
     *
     * <p>
     * doc with multiple
     * lines of
     *  text
     * </p>
     */
    constants_c3_;

    /**
     * Constant "constants.c4".
     *
     * <p>
     * some doc
     * </p>
     */
    constants_c4_;

    /**
     * Constant "constants.c5".
     *
     * <p>
     * First doc.
     * </p>
     *
     * <p>
     * Second doc line 1.
     * Second doc line 2.
     * </p>
     */
    constants_c5_;

    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Continuous variable "contvars.c1". */
    contvars_c1_;

    /**
     * Continuous variable "contvars.c2".
     *
     * <p>
     * single line doc
     * </p>
     */
    contvars_c2_;

    /**
     * Continuous variable "contvars.c3".
     *
     * <p>
     * doc with multiple
     * lines of
     *  text
     * </p>
     */
    contvars_c3_;

    /**
     * Continuous variable "contvars.c4".
     *
     * <p>
     * some doc
     * </p>
     */
    contvars_c4_;

    /**
     * Continuous variable "contvars.c5".
     *
     * <p>
     * First doc.
     * </p>
     *
     * <p>
     * Second doc line 1.
     * Second doc line 2.
     * </p>
     */
    contvars_c5_;

    /** Discrete variable "discvars.d1". */
    discvars_d1_;

    /**
     * Discrete variable "discvars.d2".
     *
     * <p>
     * single line doc
     * </p>
     */
    discvars_d2_;

    /**
     * Discrete variable "discvars.d3".
     *
     * <p>
     * doc with multiple
     * lines of
     *  text
     * </p>
     */
    discvars_d3_;

    /**
     * Discrete variable "discvars.d4".
     *
     * <p>
     * some doc
     * </p>
     */
    discvars_d4_;

    /**
     * Discrete variable "discvars.d5".
     *
     * <p>
     * First doc.
     * </p>
     *
     * <p>
     * Second doc line 1.
     * Second doc line 2.
     * </p>
     */
    discvars_d5_;

    /** Input variable "i1". */
    i1_;

    /**
     * Input variable "i2".
     *
     * <p>
     * single line doc
     * </p>
     */
    i2_;

    /**
     * Input variable "i3".
     *
     * <p>
     * doc with multiple
     * lines of
     *  text
     * </p>
     */
    i3_;

    /**
     * Input variable "i4".
     *
     * <p>
     * some doc
     * </p>
     */
    i4_;

    /**
     * Input variable "i5".
     *
     * <p>
     * First doc.
     * </p>
     *
     * <p>
     * Second doc line 1.
     * Second doc line 2.
     * </p>
     */
    i5_;

    /**
     * Input variable "preconditions.i".
     *
     * <p>
     * {}
     * </p>
     */
    preconditions_i_;

    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {annos_docException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        try {
                            var deriv0 = annos_doc.contvars_c1_deriv();
                            var deriv1 = annos_doc.contvars_c2_deriv();
                            var deriv2 = annos_doc.contvars_c3_deriv();
                            var deriv3 = annos_doc.contvars_c4_deriv();
                            var deriv4 = annos_doc.contvars_c5_deriv();

                            annos_doc.contvars_c1_ = annos_doc.contvars_c1_ + delta * deriv0;
                            annos_docUtils.checkReal(annos_doc.contvars_c1_, "contvars.c1");
                            if (annos_doc.contvars_c1_ == -0.0) annos_doc.contvars_c1_ = 0.0;
                            annos_doc.contvars_c2_ = annos_doc.contvars_c2_ + delta * deriv1;
                            annos_docUtils.checkReal(annos_doc.contvars_c2_, "contvars.c2");
                            if (annos_doc.contvars_c2_ == -0.0) annos_doc.contvars_c2_ = 0.0;
                            annos_doc.contvars_c3_ = annos_doc.contvars_c3_ + delta * deriv2;
                            annos_docUtils.checkReal(annos_doc.contvars_c3_, "contvars.c3");
                            if (annos_doc.contvars_c3_ == -0.0) annos_doc.contvars_c3_ = 0.0;
                            annos_doc.contvars_c4_ = annos_doc.contvars_c4_ + delta * deriv3;
                            annos_docUtils.checkReal(annos_doc.contvars_c4_, "contvars.c4");
                            if (annos_doc.contvars_c4_ == -0.0) annos_doc.contvars_c4_ = 0.0;
                            annos_doc.contvars_c5_ = annos_doc.contvars_c5_ + delta * deriv4;
                            annos_docUtils.checkReal(annos_doc.contvars_c5_, "contvars.c5");
                            if (annos_doc.contvars_c5_ == -0.0) annos_doc.contvars_c5_ = 0.0;
                        } catch (e) {
                            if (e instanceof annos_docException) {
                                e = new annos_docException("Failed to update continuous variables after time passage.", e);
                                this.runtimeError(e);
                            }
                            throw e;
                        }
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) annos_doc.log('Initial state: ' + annos_doc.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "e".
            anythingExecuted |= this.execEdge0();

            // Event "events.e1".
            anythingExecuted |= this.execEdge1();

            // Event "events.e2".
            anythingExecuted |= this.execEdge2();

            // Event "events.e3".
            anythingExecuted |= this.execEdge3();

            // Event "events.e4".
            anythingExecuted |= this.execEdge4();

            // Event "events.e5".
            anythingExecuted |= this.execEdge5();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {annos_docException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {annos_docException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (annos_doc.first) {
                    annos_doc.first = false;
                    annos_doc.startMilli = now;
                    annos_doc.targetMilli = annos_doc.startMilli;
                    preMilli = annos_doc.startMilli;
                }

                // Handle pausing/playing.
                if (!annos_doc.playing) {
                    annos_doc.timePaused = now;
                    return;
                }

                if (annos_doc.timePaused) {
                    annos_doc.startMilli += (now - annos_doc.timePaused);
                    annos_doc.targetMilli += (now - annos_doc.timePaused);
                    annos_doc.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = annos_doc.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - annos_doc.startMilli;

                // Execute once.
                annos_doc.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (annos_doc.doInfoExec) {
                    annos_doc.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    annos_doc.targetMilli += cycleMilli;
                    remainderMilli = annos_doc.targetMilli - postMilli;
                }

                // Execute again.
                annos_doc.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge0() {
        try {
            var guard;
            try {
                guard = ((((annos_doc.contvars_c1_) > (0)) || ((annos_doc.contvars_c2_) > (0))) || (((annos_doc.contvars_c3_) > (0)) || (((annos_doc.contvars_c4_) > (0)) || ((annos_doc.contvars_c5_) > (0))))) && (((annos_doc.discvars_d1_) || (annos_doc.discvars_d2_)) || ((annos_doc.discvars_d3_) || ((annos_doc.discvars_d4_) || (annos_doc.discvars_d5_))));
            } catch (e) {
                if (e instanceof annos_docException) {
                    e = new annos_docException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(0, true);
            if (this.doInfoEvent) this.infoEvent(0, true);

            if (this.doInfoEvent) this.infoEvent(0, false);
            if (this.doInfoPrintOutput) this.printOutput(0, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to (try to) execute event \"e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 1 and event "events.e1".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge1() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(1, true);
            if (this.doInfoEvent) this.infoEvent(1, true);

            if (this.doInfoEvent) this.infoEvent(1, false);
            if (this.doInfoPrintOutput) this.printOutput(1, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to (try to) execute event \"events.e1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 2 and event "events.e2".
     *
     * <p>
     * single line doc
     * </p>
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge2() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(2, true);
            if (this.doInfoEvent) this.infoEvent(2, true);

            if (this.doInfoEvent) this.infoEvent(2, false);
            if (this.doInfoPrintOutput) this.printOutput(2, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to (try to) execute event \"events.e2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 3 and event "events.e3".
     *
     * <p>
     * doc with multiple
     * lines of
     *  text
     * </p>
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge3() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(3, true);
            if (this.doInfoEvent) this.infoEvent(3, true);

            if (this.doInfoEvent) this.infoEvent(3, false);
            if (this.doInfoPrintOutput) this.printOutput(3, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to (try to) execute event \"events.e3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 4 and event "events.e4".
     *
     * <p>
     * some doc
     * </p>
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge4() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(4, true);
            if (this.doInfoEvent) this.infoEvent(4, true);

            if (this.doInfoEvent) this.infoEvent(4, false);
            if (this.doInfoPrintOutput) this.printOutput(4, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to (try to) execute event \"events.e4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 5 and event "events.e5".
     *
     * <p>
     * First doc.
     * </p>
     *
     * <p>
     * Second doc line 1.
     * Second doc line 2.
     * </p>
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge5() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(5, true);
            if (this.doInfoEvent) this.infoEvent(5, true);

            if (this.doInfoEvent) this.infoEvent(5, false);
            if (this.doInfoPrintOutput) this.printOutput(5, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to (try to) execute event \"events.e5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the state.
     *
     * @throws {annos_docException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;
            annos_doc.constants_c1_ = 1;
            annos_doc.constants_c2_ = 2;
            annos_doc.constants_c3_ = 3;
            annos_doc.constants_c4_ = 4;
            annos_doc.constants_c5_ = 5;
        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            annos_doc.contvars_c1_ = 0.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"contvars.c1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.contvars_c2_ = 0.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"contvars.c2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.contvars_c3_ = 0.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"contvars.c3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.contvars_c4_ = 0.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"contvars.c4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.contvars_c5_ = 0.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"contvars.c5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.discvars_d1_ = false;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"discvars.d1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.discvars_d2_ = false;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"discvars.d2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.discvars_d3_ = false;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"discvars.d3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.discvars_d4_ = false;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"discvars.d4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            annos_doc.discvars_d5_ = false;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the initial value of variable \"discvars.d5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {annos_docException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type annos_docException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof annos_docException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) annos_doc.log(annos_docUtils.fmt('Transition: event %s', annos_doc.getEventName(idx)));
        } else {
            if (this.doStateOutput) annos_doc.log('State: ' + annos_doc.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = annos_docUtils.fmt('time=%s', annos_doc.time);
        state += annos_docUtils.fmt(', contvars.c1=%s', annos_docUtils.valueToStr(annos_doc.contvars_c1_));
        state += annos_docUtils.fmt(', contvars.c1\'=%s', annos_docUtils.valueToStr(annos_doc.contvars_c1_deriv()));
        state += annos_docUtils.fmt(', contvars.c2=%s', annos_docUtils.valueToStr(annos_doc.contvars_c2_));
        state += annos_docUtils.fmt(', contvars.c2\'=%s', annos_docUtils.valueToStr(annos_doc.contvars_c2_deriv()));
        state += annos_docUtils.fmt(', contvars.c3=%s', annos_docUtils.valueToStr(annos_doc.contvars_c3_));
        state += annos_docUtils.fmt(', contvars.c3\'=%s', annos_docUtils.valueToStr(annos_doc.contvars_c3_deriv()));
        state += annos_docUtils.fmt(', contvars.c4=%s', annos_docUtils.valueToStr(annos_doc.contvars_c4_));
        state += annos_docUtils.fmt(', contvars.c4\'=%s', annos_docUtils.valueToStr(annos_doc.contvars_c4_deriv()));
        state += annos_docUtils.fmt(', contvars.c5=%s', annos_docUtils.valueToStr(annos_doc.contvars_c5_));
        state += annos_docUtils.fmt(', contvars.c5\'=%s', annos_docUtils.valueToStr(annos_doc.contvars_c5_deriv()));
        state += annos_docUtils.fmt(', discvars.d1=%s', annos_docUtils.valueToStr(annos_doc.discvars_d1_));
        state += annos_docUtils.fmt(', discvars.d2=%s', annos_docUtils.valueToStr(annos_doc.discvars_d2_));
        state += annos_docUtils.fmt(', discvars.d3=%s', annos_docUtils.valueToStr(annos_doc.discvars_d3_));
        state += annos_docUtils.fmt(', discvars.d4=%s', annos_docUtils.valueToStr(annos_doc.discvars_d4_));
        state += annos_docUtils.fmt(', discvars.d5=%s', annos_docUtils.valueToStr(annos_doc.discvars_d5_));
        return state;
    }


    /**
     * Evaluates algebraic variable "algvars.a1".
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    algvars_a1_() {
        try {
            return 1;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate algebraic variable \"algvars.a1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "algvars.a2".
     *
     * <p>
     * single line doc
     * </p>
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    algvars_a2_() {
        try {
            return 2;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate algebraic variable \"algvars.a2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "algvars.a3".
     *
     * <p>
     * doc with multiple
     * lines of
     *  text
     * </p>
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    algvars_a3_() {
        try {
            return 3;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate algebraic variable \"algvars.a3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "algvars.a4".
     *
     * <p>
     * some doc
     * </p>
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    algvars_a4_() {
        try {
            return 4;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate algebraic variable \"algvars.a4\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "algvars.a5".
     *
     * <p>
     * First doc.
     * </p>
     *
     * <p>
     * Second doc line 1.
     * Second doc line 2.
     * </p>
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    algvars_a5_() {
        try {
            return 5;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate algebraic variable \"algvars.a5\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "contvars.c1".
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    contvars_c1_deriv() {
        try {
            return 1.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the derivative of continuous variable \"contvars.c1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "contvars.c2".
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    contvars_c2_deriv() {
        try {
            return 2.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the derivative of continuous variable \"contvars.c2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "contvars.c3".
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    contvars_c3_deriv() {
        try {
            return 3.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the derivative of continuous variable \"contvars.c3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "contvars.c4".
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    contvars_c4_deriv() {
        try {
            return 4.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the derivative of continuous variable \"contvars.c4\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "contvars.c5".
     *
     * @return The evaluation result.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    contvars_c5_deriv() {
        try {
            return 5.0;
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to evaluate the derivative of continuous variable \"contvars.c5\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "funcs.func1".
     *
     * @param funcs_func1_p_ Function parameter "funcs.func1.p".
     * @return The return value of the function.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    funcs_func1_(funcs_func1_p_) {
        try {
            // Variable "funcs.func1.v1".
            var funcs_func1_v1_ = funcs_func1_p_;

            // Variable "funcs.func1.v2".
            var funcs_func1_v2_ = funcs_func1_v1_;

            // Execute statements in the function body.
            return funcs_func1_v2_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to execute internal user-defined function \"funcs.func1\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "funcs.func2".
     *
     * <p>
     * single line doc
     * </p>
     *
     * @param funcs_func2_p_ Function parameter "funcs.func2.p".
     *     <p>
     *     single line doc
     *     </p>
     * @return The return value of the function.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    funcs_func2_(funcs_func2_p_) {
        try {
            // Variable "funcs.func2.v1".
            //
            // single line doc
            var funcs_func2_v1_ = funcs_func2_p_;

            // Variable "funcs.func2.v2".
            //
            // single line doc
            var funcs_func2_v2_ = funcs_func2_v1_;

            // Execute statements in the function body.
            return funcs_func2_v2_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to execute internal user-defined function \"funcs.func2\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "funcs.func3".
     *
     * <p>
     * doc with multiple
     * lines of
     *  text
     * </p>
     *
     * @param funcs_func3_p_ Function parameter "funcs.func3.p".
     *     <p>
     *     doc with multiple
     *     lines of
     *      text
     *     </p>
     * @return The return value of the function.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    funcs_func3_(funcs_func3_p_) {
        try {
            // Variable "funcs.func3.v1".
            //
            // doc with multiple
            // lines of
            //  text
            var funcs_func3_v1_ = funcs_func3_p_;

            // Variable "funcs.func3.v2".
            //
            // doc with multiple
            // lines of
            //  text
            var funcs_func3_v2_ = funcs_func3_v1_;

            // Execute statements in the function body.
            return funcs_func3_v2_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to execute internal user-defined function \"funcs.func3\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "funcs.func4".
     *
     * <p>
     * some doc
     * </p>
     *
     * @param funcs_func4_p_ Function parameter "funcs.func4.p".
     *     <p>
     *     some doc
     *     </p>
     * @return The return value of the function.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    funcs_func4_(funcs_func4_p_) {
        try {
            // Variable "funcs.func4.v1".
            //
            // some doc
            var funcs_func4_v1_ = funcs_func4_p_;

            // Variable "funcs.func4.v2".
            //
            // some doc
            var funcs_func4_v2_ = funcs_func4_v1_;

            // Execute statements in the function body.
            return funcs_func4_v2_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to execute internal user-defined function \"funcs.func4\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "funcs.func5".
     *
     * <p>
     * First doc.
     * </p>
     *
     * <p>
     * Second doc line 1.
     * Second doc line 2.
     * </p>
     *
     * @param funcs_func5_p_ Function parameter "funcs.func5.p".
     *     <p>
     *     First doc.
     *     </p>
     *     <p>
     *     Second doc line 1.
     *     Second doc line 2.
     *     </p>
     * @return The return value of the function.
     * @throws {annos_docException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    funcs_func5_(funcs_func5_p_) {
        try {
            // Variable "funcs.func5.v1".
            //
            // First doc.
            //
            // Second doc line 1.
            // Second doc line 2.
            var funcs_func5_v1_ = funcs_func5_p_;

            // Variable "funcs.func5.v2".
            //
            // First doc.
            //
            // Second doc line 1.
            // Second doc line 2.
            var funcs_func5_v2_ = funcs_func5_v1_;

            // Execute statements in the function body.
            return funcs_func5_v2_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof annos_docException) {
                e = new annos_docException("Failed to execute internal user-defined function \"funcs.func5\".", e);
            }
            throw e;
        }
    }

    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {annos_docException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link annos_docUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            annos_doc.log(text);
        } else if (target == ':stderr') {
            annos_doc.error(text);
        } else {
            var path = annos_docUtils.normalizePrintTarget(target);
            annos_doc.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
