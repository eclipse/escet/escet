/** Tuples. */


/** svg_input_event code generated from a CIF specification. */
class svg_input_event_class {
    /** svg_input_eventEnum declaration. It contains the single merged enum from the CIF model. */
    svg_input_eventEnum = Object.freeze({
        /** Literal "l1". */
        _l1: Symbol("l1"),

        /** Literal "l2". */
        _l2: Symbol("l2")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "a",
        "b1",
        "b2",
        "c",
        "d",
        "e",
        "f",
        "g.z"
    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "aut_b". */
    aut_b_;

    /** Discrete variable "aut_d.vd". */
    aut_d_vd_;

    /** Discrete variable "aut_f.b". */
    aut_f_b_;

    /** Discrete variable "g.a.b". */
    g_a_b_;

    /** Discrete variable "x6.x". */
    x6_x_;


    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) svg_input_event.log('Initial state: ' + svg_input_event.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "a".
            anythingExecuted |= this.execEdge0();

            // Event "b1".
            anythingExecuted |= this.execEdge1();

            // Event "b2".
            anythingExecuted |= this.execEdge2();

            // Event "c".
            anythingExecuted |= this.execEdge3();

            // Event "d".
            anythingExecuted |= this.execEdge4();

            // Event "e".
            anythingExecuted |= this.execEdge5();

            // Event "f".
            anythingExecuted |= this.execEdge6();

            // Event "g.z".
            anythingExecuted |= this.execEdge7();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {svg_input_eventException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {svg_input_eventException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (svg_input_event.first) {
                    svg_input_event.first = false;
                    svg_input_event.startMilli = now;
                    svg_input_event.targetMilli = svg_input_event.startMilli;
                    preMilli = svg_input_event.startMilli;
                }

                // Handle pausing/playing.
                if (!svg_input_event.playing) {
                    svg_input_event.timePaused = now;
                    return;
                }

                if (svg_input_event.timePaused) {
                    svg_input_event.startMilli += (now - svg_input_event.timePaused);
                    svg_input_event.targetMilli += (now - svg_input_event.timePaused);
                    svg_input_event.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = svg_input_event.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - svg_input_event.startMilli;

                // Execute once.
                svg_input_event.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (svg_input_event.doInfoExec) {
                    svg_input_event.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    svg_input_event.targetMilli += cycleMilli;
                    remainderMilli = svg_input_event.targetMilli - postMilli;
                }

                // Execute again.
                svg_input_event.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge0() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(0, true);
            if (this.doInfoEvent) this.infoEvent(0, true);

            if (this.doInfoEvent) this.infoEvent(0, false);
            if (this.doInfoPrintOutput) this.printOutput(0, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to (try to) execute event \"a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 1 and event "b1".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge1() {
        try {
            var guard;
            try {
                guard = (svg_input_event.aut_b_) == (svg_input_event.svg_input_eventEnum._l1);
            } catch (e) {
                if (e instanceof svg_input_eventException) {
                    e = new svg_input_eventException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(1, true);
            if (this.doInfoEvent) this.infoEvent(1, true);

            try {
                svg_input_event.aut_b_ = svg_input_event.svg_input_eventEnum._l2;
            } catch (e) {
                if (e instanceof svg_input_eventException) {
                    e = new svg_input_eventException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(1, false);
            if (this.doInfoPrintOutput) this.printOutput(1, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to (try to) execute event \"b1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 2 and event "b2".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge2() {
        try {
            var guard;
            try {
                guard = (svg_input_event.aut_b_) == (svg_input_event.svg_input_eventEnum._l2);
            } catch (e) {
                if (e instanceof svg_input_eventException) {
                    e = new svg_input_eventException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(2, true);
            if (this.doInfoEvent) this.infoEvent(2, true);

            try {
                svg_input_event.aut_b_ = svg_input_event.svg_input_eventEnum._l1;
            } catch (e) {
                if (e instanceof svg_input_eventException) {
                    e = new svg_input_eventException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(2, false);
            if (this.doInfoPrintOutput) this.printOutput(2, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to (try to) execute event \"b2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 3 and event "c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge3() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(3, true);
            if (this.doInfoEvent) this.infoEvent(3, true);

            if (this.doInfoEvent) this.infoEvent(3, false);
            if (this.doInfoPrintOutput) this.printOutput(3, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to (try to) execute event \"c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 4 and event "d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge4() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(4, true);
            if (this.doInfoEvent) this.infoEvent(4, true);

            try {
                if (true) {
                    svg_input_event.aut_d_vd_ = svg_input_eventUtils.addInt(svg_input_event.aut_d_vd_, 1);
                } else if (true) {
                    svg_input_event.aut_d_vd_ = svg_input_eventUtils.multiplyInt(svg_input_event.aut_d_vd_, -(1));
                }
            } catch (e) {
                if (e instanceof svg_input_eventException) {
                    e = new svg_input_eventException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(4, false);
            if (this.doInfoPrintOutput) this.printOutput(4, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to (try to) execute event \"d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 5 and event "e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge5() {
        try {
            var guard;
            try {
                guard = false;
            } catch (e) {
                if (e instanceof svg_input_eventException) {
                    e = new svg_input_eventException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(5, true);
            if (this.doInfoEvent) this.infoEvent(5, true);

            if (this.doInfoEvent) this.infoEvent(5, false);
            if (this.doInfoPrintOutput) this.printOutput(5, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to (try to) execute event \"e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 6 and event "f".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge6() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(6, true);
            if (this.doInfoEvent) this.infoEvent(6, true);

            try {
                svg_input_event.aut_f_b_ = !(svg_input_event.aut_f_b_);
            } catch (e) {
                if (e instanceof svg_input_eventException) {
                    e = new svg_input_eventException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(6, false);
            if (this.doInfoPrintOutput) this.printOutput(6, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to (try to) execute event \"f\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 7 and event "g.z".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {svg_input_eventException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge7() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(7, true);
            if (this.doInfoEvent) this.infoEvent(7, true);

            try {
                svg_input_event.g_a_b_ = !(svg_input_event.g_a_b_);
            } catch (e) {
                if (e instanceof svg_input_eventException) {
                    e = new svg_input_eventException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(7, false);
            if (this.doInfoPrintOutput) this.printOutput(7, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to (try to) execute event \"g.z\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the state.
     *
     * @throws {svg_input_eventException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            svg_input_event.aut_b_ = svg_input_event.svg_input_eventEnum._l1;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to evaluate the initial value of variable \"aut_b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            svg_input_event.aut_d_vd_ = 0;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to evaluate the initial value of variable \"aut_d.vd\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            svg_input_event.aut_f_b_ = true;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to evaluate the initial value of variable \"aut_f.b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            svg_input_event.g_a_b_ = false;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to evaluate the initial value of variable \"g.a.b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            svg_input_event.x6_x_ = 0.0;
        } catch (e) {
            if (e instanceof svg_input_eventException) {
                e = new svg_input_eventException("Failed to evaluate the initial value of variable \"x6.x\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {svg_input_eventException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type svg_input_eventException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof svg_input_eventException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) svg_input_event.log(svg_input_eventUtils.fmt('Transition: event %s', svg_input_event.getEventName(idx)));
        } else {
            if (this.doStateOutput) svg_input_event.log('State: ' + svg_input_event.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = svg_input_eventUtils.fmt('time=%s', svg_input_event.time);
        state += svg_input_eventUtils.fmt(', aut_b=%s', svg_input_eventUtils.valueToStr(svg_input_event.aut_b_));
        state += svg_input_eventUtils.fmt(', aut_d.vd=%s', svg_input_eventUtils.valueToStr(svg_input_event.aut_d_vd_));
        state += svg_input_eventUtils.fmt(', aut_f.b=%s', svg_input_eventUtils.valueToStr(svg_input_event.aut_f_b_));
        state += svg_input_eventUtils.fmt(', g.a.b=%s', svg_input_eventUtils.valueToStr(svg_input_event.g_a_b_));
        state += svg_input_eventUtils.fmt(', x6.x=%s', svg_input_eventUtils.valueToStr(svg_input_event.x6_x_));
        return state;
    }





    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {svg_input_eventException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link svg_input_eventUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            svg_input_event.log(text);
        } else if (target == ':stderr') {
            svg_input_event.error(text);
        } else {
            var path = svg_input_eventUtils.normalizePrintTarget(target);
            svg_input_event.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
