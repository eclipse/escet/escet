/** Tuples. */


/** declarations code generated from a CIF specification. */
class declarations_class {
    /** declarationsEnum declaration. It contains the single merged enum from the CIF model. */
    declarationsEnum = Object.freeze({
        /** Literal "loc1". */
        _loc1: Symbol("loc1"),

        /** Literal "loc2". */
        _loc2: Symbol("loc2")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "c_e1",
        "c_e2",
        "c_e3",
        "c_e4",
        "u_e1",
        "u_e2"
    ];

    /** Constant "c1". */
    c1_;

    /** Constant "c4". */
    c4_;

    /** Constant "c5". */
    c5_;

    /** Constant "c3". */
    c3_;

    /** Constant "c2". */
    c2_;

    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "aut1.v1". */
    aut1_v1_;

    /** Discrete variable "aut1.v4". */
    aut1_v4_;

    /** Discrete variable "aut1.v5". */
    aut1_v5_;

    /** Continuous variable "aut2.v2". */
    aut2_v2_;

    /** Discrete variable "g1.a1". */
    g1_a1_;

    /** Continuous variable "aut1.v3". */
    aut1_v3_;

    /** Discrete variable "aut2.v1". */
    aut2_v1_;

    /** Discrete variable "aut1.v2". */
    aut1_v2_;

    /** Discrete variable "aut1.v7". */
    aut1_v7_;

    /** Discrete variable "aut1.v8". */
    aut1_v8_;

    /** Discrete variable "aut1.v6". */
    aut1_v6_;

    /** Input variable "i1". */
    i1_;

    /** Input variable "i2". */
    i2_;

    /** Input variable "i3". */
    i3_;

    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {declarationsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        try {
                            var deriv0 = declarations.aut1_v3_deriv();
                            var deriv1 = declarations.aut2_v2_deriv();

                            declarations.aut1_v3_ = declarations.aut1_v3_ + delta * deriv0;
                            declarationsUtils.checkReal(declarations.aut1_v3_, "aut1.v3");
                            if (declarations.aut1_v3_ == -0.0) declarations.aut1_v3_ = 0.0;
                            declarations.aut2_v2_ = declarations.aut2_v2_ + delta * deriv1;
                            declarationsUtils.checkReal(declarations.aut2_v2_, "aut2.v2");
                            if (declarations.aut2_v2_ == -0.0) declarations.aut2_v2_ = 0.0;
                        } catch (e) {
                            if (e instanceof declarationsException) {
                                e = new declarationsException("Failed to update continuous variables after time passage.", e);
                                this.runtimeError(e);
                            }
                            throw e;
                        }
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) declarations.log('Initial state: ' + declarations.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "u_e1".
            anythingExecuted |= this.execEdge0();

            // Event "u_e2".
            anythingExecuted |= this.execEdge1();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "c_e1".
            anythingExecuted |= this.execEdge2();

            // Event "c_e2".
            anythingExecuted |= this.execEdge3();

            // Event "c_e3".
            anythingExecuted |= this.execEdge4();

            // Event "c_e4".
            anythingExecuted |= this.execEdge5();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {declarationsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {declarationsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (declarations.first) {
                    declarations.first = false;
                    declarations.startMilli = now;
                    declarations.targetMilli = declarations.startMilli;
                    preMilli = declarations.startMilli;
                }

                // Handle pausing/playing.
                if (!declarations.playing) {
                    declarations.timePaused = now;
                    return;
                }

                if (declarations.timePaused) {
                    declarations.startMilli += (now - declarations.timePaused);
                    declarations.targetMilli += (now - declarations.timePaused);
                    declarations.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = declarations.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - declarations.startMilli;

                // Execute once.
                declarations.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (declarations.doInfoExec) {
                    declarations.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    declarations.targetMilli += cycleMilli;
                    remainderMilli = declarations.targetMilli - postMilli;
                }

                // Execute again.
                declarations.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "u_e1".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge0() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(4, true);
            if (this.doInfoEvent) this.infoEvent(4, true);

            if (this.doInfoEvent) this.infoEvent(4, false);
            if (this.doInfoPrintOutput) this.printOutput(4, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to (try to) execute event \"u_e1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 1 and event "u_e2".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge1() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(5, true);
            if (this.doInfoEvent) this.infoEvent(5, true);

            if (this.doInfoEvent) this.infoEvent(5, false);
            if (this.doInfoPrintOutput) this.printOutput(5, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to (try to) execute event \"u_e2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 2 and event "c_e1".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge2() {
        try {
            var guard;
            try {
                guard = (declarations.g1_a1_) == (declarations.declarationsEnum._loc1);
            } catch (e) {
                if (e instanceof declarationsException) {
                    e = new declarationsException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(0, true);
            if (this.doInfoEvent) this.infoEvent(0, true);

            try {
                declarations.g1_a1_ = declarations.declarationsEnum._loc2;
            } catch (e) {
                if (e instanceof declarationsException) {
                    e = new declarationsException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(0, false);
            if (this.doInfoPrintOutput) this.printOutput(0, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to (try to) execute event \"c_e1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 3 and event "c_e2".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge3() {
        try {
            var guard;
            try {
                guard = (declarations.g1_a1_) == (declarations.declarationsEnum._loc2);
            } catch (e) {
                if (e instanceof declarationsException) {
                    e = new declarationsException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(1, true);
            if (this.doInfoEvent) this.infoEvent(1, true);

            try {
                declarations.g1_a1_ = declarations.declarationsEnum._loc1;
            } catch (e) {
                if (e instanceof declarationsException) {
                    e = new declarationsException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(1, false);
            if (this.doInfoPrintOutput) this.printOutput(1, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to (try to) execute event \"c_e2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 4 and event "c_e3".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge4() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(2, true);
            if (this.doInfoEvent) this.infoEvent(2, true);

            if (this.doInfoEvent) this.infoEvent(2, false);
            if (this.doInfoPrintOutput) this.printOutput(2, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to (try to) execute event \"c_e3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 5 and event "c_e4".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge5() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(3, true);
            if (this.doInfoEvent) this.infoEvent(3, true);

            if (this.doInfoEvent) this.infoEvent(3, false);
            if (this.doInfoPrintOutput) this.printOutput(3, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to (try to) execute event \"c_e4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the state.
     *
     * @throws {declarationsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;
            declarations.c1_ = 2.23606797749979;
            declarations.c4_ = declarationsUtils.addReal(declarations.c1_, 2.0);
            declarations.c5_ = declarations.c4_;
            declarations.c3_ = declarations.c5_;
            declarations.c2_ = declarations.c3_;
        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            declarations.aut1_v1_ = 2.6457513110645907;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut1.v1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut1_v4_ = declarationsUtils.addReal(declarations.aut1_v1_, 2.0);
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut1.v4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut1_v5_ = declarations.aut1_v4_;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut1.v5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut2_v2_ = 0.0;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut2.v2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.g1_a1_ = declarations.declarationsEnum._loc1;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"g1.a1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut1_v3_ = declarations.aut1_v5_;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut1.v3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut2_v1_ = declarations.aut2_v2_;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut2.v1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut1_v2_ = declarations.aut1_v3_;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut1.v2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut1_v7_ = declarationsUtils.addReal(declarations.aut1_v2_, declarations.aut2_v1_);
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut1.v7\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut1_v8_ = declarations.aut1_v7_;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut1.v8\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            declarations.aut1_v6_ = declarations.aut1_v8_;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the initial value of variable \"aut1.v6\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {declarationsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type declarationsException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof declarationsException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) declarations.log(declarationsUtils.fmt('Transition: event %s', declarations.getEventName(idx)));
        } else {
            if (this.doStateOutput) declarations.log('State: ' + declarations.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = declarationsUtils.fmt('time=%s', declarations.time);
        state += declarationsUtils.fmt(', aut1.v1=%s', declarationsUtils.valueToStr(declarations.aut1_v1_));
        state += declarationsUtils.fmt(', aut1.v2=%s', declarationsUtils.valueToStr(declarations.aut1_v2_));
        state += declarationsUtils.fmt(', aut1.v3=%s', declarationsUtils.valueToStr(declarations.aut1_v3_));
        state += declarationsUtils.fmt(', aut1.v3\'=%s', declarationsUtils.valueToStr(declarations.aut1_v3_deriv()));
        state += declarationsUtils.fmt(', aut1.v4=%s', declarationsUtils.valueToStr(declarations.aut1_v4_));
        state += declarationsUtils.fmt(', aut1.v5=%s', declarationsUtils.valueToStr(declarations.aut1_v5_));
        state += declarationsUtils.fmt(', aut1.v6=%s', declarationsUtils.valueToStr(declarations.aut1_v6_));
        state += declarationsUtils.fmt(', aut1.v7=%s', declarationsUtils.valueToStr(declarations.aut1_v7_));
        state += declarationsUtils.fmt(', aut1.v8=%s', declarationsUtils.valueToStr(declarations.aut1_v8_));
        state += declarationsUtils.fmt(', aut2.v1=%s', declarationsUtils.valueToStr(declarations.aut2_v1_));
        state += declarationsUtils.fmt(', aut2.v2=%s', declarationsUtils.valueToStr(declarations.aut2_v2_));
        state += declarationsUtils.fmt(', aut2.v2\'=%s', declarationsUtils.valueToStr(declarations.aut2_v2_deriv()));
        state += declarationsUtils.fmt(', g1.a1=%s', declarationsUtils.valueToStr(declarations.g1_a1_));
        return state;
    }


    /**
     * Evaluates algebraic variable "a1".
     *
     * @return The evaluation result.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a1_() {
        try {
            return declarationsUtils.addReal(declarationsUtils.addReal(declarations.i1_, declarations.a3_()), declarations.c1_);
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate algebraic variable \"a1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a2".
     *
     * @return The evaluation result.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a2_() {
        try {
            return declarationsUtils.floor(declarations.a4_());
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate algebraic variable \"a2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a3".
     *
     * @return The evaluation result.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a3_() {
        try {
            return declarationsUtils.multiplyReal(declarations.a2_(), 3.0);
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate algebraic variable \"a3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a4".
     *
     * @return The evaluation result.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a4_() {
        try {
            return declarationsUtils.addReal(123.4, declarations.i2_);
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate algebraic variable \"a4\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "aut1.v3".
     *
     * @return The evaluation result.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut1_v3_deriv() {
        try {
            return declarations.aut1_v6_;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the derivative of continuous variable \"aut1.v3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "aut2.v2".
     *
     * @return The evaluation result.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut2_v2_deriv() {
        try {
            return declarations.aut1_v5_;
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to evaluate the derivative of continuous variable \"aut2.v2\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "inc".
     *
     * @param inc_x_ Function parameter "inc.x".
     * @return The return value of the function.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    inc_(inc_x_) {
        try {
            // Execute statements in the function body.
            return declarationsUtils.addInt(inc_x_, 1);
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to execute internal user-defined function \"inc\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "f1".
     *
     * @param f1_x_ Function parameter "f1.x".
     * @return The return value of the function.
     * @throws {declarationsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    f1_(f1_x_) {
        try {
            // Variable "f1.v1".
            var f1_v1_ = 2.449489742783178;

            // Variable "f1.v4".
            var f1_v4_ = declarationsUtils.addReal(f1_v1_, 2.0);

            // Variable "f1.v5".
            var f1_v5_ = f1_v4_;

            // Variable "f1.v3".
            var f1_v3_ = f1_v5_;

            // Variable "f1.v2".
            var f1_v2_ = f1_v3_;

            // Execute statements in the function body.
            return declarationsUtils.addReal(declarationsUtils.addReal(declarationsUtils.addReal(declarationsUtils.addReal(f1_v1_, f1_v2_), f1_v3_), declarationsUtils.multiplyReal(f1_v4_, f1_v5_)), f1_x_);
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof declarationsException) {
                e = new declarationsException("Failed to execute internal user-defined function \"f1\".", e);
            }
            throw e;
        }
    }

    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {declarationsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link declarationsUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            declarations.log(text);
        } else if (target == ':stderr') {
            declarations.error(text);
        } else {
            var path = declarationsUtils.normalizePrintTarget(target);
            declarations.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
