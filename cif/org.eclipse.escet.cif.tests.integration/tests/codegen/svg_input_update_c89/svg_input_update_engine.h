/* Headers for the CIF to C translation of svg_input_update.cif
 * Generated file, DO NOT EDIT
 */

#ifndef CIF_C_SVG_INPUT_UPDATE_ENGINE_H
#define CIF_C_SVG_INPUT_UPDATE_ENGINE_H

#include "svg_input_update_library.h"

/* Types of the specification.
 * Note that integer ranges are ignored in C.
 */
/* CIF type: list[2] bool */
struct A2B_struct {
    BoolType data[2];
};
typedef struct A2B_struct A2BType;

extern BoolType A2BTypeEquals(A2BType *left, A2BType *right);
extern BoolType A2BTypeProject(A2BType *array, IntType index);
extern void A2BTypeModify(A2BType *array, IntType index, BoolType value);
extern int A2BTypePrint(A2BType *array, char *dest, int start, int end);

enum Enumsvg_input_update_ {
    /** Literal "__some_dummy_enum_literal". */
    _svg_input_update___some_dummy_enum_literal,
};
typedef enum Enumsvg_input_update_ svg_input_updateEnum;

extern const char *enum_names[];
extern int EnumTypePrint(svg_input_updateEnum value, char *dest, int start, int end);

/* CIF type: list[3] int[0..3] */
struct A3I_struct {
    IntType data[3];
};
typedef struct A3I_struct A3IType;

extern BoolType A3ITypeEquals(A3IType *left, A3IType *right);
extern IntType A3ITypeProject(A3IType *array, IntType index);
extern void A3ITypeModify(A3IType *array, IntType index, IntType value);
extern int A3ITypePrint(A3IType *array, char *dest, int start, int end);


/* Event declarations. */
enum svg_input_updateEventEnum_ {
    /** Initial step. */
    EVT_INITIAL_,

    /** Delay step. */
    EVT_DELAY_,
};
typedef enum svg_input_updateEventEnum_ svg_input_update_Event_;

/** Names of all the events. */
extern const char *svg_input_update_event_names[];

/* Constants. */


/* Input variables. */

/** Input variable "bool i". */
extern BoolType i_;

/** Input variable "bool j". */
extern BoolType j_;

/** Input variable "int[0..3] ia". */
extern IntType ia_;

/** Input variable "int[0..3] ib". */
extern IntType ib_;

/** Input variable "int[0..3] ic". */
extern IntType ic_;

/** Input variable "int[-1..4] g.i". */
extern IntType g_i_;

extern void svg_input_update_AssignInputVariables();

/* Declaration of internal functions. */


/* State variables (use for output only). */
extern RealType model_time; /**< Current model time. */


/* Algebraic and derivative functions (use for output only). */

A2BType a_(void);
IntType iabc_(void);


/* Code entry points. */
void svg_input_update_EngineFirstStep(void);
void svg_input_update_EngineTimeStep(double delta);

#if EVENT_OUTPUT
/**
 * External callback function reporting about the execution of an event.
 * @param event Event being executed.
 * @param pre If \c TRUE, event is about to be executed. If \c FALSE, event has been executed.
 * @note Function must be implemented externally.
 */
extern void svg_input_update_InfoEvent(svg_input_update_Event_ event, BoolType pre);
#endif

#if PRINT_OUTPUT
/**
 * External callback function to output the given text-line to the given filename.
 * @param text Text to print (does not have a EOL character).
 * @param fname Name of the file to print to.
 */
extern void svg_input_update_PrintOutput(const char *text, const char *fname);
#endif

#endif

