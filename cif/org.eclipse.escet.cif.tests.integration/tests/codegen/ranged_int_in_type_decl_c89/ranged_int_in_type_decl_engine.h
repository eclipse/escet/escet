/* Headers for the CIF to C translation of ranged_int_in_type_decl.cif
 * Generated file, DO NOT EDIT
 */

#ifndef CIF_C_RANGED_INT_IN_TYPE_DECL_ENGINE_H
#define CIF_C_RANGED_INT_IN_TYPE_DECL_ENGINE_H

#include "ranged_int_in_type_decl_library.h"

/* Types of the specification.
 * Note that integer ranges are ignored in C.
 */
enum Enumranged_int_in_type_decl_ {
    /** Literal "__some_dummy_enum_literal". */
    _ranged_int_in_type_decl___some_dummy_enum_literal,
};
typedef enum Enumranged_int_in_type_decl_ ranged_int_in_type_declEnum;

extern const char *enum_names[];
extern int EnumTypePrint(ranged_int_in_type_declEnum value, char *dest, int start, int end);


/* Event declarations. */
enum ranged_int_in_type_declEventEnum_ {
    /** Initial step. */
    EVT_INITIAL_,

    /** Delay step. */
    EVT_DELAY_,

    /** Event "c_decr". */
    c_decr_,
};
typedef enum ranged_int_in_type_declEventEnum_ ranged_int_in_type_decl_Event_;

/** Names of all the events. */
extern const char *ranged_int_in_type_decl_event_names[];

/* Constants. */


/* Input variables. */




/* Declaration of internal functions. */


/* State variables (use for output only). */
extern RealType model_time; /**< Current model time. */

/** Discrete variable "int[0..10] Test.item". */
extern IntType Test_item_;

/* Algebraic and derivative functions (use for output only). */



/* Code entry points. */
void ranged_int_in_type_decl_EngineFirstStep(void);
void ranged_int_in_type_decl_EngineTimeStep(double delta);

#if EVENT_OUTPUT
/**
 * External callback function reporting about the execution of an event.
 * @param event Event being executed.
 * @param pre If \c TRUE, event is about to be executed. If \c FALSE, event has been executed.
 * @note Function must be implemented externally.
 */
extern void ranged_int_in_type_decl_InfoEvent(ranged_int_in_type_decl_Event_ event, BoolType pre);
#endif

#if PRINT_OUTPUT
/**
 * External callback function to output the given text-line to the given filename.
 * @param text Text to print (does not have a EOL character).
 * @param fname Name of the file to print to.
 */
extern void ranged_int_in_type_decl_PrintOutput(const char *text, const char *fname);
#endif

#endif

