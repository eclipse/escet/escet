//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Declarations to keep, in different scopes.

controllable c_e;
const real c1 = 5.0;
enum E = L1, L2;
type t = list int;

func int inc(int x):
  int result = x + 1;
  return result;
end

group grp_decls_keep:
  group grp:
    controllable c_e;
    const real x = 5.0;
    enum E = L1, L2;
    type t = list int;

    func int inc(int x):
      int result = x + 1;
      return result;
    end
  end

  controllable c_e;
  const real x = 5.0;
  enum E = L1, L2;
  type t = list int;

  func int inc(int x):
    int result = x + 1;
    return result;
  end
end

// Declarations that become input variables, in different scopes.

alg real a = 5.0;
cont c2 = 3.0 der 4.0;

group grp_decl2input:
  group grp:
    alg real a = 5.0;
    cont c2 = 3.0 der 4.0;
  end

  alg real a = 5.0;
  cont c2 = 3.0 der 4.0;
end

// Automata, locations and discrete variables that become groups and input variables.

automaton aut1:
  type t = bool;
  const t c = true;
  alg int a = v1 + v2;
  disc int v1 = v2 +1;
  disc int v2 = 5;

  event e;
  alphabet e;
  monitor e;

  location loc1:
    initial;
    edge e when v1 = 3 goto loc2;
  location loc2:
    initial;
    edge tau do v2 := 0 goto loc1;
end

automaton aut2:
  alphabet grp_decls_keep.c_e, grp_decls_keep.grp.c_e;
  location:
    initial;
    edge grp_decls_keep.c_e, grp_decls_keep.grp.c_e;
end

// Input variables, in different scopes, that are kept.

input bool i;

group grp_input:
  group grp:
    input bool i;
  end

  input bool i;
end

// Initialization predicates, marker predicates and invariants in components get removed.

initial 1.0 = 1.0;
marked 2.0 = 2.0;
invariant 3.0 = 3.0;

group grp_init_marked_inv:
  group grp:
    initial 1.1 = 1.1;
    marked 2.1 = 2.1;
    invariant 3.1 = 3.1;
  end

  initial 1.2 = 1.2;
  marked 2.2 = 2.2;
  invariant 3.2 = 3.2;
end

// Equations get removed.

alg real x_eqn;
equation x_eqn = 5;
cont cont_eqn = 1.0;
equation cont_eqn' = x_eqn;

group grp_eqn:
  group grp:
    alg real x_eqn;
    equation x_eqn = 5;
    cont cont_eqn = 1.0;
    equation cont_eqn' = x_eqn;
  end

  alg real x_eqn;
  equation x_eqn = 5;
  cont cont_eqn = 1.0;
  equation cont_eqn' = x_eqn;
end

automaton aut_eqn:
  alg real x_eqn;
  cont cont_eqn = 1.0;

  location loc1:
    initial;
    equation x_eqn = 5;
    equation cont_eqn' = x_eqn;

  location loc2:
    initial;
    equation x_eqn = 5;
    equation cont_eqn' = x_eqn;
end

// I/O declarations are removed.

print time;
svgfile "convert_to_interface.svg";
svgout id "r1" attr "x" value "20";
svgin id "r1" event c_e;

group grp_iodecl:
  group grp:
    print time;
    svgfile "convert_to_interface.svg";
    svgout id "r2" attr "x" value "20";
    svgin id "r2" event c_e;
  end

  print time;
  svgfile "convert_to_interface.svg";
  svgout id "r3" attr "x" value "20";
  svgin id "r3" event c_e;
end

// Annotations are removed.

@doc("grp_annos")
group grp_annos:
  @doc("grp")
  group grp:
    @doc("alg")
    alg real a = 5.0;
  end
end

// Component definitions/instantiations remain, but some parameters/arguments are removed.

group def G():
  type t = bool;
  controllable c_e;
end

g1: G();
g2: G();

automaton def A(alg bool b; event e; location l; G g):
  disc g.t v = b;
  initial b;
  invariant b;

  location loc:
    initial;
    edge e, g.c_e when b do v := l;
end

a1: A(true,   g2.c_e, a2.loc, g1);
a2: A(aut1.c, g1.c_e, a1.loc, g2);

group grp_def_inst:
  group def G():
    type t = bool;
    controllable c_e;
  end

  g1: G();
  g2: G();

  automaton def A(alg bool b; event e; location l; G g):
    disc g.t v = b;
    initial b;
    invariant b;

    location loc:
      initial;
      edge e, g.c_e when b do v := l;
  end

  a1: A(true,   g2.c_e, a2.loc, g1);
  a2: A(aut1.c, g1.c_e, a1.loc, g2);
end
