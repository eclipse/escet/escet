FUNCTION tryCon_specification: BOOL
{ S7_Optimized_Access := 'true' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_sup: BYTE;
        current_timer: BOOL;
        current_timer_t: LREAL;
        edge_sup: BYTE;
        edge_timer: BOOL;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
        dummyVar3: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform controllable event "start".
     *
     * - Automaton "sup" must always synchronize.
     * - Automaton "timer" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edges of automaton "sup" to synchronize for event "start".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "s3":
     *   - 1st edge in the location
     * - Location "s5":
     *   - 1st edge in the location
     ***********)
    IF "DB".sup = sup_s3 THEN
        edge_sup := 0;
    ELSIF "DB".sup = sup_s5 THEN
        edge_sup := 1;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Test edge of automaton "timer" to synchronize for event "start".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edge being tested:
         * - Location "Idle":
         *   - 1st edge in the location
         ***********)
        IF "DB".timer_1 = timer_Idle THEN
            edge_timer := 0;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "start" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sup := "DB".sup;
        current_timer := "DB".timer_1;
        current_timer_t := "DB".timer_t;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sup". *)
        IF edge_sup = 0 THEN
            (* Perform assignments of the 1st edge in location "sup.s3". *)
            (* Perform update of current-location variable for automaton "sup". *)
            "DB".sup := sup_s6;
        ELSIF edge_sup = 1 THEN
            (* Perform assignments of the 1st edge in location "sup.s5". *)
            (* Perform update of current-location variable for automaton "sup". *)
            "DB".sup := sup_s7;
        END_IF;
        (* Perform assignments of automaton "timer". *)
        IF edge_timer = 0 THEN
            (* Perform assignments of the 1st edge in location "timer.Idle". *)
            (* Perform update of continuous variable "timer.t". *)
            "DB".timer_t := 3.0;
            (* Reset timer of "timer_t". *)
            "DB".preset_timer_t := DINT_TO_TIME(LREAL_TO_DINT("DB".timer_t * 1000.0));
            ton_timer_t.TON(IN := FALSE, PT := "DB".preset_timer_t);
            ton_timer_t.TON(IN := TRUE, PT := "DB".preset_timer_t);
            (* Perform update of current-location variable for automaton "timer". *)
            "DB".timer_1 := timer_Running;
        END_IF;
    END_IF;

    (* Return event execution progress. *)
    tryCon_specification := funcIsProgress;
    RETURN;
END_FUNCTION
