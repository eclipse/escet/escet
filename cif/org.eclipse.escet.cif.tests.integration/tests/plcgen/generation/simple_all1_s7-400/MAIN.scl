ORGANIZATION_BLOCK MAIN
{ S7_Optimized_Access := 'false' }
    VAR_TEMP
        curValue: TIME;
        current_p: BYTE;
        current_p_b: BOOL;
        current_p_j: DINT;
        current_p_r: REAL;
        current_p_tv: TupleStruct2;
        current_p_v3: g_ttrrr;
        current_p_ve: BOOL;
        current_p_x: DINT;
        edge_p: BYTE;
        eventEnabled: BOOL;
        inMax: REAL;
        inMin: REAL;
        isProgress: BOOL;
        litStruct: g_trr;
        litStruct_1: g_ttrrr;
        litStruct_2: TupleStruct2;
        outMax: REAL;
        outMin: REAL;
        rightValue: TupleStruct2;
        timeOut: BOOL;
        trunced: DINT;
        value: REAL;
    END_VAR

BEGIN
    (* Header text file for:
     *  -> (-*-) CIF PLC code generator.
     *)

    (* An overview of the CIF model automata with their events and variables is written after the main code. *)



    (* --- Initialize state or update continuous variables. -------------------- *)
    IF "DB".firstRun THEN
        "DB".firstRun := FALSE;

        (* Initialize the state variables. *)
        (* Initialize discrete variable "p.b". *)
        "DB".p_b := TRUE;
        (* Initialize discrete variable "p.x". *)
        "DB".p_x := 0;
        (* Initialize discrete variable "p.y". *)
        "DB".p_y := 1.23;
        (* Initialize discrete variable "p.ve". *)
        "DB".p_ve := g_LIT1;
        (* Initialize discrete variable "p.v1". *)
        "DB".p_v1 := 0;
        (* Initialize discrete variable "p.v2". *)
        litStruct.field1 := 0.0;
        litStruct.field2 := 0.0;
        "DB".p_v2 := litStruct;
        (* Initialize discrete variable "p.v3". *)
        litStruct.field1 := 0.0;
        litStruct.field2 := 0.0;
        litStruct_1.field1 := litStruct;
        litStruct_1.field2 := 0.0;
        "DB".p_v3 := litStruct_1;
        (* Initialize discrete variable "p.tv". *)
        litStruct_2.field1 := 0;
        litStruct_2.field2 := 0;
        "DB".p_tv := litStruct_2;
        (* Initialize discrete variable "p.j". *)
        "DB".p_j := 0;
        (* Initialize discrete variable "p.r". *)
        "DB".p_r := 1000000.0;
        (* Initialize current-location variable for automaton "p". *)
        "DB".p := p_l1;
        (* Initialize continuous variable "p.t". *)
        "DB".p_t := 0.0;
        (* Reset timer of "p_t". *)
        "DB".preset_p_t := DINT_TO_TIME(REAL_TO_DINT("DB".p_t * 1000.0));
        ton_p_t.TON(IN := FALSE, PT := "DB".preset_p_t);
        ton_p_t.TON(IN := TRUE, PT := "DB".preset_p_t);
    ELSE
        (* Update remaining time of continuous variable "p.t". *)
        ton_p_t.TON(IN := TRUE, PT := "DB".preset_p_t, Q => timeOut, ET => curValue);
        "DB".p_t := SEL_REAL(G := timeOut, IN0 := MAX(IN1 := DINT_TO_REAL(TIME_TO_DINT("DB".preset_p_t - curValue)) / 1000.0, IN2 := 0.0), IN1 := 0.0);
    END_IF;

    (* --- Process controllable events. ---------------------------------------- *)
    isProgress := TRUE;
    (* Perform events until none can be done anymore. *)
    WHILE isProgress DO
        isProgress := FALSE;

        (*************************************************************
         * Try to perform controllable event "p.evt".
         *
         * - Automaton "p" must always synchronize.
         *************************************************************)
        eventEnabled := TRUE;
        (*******************************
         * Check each synchronizing automaton for having an edge with a true guard.
         *******************************)
        (***********
         * Test edges of automaton "p" to synchronize for event "p.evt".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location "l1":
         *   - 1st edge in the location
         * - Location "l6":
         *   - 1st edge in the location
         * - Location "l7":
         *   - 1st edge in the location
         * - Location "l8":
         *   - 1st edge in the location
         * - Location "l9":
         *   - 1st edge in the location
         * - Location "l10":
         *   - 1st edge in the location
         * - Location "l11":
         *   - 1st edge in the location
         * - Location "l12":
         *   - 1st edge in the location
         * - Location "l13":
         *   - 1st edge in the location
         * - Location "l14":
         *   - 1st edge in the location
         * - Location "l15":
         *   - 1st edge in the location
         * - Location "l16":
         *   - 1st edge in the location
         * - Location "l17":
         *   - 1st edge in the location
         * - Location "l18a":
         *   - 1st edge in the location
         * - Location "l18b":
         *   - 1st edge in the location
         * - Location "l19":
         *   - 1st edge in the location
         * - Location "l20":
         *   - 1st edge in the location
         * - Location "l21":
         *   - 1st edge in the location
         * - Location "l22a":
         *   - 1st edge in the location
         * - Location "l22b":
         *   - 1st edge in the location
         ***********)
        IF "DB".p = p_l1 THEN
            edge_p := 0;
        ELSIF "DB".p = p_l6 THEN
            edge_p := 1;
        ELSIF "DB".p = p_l7 THEN
            edge_p := 2;
        ELSIF "DB".p = p_l8 THEN
            edge_p := 3;
        ELSIF "DB".p = p_l9 THEN
            edge_p := 4;
        ELSIF "DB".p = p_l10 THEN
            edge_p := 5;
        ELSIF "DB".p = p_l11 THEN
            edge_p := 6;
        ELSIF "DB".p = p_l12 THEN
            edge_p := 7;
        ELSIF "DB".p = p_l13 THEN
            edge_p := 8;
        ELSIF "DB".p = p_l14 THEN
            edge_p := 9;
        ELSIF "DB".p = p_l15 THEN
            edge_p := 10;
        ELSIF "DB".p = p_l16 THEN
            edge_p := 11;
        ELSIF "DB".p = p_l17 AND "DB".p_j < "DB".p_j = "DB".p_j < "DB".p_j THEN
            edge_p := 12;
        ELSIF "DB".p = p_l18a THEN
            edge_p := 13;
        ELSIF "DB".p = p_l18b THEN
            edge_p := 14;
        ELSIF "DB".p = p_l19 THEN
            edge_p := 15;
        ELSIF "DB".p = p_l20 THEN
            edge_p := 16;
        ELSIF "DB".p = p_l21 THEN
            edge_p := 17;
        ELSIF "DB".p = p_l22a THEN
            edge_p := 18;
        ELSIF "DB".p = p_l22b THEN
            edge_p := 19;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
        (* All checks have been done. If variable "eventEnabled" still holds, event "p.evt" can occur. *)
        IF eventEnabled THEN
            isProgress := TRUE;
            (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
            current_p := "DB".p;
            current_p_b := "DB".p_b;
            current_p_j := "DB".p_j;
            current_p_r := "DB".p_r;
            current_p_tv := "DB".p_tv;
            current_p_v3 := "DB".p_v3;
            current_p_ve := "DB".p_ve;
            current_p_x := "DB".p_x;
            (*******************************
             * Perform the assignments of each synchronizing automaton.
             *******************************)
            (* Perform assignments of automaton "p". *)
            IF edge_p = 0 THEN
                (* Perform assignments of the 1st edge in location "p.l1". *)
                (* Perform update of discrete variable "p.tv". *)
                litStruct_2.field1 := 1;
                litStruct_2.field2 := 2;
                "DB".p_tv := litStruct_2;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l6;
            ELSIF edge_p = 1 THEN
                (* Perform assignments of the 1st edge in location "p.l6". *)
                (* Perform update of discrete variable "p.x". *)
                "DB".p_x := 3;
                (* Perform update of discrete variable "p.j". *)
                "DB".p_j := 4;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l7;
            ELSIF edge_p = 2 THEN
                (* Perform assignments of the 1st edge in location "p.l7". *)
                rightValue := current_p_tv;
                (* Perform update of discrete variable "p.x". *)
                "DB".p_x := rightValue.field1;
                (* Perform update of discrete variable "p.j". *)
                "DB".p_j := rightValue.field2;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l8;
            ELSIF edge_p = 3 THEN
                (* Perform assignments of the 1st edge in location "p.l8". *)
                (* Perform update of discrete variable "p.tv". *)
                "DB".p_tv.field1 := 5;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l9;
            ELSIF edge_p = 4 THEN
                (* Perform assignments of the 1st edge in location "p.l9". *)
                (* Perform update of discrete variable "p.tv". *)
                "DB".p_tv.field2 := 6;
                (* Perform update of discrete variable "p.j". *)
                "DB".p_j := 7;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l10;
            ELSIF edge_p = 5 THEN
                (* Perform assignments of the 1st edge in location "p.l10". *)
                (* Perform update of discrete variable "p.v3". *)
                "DB".p_v3.field1.field2 := 7.8;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l11;
            ELSIF edge_p = 6 THEN
                (* Perform assignments of the 1st edge in location "p.l11". *)
                (* Perform update of discrete variable "p.v3". *)
                litStruct.field1 := 1.2;
                litStruct.field2 := 3.4;
                litStruct_1.field1 := litStruct;
                litStruct_1.field2 := 5.6;
                "DB".p_v3 := litStruct_1;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l12;
            ELSIF edge_p = 7 THEN
                (* Perform assignments of the 1st edge in location "p.l12". *)
                (* Perform update of discrete variable "p.j". *)
                "DB".p_j := current_p_j + 1;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l13;
            ELSIF edge_p = 8 THEN
                (* Perform assignments of the 1st edge in location "p.l13". *)
                (* Perform update of discrete variable "p.b". *)
                "DB".p_b := FALSE;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l14;
            ELSIF edge_p = 9 THEN
                (* Perform assignments of the 1st edge in location "p.l14". *)
                IF (NOT current_p_b) THEN
                    (* Perform update of discrete variable "p.ve". *)
                    "DB".p_ve := g_LIT2;
                END_IF;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l15;
            ELSIF edge_p = 10 THEN
                (* Perform assignments of the 1st edge in location "p.l15". *)
                (* Perform update of discrete variable "p.r". *)
                "DB".p_r := DINT_TO_REAL("DB".g_inp) + 10.0;
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l16;
            ELSIF edge_p = 11 THEN
                (* Perform assignments of the 1st edge in location "p.l16". *)
                (* Perform update of discrete variable "p.x". *)
                "DB".p_x := (-(-current_p_x));
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l17;
            ELSIF edge_p = 12 THEN
                (* Perform assignments of the 1st edge in location "p.l17". *)
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l18a;
            ELSIF edge_p = 13 THEN
                (* Perform assignments of the 1st edge in location "p.l18a". *)
                (* Perform update of discrete variable "p.j". *)
                (*
                 * CIFsign(x) = -1 if x < 0
                 *               0 if x = 0
                 *              +1 if x > 0
                 *)
                "DB".p_j := SEL_DINT(G := current_p_r > 0.0, IN0 := SEL_DINT(G := current_p_r < 0.0, IN0 := 0, IN1 := (-1)), IN1 := 1);
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l18b;
            ELSIF edge_p = 14 THEN
                (* Perform assignments of the 1st edge in location "p.l18b". *)
                (* Perform update of discrete variable "p.j". *)
                (*
                 * CIFsign(x) = -1 if x < 0
                 *               0 if x = 0
                 *              +1 if x > 0
                 *)
                "DB".p_j := SEL_DINT(G := current_p_j > 0, IN0 := SEL_DINT(G := current_p_j < 0, IN0 := 0, IN1 := (-1)), IN1 := 1);
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l19;
            ELSIF edge_p = 15 THEN
                (* Perform assignments of the 1st edge in location "p.l19". *)
                (* Perform update of discrete variable "p.j". *)
                (* CIFfloor: Round down towards -infinity. *)
                trunced := TRUNC(current_p_r);
                (*
                 * If trunced = inputValue, the inputValue was already floored.
                 * If inputValue >= 0, TRUNC already rounded towards -infinity.
                 * If inputValue <= minimal DINT value, TRUNC already rounded towards -infinity as far as possible.
                 *)
                "DB".p_j := SEL_DINT(G := DINT_TO_REAL(trunced) <> current_p_r AND current_p_r < 0.0 AND current_p_r > -2147483648.0, IN0 := trunced, IN1 := trunced - 1);
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l20;
            ELSIF edge_p = 16 THEN
                (* Perform assignments of the 1st edge in location "p.l20". *)
                (* Perform update of discrete variable "p.j". *)
                (* CIFceil: Round up towards +infinity. *)
                trunced := TRUNC(current_p_r);
                (*
                 * If trunced = inputValue, the inputValue was already ceiled.
                 * If inputValue <= 0, TRUNC already rounded towards +infinity.
                 * If inputValue >= maximal DINT value, TRUNC already rounded towards +infinity as far as possible.
                 *)
                "DB".p_j := SEL_DINT(G := DINT_TO_REAL(trunced) <> current_p_r AND current_p_r > 0.0 AND current_p_r < 2147483647.0, IN0 := trunced, IN1 := trunced + 1);
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l21;
            ELSIF edge_p = 17 THEN
                (* Perform assignments of the 1st edge in location "p.l21". *)
                (* Perform update of discrete variable "p.j". *)
                (* CIFround(arg) = CIFfloor(arg + 0.5) *)
                (* CIFfloor: Round down towards -infinity. *)
                trunced := TRUNC(current_p_r + 0.5);
                (*
                 * If trunced = inputValue, the inputValue was already floored.
                 * If inputValue >= 0, TRUNC already rounded towards -infinity.
                 * If inputValue <= minimal DINT value, TRUNC already rounded towards -infinity as far as possible.
                 *)
                "DB".p_j := SEL_DINT(G := DINT_TO_REAL(trunced) <> current_p_r + 0.5 AND current_p_r + 0.5 < 0.0 AND current_p_r + 0.5 > -2147483648.0, IN0 := trunced, IN1 := trunced - 1);
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l22a;
            ELSIF edge_p = 18 THEN
                (* Perform assignments of the 1st edge in location "p.l22a". *)
                (* Perform update of discrete variable "p.r". *)
                (*
                 * CIFscale: Input value 'inMin' is scaled to output value 'outMin'. Input value
                 * 'inMax' is scaled to output value 'outMax'. All other values are scaled by
                 * interpolating or extrapolating from those value pairs.
                 *)
                value := current_p_r;
                inMin := DINT_TO_REAL(current_p_j);
                inMax := current_p_r;
                outMin := DINT_TO_REAL(current_p_j);
                outMax := current_p_r;
                "DB".p_r := outMin + (value - inMin) / (inMax - inMin) * (outMax - outMin);
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l22b;
            ELSIF edge_p = 19 THEN
                (* Perform assignments of the 1st edge in location "p.l22b". *)
                (* Perform update of discrete variable "p.r". *)
                (*
                 * CIFscale: Input value 'inMin' is scaled to output value 'outMin'. Input value
                 * 'inMax' is scaled to output value 'outMax'. All other values are scaled by
                 * interpolating or extrapolating from those value pairs.
                 *)
                value := DINT_TO_REAL(current_p_j);
                inMin := current_p_r;
                inMax := DINT_TO_REAL(current_p_j);
                outMin := current_p_r;
                outMax := DINT_TO_REAL(current_p_j);
                "DB".p_r := outMin + (value - inMin) / (inMax - inMin) * (outMax - outMin);
                (* Perform update of current-location variable for automaton "p". *)
                "DB".p := p_l23;
            END_IF;
        END_IF;
    END_WHILE;

    (*------------------------------------------------------
     * CIF model overview:
     *
     * ----
     * Group "g":
     *
     * - Input variable "g.inp".
     *
     * ----
     * Automaton "p":
     *
     * - Discrete variable "p.b".
     * - Discrete variable "p.j".
     * - Discrete variable "p.r".
     * - Continuous variable "p.t".
     * - Discrete variable "p.tv".
     * - Discrete variable "p.v1".
     * - Discrete variable "p.v2".
     * - Discrete variable "p.v3".
     * - Discrete variable "p.ve".
     * - Discrete variable "p.x".
     * - Discrete variable "p.y".
     *
     * - PLC current-location variable for automaton "p".
     *   The current location of the automaton. It amongst others contains the edges that are considered for taking an event.
     *
     * - PLC edge selection variable "edge_p".
     *   Unique edge number of the edge within the automaton that was selected to perform the considered event.
     *
     * - No use of uncontrollable events.
     *
     * - Controllable event "p.evt".
     *------------------------------------------------------ *)
END_ORGANIZATION_BLOCK
