//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

input real inp;

cont c der -1;
alg real a = inp;

automaton aut:
  controllable e;
  uncontrollable f;

  disc real r;
  disc bool b;
  disc int i;
  disc tuple(int a,b) tii;

  location l1:
    initial;
    edge e do r := inp goto l2;                  // input var ref

  location l2:
    edge e do r := a + c' goto l3;               // alg + deriv refs

  location l3:
    edge e do r := <real>i goto l4;              // cast int to real

  location l4:
    edge e do b := not b goto l5;                // not

  location l5:
    edge e do b := b => b goto l6;               // =>

  location l6:
    edge e do r := i / i goto l7;                // division on integers

  location l7:
    edge e do b := if b: b else b end goto l8;   // 'if' expr -> func over state

  location l8:
    edge e do i := tii[0] goto l9;               // tuple proj

  location l9:
    edge e do r := abs(r) goto l10;              // abs

  location l10:
    edge e do r := exp(r) goto l11;              // exp

  location l11:
    edge e do r := ln(r) goto l12;               // ln

  location l12:
    edge e do r := log(r) goto l13;              // log

  location l13:
    edge e do i := min(i, i) goto l14;           // min

  location l14:
    edge e do r := min(i, r) goto l15;           // min

  location l15:
    edge e do r := min(r, i) goto l16;           // min

  location l16:
    edge e do r := min(r, r) goto l17;           // min

  location l17:
    edge e do i := max(i, i) goto l18;           // max

  location l18:
    edge e do r := max(i, r) goto l19;           // max

  location l19:
    edge e do r := max(r, i) goto l20;           // max

  location l20:
    edge e do r := max(r, r) goto l21;           // max

  location l21:
    edge e do r := sqrt(r) goto l22;             // sqrt

  location l22:
    edge e do r := asin(r) goto l23;             // asin

  location l23:
    edge e do r := acos(r) goto l24;             // acos

  location l24:
    edge e do r := atan(r) goto l25;             // atan

  location l25:
    edge e do r := sin(r) goto l26;              // sin

  location l26:
    edge e do r := cos(r) goto l27;              // cos

  location l27:
    edge e do r := tan(r) goto l28;              // tan

  location l28:
    edge e do tii := (i, i + 1) goto l29;        // tuple literal

  location l29:
    edge e do (r, i) := (r * 2, i + 1) goto l30; // tuple addressable

  location l30:
    edge f when b or b goto l31;                 // disjunction in guard

  location l31:
    edge f when b goto l32;                      // disjunction between guards of 'f'

  location l32;
end
