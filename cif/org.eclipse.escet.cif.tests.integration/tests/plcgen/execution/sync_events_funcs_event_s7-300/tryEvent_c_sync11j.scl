FUNCTION tryEvent_c_sync11j: BOOL
{ S7_Optimized_Access := 'false' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_monitor11c_count: DINT;
        current_monitor11d_count: DINT;
        current_sync11_count10: DINT;
        edge_sync11: BOOL;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform controllable event "c_sync11j".
     *
     * - Automaton "sync11" must always synchronize.
     *
     * - Automaton "monitor11a" may synchronize.
     * - Automaton "monitor11b" may synchronize.
     * - Automaton "monitor11c" may synchronize.
     * - Automaton "monitor11d" may synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edge of automaton "sync11" to synchronize for event "c_sync11j".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edge being tested:
     * - Location:
     *   - 10th edge in the location
     ***********)
    IF "DB".sync11_count10 < 3 THEN
        edge_sync11 := 0;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "c_sync11j" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_monitor11c_count := "DB".monitor11c_count;
        current_monitor11d_count := "DB".monitor11d_count;
        current_sync11_count10 := "DB".sync11_count10;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sync11". *)
        IF edge_sync11 = 0 THEN
            (* Perform assignments of the 10th edge of automaton "sync11". *)
            (* Perform update of discrete variable "sync11.count10". *)
            "DB".sync11_count10 := current_sync11_count10 + 1;
        END_IF;
        (*******************************
         * Perform the assignments of each optionally synchronizing automaton.
         *******************************)
        IF TRUE THEN
            (***********
             * Perform assignments of automaton "monitor11c".
             *
             * Location:
             *
             * Perform assignments of the 10th edge of automaton "monitor11c".
             ***********)
            (* Perform update of discrete variable "monitor11c.count". *)
            "DB".monitor11c_count := current_monitor11c_count + 1000;
        END_IF;
        (* Perform assignments of automaton "monitor11d". *)
        IF current_monitor11d_count < 1000 THEN
            (***********
             * Location:
             *
             * Perform assignments of the 10th edge of automaton "monitor11d".
             ***********)
            (* Perform update of discrete variable "monitor11d.count". *)
            "DB".monitor11d_count := current_monitor11d_count + 500;
        END_IF;
        (* Automaton "monitor11a" has no assignments to perform. *)
        (* Automaton "monitor11b" has no assignments to perform. *)
    END_IF;

    (* Return event execution progress. *)
    tryEvent_c_sync11j := funcIsProgress;
    RETURN;
END_FUNCTION
