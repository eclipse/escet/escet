FUNCTION tryEvent_c_sync11b: BOOL
{ S7_Optimized_Access := 'false' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_sync11_count02: DINT;
        edge_sync11: BOOL;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
        dummyVar3: DINT;
        dummyVar4: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform controllable event "c_sync11b".
     *
     * - Automaton "sync11" must always synchronize.
     *
     * - Automaton "monitor11a" may synchronize.
     * - Automaton "monitor11b" may synchronize.
     * - Automaton "monitor11c" may synchronize.
     * - Automaton "monitor11d" may synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edge of automaton "sync11" to synchronize for event "c_sync11b".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edge being tested:
     * - Location:
     *   - 2nd edge in the location
     ***********)
    IF "DB".sync11_count02 < 3 THEN
        edge_sync11 := 0;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "c_sync11b" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sync11_count02 := "DB".sync11_count02;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sync11". *)
        IF edge_sync11 = 0 THEN
            (* Perform assignments of the 2nd edge of automaton "sync11". *)
            (* Perform update of discrete variable "sync11.count02". *)
            "DB".sync11_count02 := current_sync11_count02 + 1;
        END_IF;
        (*******************************
         * Perform the assignments of each optionally synchronizing automaton.
         *******************************)
        (* There are no assignments to perform for automata that may synchronize. *)
        (* Automaton "monitor11a" has no assignments to perform. *)
        (* Automaton "monitor11b" has no assignments to perform. *)
        (* Automaton "monitor11c" has no assignments to perform. *)
        (* Automaton "monitor11d" has no assignments to perform. *)
    END_IF;

    (* Return event execution progress. *)
    tryEvent_c_sync11b := funcIsProgress;
    RETURN;
END_FUNCTION
