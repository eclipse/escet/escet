//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

input bool in_bool;
input int in_int;

controllable evt;

automaton a:
  disc bool out_bool;
  disc int out_int;

  alg bool out_alg_bool = not out_bool;

  location:
    initial;
    marked;

    edge evt when out_bool != in_bool do out_bool := in_bool;
    edge evt when out_int  != in_int  do out_int  := in_int;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Adapt the 'io_main_func.csv' I/O table file to connect the following
//   variables to appropriate input/output ports, adapting only the addresses
//   as needed:
//   - Inputs:
//     - in_bool to a boolean input port
//     - in_int to an integer input port
//   - Outputs:
//     - a.out_alg_bool to a boolean output port
//     - a.out_bool to a boolean output port
//     - a.out_int to an integer output port
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code.
// - Perform the experiments listed below. These experiments must be
//   performed manually, as the automatic comparison script does not
//   support this model.

// ##############################################
// Expected result.
// ##############################################
//
// Set certain values on the input ports, and they should get mirrored (copied)
// to the output port of the same type. And whatever value the 'out_bool' output
// port has, the 'out_alg_bool' should have the inverse value. Try the following
// values (checking 'out_alg_bool' as well, where relevant):
//
// input port       input value     output port     output value
// -------------------------------------------------------------
// in_bool          true            out_bool        true
// in_bool          false           out_bool        false
// in_bool          true            out_bool        true
// in_bool          false           out_bool        false
//
// in_int           2               out_int         2
// in_int           -3              out_int         -3
// in_int           99              out_int         99
//
// in_bool          true            out_bool        true
// in_int           4               out_int         4
