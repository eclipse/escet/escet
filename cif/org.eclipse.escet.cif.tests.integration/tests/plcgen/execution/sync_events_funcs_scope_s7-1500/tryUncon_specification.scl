FUNCTION tryUncon_specification: BOOL
{ S7_Optimized_Access := 'true' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_sync05a: BYTE;
        current_sync05a_count: DINT;
        current_sync05b: BYTE;
        current_sync05b_count: DINT;
        edge_sync05a: BYTE;
        edge_sync05b: BYTE;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform uncontrollable event "u_sync05".
     *
     * - Automaton "sync05a" must always synchronize.
     * - Automaton "sync05b" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edges of automaton "sync05a" to synchronize for event "u_sync05".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "loc1":
     *   - 1st edge in the location
     * - Location "loc2":
     *   - 1st edge in the location
     *   - 2nd edge in the location
     ***********)
    IF "DB".sync05a = sync05a_loc1 AND "DB".sync05a_count < 5 THEN
        edge_sync05a := 0;
    ELSIF "DB".sync05a = sync05a_loc2 AND "DB".sync05a_count >= 0 THEN
        edge_sync05a := 1;
    ELSIF "DB".sync05a = sync05a_loc2 THEN
        edge_sync05a := 2;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Test edges of automaton "sync05b" to synchronize for event "u_sync05".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location "loc1":
         *   - 1st edge in the location
         *   - 2nd edge in the location
         * - Location "loc2":
         *   - 1st edge in the location
         *   - 2nd edge in the location
         ***********)
        IF "DB".sync05b = sync05b_loc1 THEN
            edge_sync05b := 0;
        ELSIF "DB".sync05b = sync05b_loc1 THEN
            edge_sync05b := 1;
        ELSIF "DB".sync05b = sync05b_loc2 THEN
            edge_sync05b := 2;
        ELSIF "DB".sync05b = sync05b_loc2 THEN
            edge_sync05b := 3;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "u_sync05" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sync05a := "DB".sync05a;
        current_sync05a_count := "DB".sync05a_count;
        current_sync05b := "DB".sync05b;
        current_sync05b_count := "DB".sync05b_count;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sync05a". *)
        IF edge_sync05a = 0 THEN
            (* Perform assignments of the 1st edge in location "sync05a.loc1". *)
            (* Perform update of current-location variable for automaton "sync05a". *)
            "DB".sync05a := sync05a_loc2;
        ELSIF edge_sync05a = 1 THEN
            (* Perform assignments of the 1st edge in location "sync05a.loc2". *)
            (* Perform update of discrete variable "sync05a.count". *)
            "DB".sync05a_count := current_sync05a_count + 1;
            (* Perform update of current-location variable for automaton "sync05a". *)
            "DB".sync05a := sync05a_loc1;
        ELSIF edge_sync05a = 2 THEN
            (* Perform assignments of the 2nd edge in location "sync05a.loc2". *)
            (* Perform update of discrete variable "sync05a.count". *)
            "DB".sync05a_count := (-1);
            (* Perform update of current-location variable for automaton "sync05a". *)
            "DB".sync05a := sync05a_loc3;
        END_IF;
        (* Perform assignments of automaton "sync05b". *)
        IF edge_sync05b = 0 THEN
            (* Perform assignments of the 1st edge in location "sync05b.loc1". *)
            (* Perform update of current-location variable for automaton "sync05b". *)
            "DB".sync05b := sync05b_loc2;
        ELSIF edge_sync05b = 1 THEN
            (* Perform assignments of the 2nd edge in location "sync05b.loc1". *)
            (* Perform update of discrete variable "sync05b.count". *)
            "DB".sync05b_count := (-1);
            (* Perform update of current-location variable for automaton "sync05b". *)
            "DB".sync05b := sync05b_loc3;
        ELSIF edge_sync05b = 2 THEN
            (* Perform assignments of the 1st edge in location "sync05b.loc2". *)
            (* Perform update of discrete variable "sync05b.count". *)
            "DB".sync05b_count := current_sync05b_count + 2;
            (* Perform update of current-location variable for automaton "sync05b". *)
            "DB".sync05b := sync05b_loc1;
        ELSIF edge_sync05b = 3 THEN
            (* Perform assignments of the 2nd edge in location "sync05b.loc2". *)
            (* Perform update of discrete variable "sync05b.count". *)
            "DB".sync05b_count := (-1);
            (* Perform update of current-location variable for automaton "sync05b". *)
            "DB".sync05b := sync05b_loc3;
        END_IF;
    END_IF;

    (* Return event execution progress. *)
    tryUncon_specification := funcIsProgress;
    RETURN;
END_FUNCTION
