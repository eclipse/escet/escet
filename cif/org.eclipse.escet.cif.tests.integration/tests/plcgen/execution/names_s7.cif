//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

plant $timer:
  controllable e;

  location loc1:
    initial;
    edge e goto loc2;

  location loc2;
end

plant DB0:
  controllable e;

  location loc1:
    initial;
    edge e goto loc2;

  location loc2;
end

plant MW10:
  controllable e;

  location loc1:
    initial;
    edge e goto loc2;

  location loc2;
end

plant DATA_BLOCK:
  controllable e;

  location loc1:
    initial;
    edge e goto loc2;

  location loc2;
end

plant begin:
  controllable e;

  location loc1:
    initial;
    edge e goto loc2;

  location loc2;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.
//   In case a '->' is given on a line, additionally accepted alternative
//   values are indicated, surrounded by single quotes (') and separated
//   by forward slashes (/).
// - If enumerations are eliminated, the value may be a number instead of
//   an enumeration literal. Depending on how the number is encoded, it
//   may be displayed differently in the PLC development environment.
// - Take special care in checking that the names of the variables given below
//   are identical to what their names are in the PLC.

// ##############################################
// Expected result.
// ##############################################
//
// begin_1 = begin_1_loc2    -> 'x_loc2' / '1' / 'true'
// DATA_BLOCK_1 = DATA_BLOCK_1_loc2    -> 'x_loc2' / '1' / 'true'
// DB0_1 = DB0_1_loc2    -> 'x_loc2' / '1' / 'true'
// MW10_1 = MW10_1_loc2    -> 'x_loc2' / '1' / 'true'
// timer_1 = timer_1_loc2    -> 'x_loc2' / '1' / 'true'
