//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Compatible enumerations, and ones with the literals in different orders.
enum E1 = A, B, C, D;
enum E2 = E, F;

group g:
  enum E3 = A, B, C, D; // Same order.
  enum E2 = F, E; // Different order.
end

// Type declarations.
type t1a = E1;
type t1b = t1a;
type t2a = g.E3;
type t2b = t2a;

// Constants.
const E1 c01 = A;
const E1 c02 = B;
const E2 c03 = E;
const E2 c04 = F;
const E1 c05 = g.A;
const E1 c06 = g.B;

const g.E3 c07 = g.A;
const g.E3 c08 = g.B;
const g.E2 c09 = g.E;
const g.E2 c10 = g.F;
const g.E3 c11 = A;
const g.E3 c12 = B;

// Discrete variables, guards and updates.
automaton aut:
  controllable evt;

  // Variables.
  disc E1 init01 = c01;
  disc E1 init02 = c02;
  disc E2 init03 = c03;
  disc E2 init04 = c04;
  disc E1 init05 = c05;
  disc E1 init06 = c06;

  disc g.E3 init07 = c07;
  disc g.E3 init08 = c08;
  disc g.E2 init09 = c09;
  disc g.E2 init10 = c10;
  disc g.E3 init11 = c11;
  disc g.E3 init12 = c12;

  disc E1 asgn01, asgn02, asgn05, asgn06;
  disc E2 asgn03, asgn04;
  disc g.E3 asgn07, asgn08, asgn11, asgn12;
  disc g.E2 asgn09, asgn10;

  disc t1a type_compat1 = A;
  disc t1b type_compat2 = B;
  disc t2a type_compat3 = g.C;
  disc t2b type_compat4 = g.D;

  disc bool eq1, eq2, eq3, eq4;
  disc bool ne1, ne2, ne3, ne4;

  disc E1 repeated_update = A;

  location loc1:
    initial;
    edge evt do
          // Simple assignments.
          asgn01 := init01,
          asgn02 := init02,
          asgn03 := init03,
          asgn04 := init04,
          asgn05 := init05,
          asgn06 := init06,
          asgn07 := init07,
          asgn08 := init08,
          asgn09 := init09,
          asgn10 := init10,
          asgn11 := init11,
          asgn12 := init12,

          // Type compatibility.
          type_compat1 := type_compat3,
          type_compat2 := type_compat4,
          type_compat3 := type_compat2,
          type_compat4 := type_compat1,

          // Comparison.
          eq1 := type_compat1 = type_compat1,
          eq2 := type_compat1 = type_compat2,
          eq3 := type_compat1 = type_compat3,
          eq4 := type_compat1 = type_compat4,

          ne1 := type_compat1 != type_compat1,
          ne2 := type_compat1 != type_compat2,
          ne3 := type_compat1 != type_compat3,
          ne4 := type_compat1 != type_compat4

      goto loc2;

    location loc2:
        // Guards and repeated updates to same variable.
        edge evt when repeated_update = A do repeated_update := B;
        edge evt when repeated_update = B do repeated_update := C;
        edge evt when repeated_update = C do repeated_update := D;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.
//   In case a '->' is given on a line, additionally accepted alternative
//   values are indicated, surrounded by single quotes (') and separated
//   by forward slashes (/).
// - If enumerations are eliminated, the value may be a number instead of
//   an enumeration literal. Depending on how the number is encoded, it
//   may be displayed differently in the PLC development environment.

// ##############################
// Expected result.
// ##############################################
//
// aut_init01 = A    -> '0' / '16#00'
// aut_init02 = B    -> '1' / '16#01'
// aut_init03 = E    -> '0' / 'false'
// aut_init04 = F    -> '1' / 'true'
// aut_init05 = A    -> '0' / '16#00'
// aut_init06 = B    -> '1' / '16#01'
//
// aut_init07 = g_A    -> 'A' / '0' / '16#00'
// aut_init08 = g_B    -> 'B' / '1' / '16#01'
// aut_init09 = g_E    -> '1' / 'true'
// aut_init10 = g_F    -> '0' / 'false'
// aut_init11 = g_A    -> 'A' / '0' / '16#00'
// aut_init12 = g_B    -> 'B' / '1' / '16#01'
//
// aut_asgn01 = A    -> '0' / '16#00'
// aut_asgn02 = B    -> '1' / '16#01'
// aut_asgn03 = E    -> '0' / 'false'
// aut_asgn04 = F    -> '1' / 'true'
// aut_asgn05 = A    -> '0' / '16#00'
// aut_asgn06 = B    -> '1' / '16#01'
//
// aut_asgn07 = g_A    -> 'A' / '0' / '16#00'
// aut_asgn08 = g_B    -> 'B' / '1' / '16#01'
// aut_asgn09 = g_E    -> '1' / 'true'
// aut_asgn10 = g_F    -> '0' / 'false'
// aut_asgn11 = g_A    -> 'A' / '0' / '16#00'
// aut_asgn12 = g_B    -> 'B' / '1' / '16#01'
//
// aut_type_compat1 = C    -> '2' / '16#02'
// aut_type_compat2 = D    -> '3' / '16#03'
// aut_type_compat3 = g_B    -> 'B' / '1' / '16#01'
// aut_type_compat4 = g_A    -> 'A' / '0' / '16#00'
//
// aut_eq1 = true
// aut_eq2 = false
// aut_eq3 = false
// aut_eq4 = false
//
// aut_ne1 = false
// aut_ne2 = true
// aut_ne3 = true
// aut_ne4 = true
//
// aut_repeated_update = D    -> '3' / '16#03'
