FUNCTION tryEvent_a_u: BOOL
{ S7_Optimized_Access := 'true' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_a_x: DINT;
        current_a_y: DINT;
        edge_a: BOOL;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
        dummyVar3: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform uncontrollable event "a.u".
     *
     * - Automaton "a" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edge of automaton "a" to synchronize for event "a.u".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edge being tested:
     * - Location:
     *   - 1st edge in the location
     ***********)
    IF "DB".a_x < 3 AND "DB".a_y > 0 THEN
        edge_a := 0;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "a.u" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_a_x := "DB".a_x;
        current_a_y := "DB".a_y;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "a". *)
        IF edge_a = 0 THEN
            (* Perform assignments of the 1st edge of automaton "a". *)
            (* Perform update of discrete variable "a.x". *)
            "DB".a_x := current_a_x + 1;
            (* Perform update of discrete variable "a.y". *)
            "DB".a_y := current_a_y - 1;
        END_IF;
    END_IF;

    (* Return event execution progress. *)
    tryEvent_a_u := funcIsProgress;
    RETURN;
END_FUNCTION
