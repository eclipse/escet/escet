//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.typechecker.declwrap;

import static org.eclipse.escet.cif.typechecker.CifTypesTypeChecker.transCifType;

import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.cif.common.CifTypeUtils;
import org.eclipse.escet.cif.common.CifValueUtils;
import org.eclipse.escet.cif.metamodel.cif.InputParameter;
import org.eclipse.escet.cif.metamodel.cif.types.CifType;
import org.eclipse.escet.cif.parser.ast.AInputParameter;
import org.eclipse.escet.cif.typechecker.CheckStatus;
import org.eclipse.escet.cif.typechecker.CifTypeChecker;
import org.eclipse.escet.cif.typechecker.ErrMsg;
import org.eclipse.escet.cif.typechecker.scopes.ParentScope;
import org.eclipse.escet.common.typechecker.SemanticException;

/** Input parameter declaration wrapper. */
public class InputParamDeclWrap extends DeclWrap<InputParameter> {
    /** The CIF AST representation of the input parameter. */
    private final AInputParameter astDecl;

    /**
     * Constructor for the {@link InputParamDeclWrap} class.
     *
     * @param tchecker The CIF type checker to use.
     * @param scope The parent scope of this declaration.
     * @param astDecl The CIF AST representation of the input parameter.
     * @param mmDecl The CIF metamodel representation of the input parameter.
     */
    public InputParamDeclWrap(CifTypeChecker tchecker, ParentScope<?> scope, AInputParameter astDecl,
            InputParameter mmDecl)
    {
        super(tchecker, scope, mmDecl);
        this.astDecl = astDecl;
    }

    @Override
    public String getName() {
        return mmDecl.getVariable().getName();
    }

    @Override
    public String getAbsName() {
        return CifTextUtils.getAbsName(mmDecl.getVariable());
    }

    @Override
    public void tcheckForUseImpl() {
        // Check for reserved names.
        checkName();

        // Get the type of the declaration. Resolve it in the parent scope
        // of the component definition, not in the parent scope of the
        // input parameter.
        tchecker.addToCycle(this);

        CifType type;
        try {
            type = transCifType(astDecl.type, scope.getParent(), tchecker);
        } finally {
            tchecker.removeFromCycle(this);
        }

        // Check for allowed types.
        if (CifTypeUtils.hasComponentLikeType(type)) {
            tchecker.addProblem(ErrMsg.DECL_INVALID_TYPE, type.getPosition(), "Input parameter", getAbsName(),
                    CifTextUtils.typeToStr(type));
            throw new SemanticException();
        }

        // Set the type.
        mmDecl.getVariable().setType(type);

        // This declaration is now checked 'for use'.
        status = CheckStatus.USE;
    }

    @Override
    public void tcheckFull() {
        // Check for use first, and make sure not already fully checked.
        tcheckForUse();
        if (isCheckedFull()) {
            return;
        }

        // Check for single-value type.
        CifType type = mmDecl.getVariable().getType();
        if (CifValueUtils.getPossibleValueCount(type) == 1) {
            tchecker.addProblem(ErrMsg.TYPE_ONE_VALUE, type.getPosition(), "", CifTextUtils.typeToStr(type),
                    "input parameter", CifTextUtils.getAbsName(mmDecl, false), "input parameter");
            // Non-fatal problem.
        }

        // This declaration is now fully checked.
        status = CheckStatus.FULL;
    }
}
