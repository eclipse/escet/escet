//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.checkers.checks;

import java.util.BitSet;
import java.util.EnumSet;
import java.util.Set;

import org.eclipse.escet.cif.checkers.CifCheckNoCompDefInst;
import org.eclipse.escet.cif.checkers.CifCheckViolations;
import org.eclipse.escet.cif.common.CifTypeUtils;
import org.eclipse.escet.cif.metamodel.cif.types.CifType;
import org.eclipse.escet.cif.metamodel.cif.types.DictType;
import org.eclipse.escet.cif.metamodel.cif.types.Field;
import org.eclipse.escet.cif.metamodel.cif.types.ListType;
import org.eclipse.escet.cif.metamodel.cif.types.SetType;
import org.eclipse.escet.cif.metamodel.cif.types.TupleType;

/** Check that limits how container types can be directly nested. */
public class TypeNoSpecificNestedTypesCheck extends CifCheckNoCompDefInst {
    /** Number of container types. */
    private static final int NUM_CONTAINER_TYPES = ContainerType.values().length;

    /** Constant for the constructor to express all directly nested combinations are allowed. */
    public static final boolean ALLOW_ALL = true;

    /** Constant for the constructor to express nesting containers is completely forbidden. */
    public static final boolean FORBID_ALL = false;

    /** Set with both types of list containers. */
    public static final Set<ContainerType> ALL_LISTS = EnumSet.of(ContainerType.ARRAY, ContainerType.NON_ARRAY_LIST);

    /** Set with all types of containers. */
    public static final Set<ContainerType> ALL_CONTAINERS = EnumSet.allOf(ContainerType.class);

    /**
     * Allowed directly nested combinations.
     *
     * <p>
     * Directly nested container type combination {@code (outerType, innerType)} are mapped to integers by
     * {@code innerType + NUM_CONTAINER_TYPES * outerType}. If the bit at that index is set, the combination is allowed.
     * </p>
     */
    private final BitSet allowed = new BitSet(NUM_CONTAINER_TYPES * NUM_CONTAINER_TYPES);

    /**
     * Constructor of the {@link TypeNoSpecificNestedTypesCheck} class.
     *
     * @param allowAll Whether the check initially allows all combinations of nested container types. Constants
     *     {@link #ALLOW_ALL} and {@link #FORBID_ALL} are available to enhance understanding of the initialization.
     */
    public TypeNoSpecificNestedTypesCheck(boolean allowAll) {
        if (allowAll) {
            allowed.set(0, NUM_CONTAINER_TYPES * NUM_CONTAINER_TYPES);
        } else {
            allowed.clear();
        }
    }

    /**
     * Daisy-chain method to allow the inner type of container directly inside the outer type of container.
     *
     * @param outerType Outer type of container.
     * @param innerType Inner type of container.
     * @return The updated instance.
     */
    public TypeNoSpecificNestedTypesCheck allow(ContainerType outerType, ContainerType innerType) {
        allowed.set(computeIndex(outerType, innerType));
        return this;
    }

    /**
     * Daisy-chain method to allow the inner type of container directly inside all of the outer types of container.
     *
     * @param outerTypes Outer types of container.
     * @param innerType Inner type of container.
     * @return The updated instance.
     */
    public TypeNoSpecificNestedTypesCheck allow(Set<ContainerType> outerTypes, ContainerType innerType) {
        return allow(outerTypes, EnumSet.of(innerType));
    }

    /**
     * Daisy-chain method to allow all of the inner types of container directly inside the outer type of container.
     *
     * @param outerType Outer type of container.
     * @param innerTypes Inner types of container.
     * @return The updated instance.
     */
    public TypeNoSpecificNestedTypesCheck allow(ContainerType outerType, Set<ContainerType> innerTypes) {
        return allow(EnumSet.of(outerType), innerTypes);
    }

    /**
     * Daisy-chain method to allow all of the inner types of container directly inside all of the outer types of
     * container.
     *
     * @param outerTypes Outer types of container.
     * @param innerTypes Inner types of container.
     * @return The updated instance.
     */
    public TypeNoSpecificNestedTypesCheck allow(Set<ContainerType> outerTypes, Set<ContainerType> innerTypes) {
        for (ContainerType outerType: outerTypes) {
            for (ContainerType innerType: innerTypes) {
                allow(outerType, innerType);
            }
        }
        return this;
    }

    /**
     * Daisy-chain method to forbid the inner type of container directly inside the outer type of container.
     *
     * @param outerType Outer type of container.
     * @param innerType Inner type of container.
     * @return The updated instance.
     */
    public TypeNoSpecificNestedTypesCheck forbid(ContainerType outerType, ContainerType innerType) {
        allowed.clear(computeIndex(outerType, innerType));
        return this;
    }

    /**
     * Daisy-chain method to forbid the inner type of container directly inside all of the outer types of container.
     *
     * @param outerTypes Outer types of container.
     * @param innerType Inner type of container.
     * @return The updated instance.
     */
    public TypeNoSpecificNestedTypesCheck forbid(Set<ContainerType> outerTypes, ContainerType innerType) {
        return forbid(outerTypes, EnumSet.of(innerType));
    }

    /**
     * Daisy-chain method to forbid all of the inner types of container directly inside the outer type of container.
     *
     * @param outerType Outer type of container.
     * @param innerTypes Inner types of container.
     * @return The updated instance.
     */
    public TypeNoSpecificNestedTypesCheck forbid(ContainerType outerType, Set<ContainerType> innerTypes) {
        return forbid(EnumSet.of(outerType), innerTypes);
    }

    /**
     * Daisy-chain method to forbid all of the inner types of container directly inside all of the outer types of
     * container.
     *
     * @param outerTypes Outer types of container.
     * @param innerTypes Inner types of container.
     * @return The updated instance.
     */
    public TypeNoSpecificNestedTypesCheck forbid(Set<ContainerType> outerTypes, Set<ContainerType> innerTypes) {
        for (ContainerType outerType: outerTypes) {
            for (ContainerType innerType: innerTypes) {
                forbid(outerType, innerType);
            }
        }
        return this;
    }

    /**
     * Is the allowance of nesting of an array or a non-array list type directly in the given outer type the same?
     *
     * @param outerType Outer type of container to assume.
     * @return Whether allowance of nesting an array or a non-array list type directly in the given outer type is the
     *     same.
     */
    private boolean isSameInnerListAllowance(ContainerType outerType) {
        int innerArrayNestingIndex = computeIndex(outerType, ContainerType.ARRAY);
        int innerNonArrayNestingIndex = computeIndex(outerType, ContainerType.NON_ARRAY_LIST);
        return allowed.get(innerArrayNestingIndex) == allowed.get(innerNonArrayNestingIndex);
    }

    /**
     * Is the allowance of nesting of the given inner type directly in an array or a non-array outer list type the same?
     *
     * @param innerType Inner type of container to assume.
     * @return Whether allowance of nesting the given inner type directly in an array or a non-array outer list type is
     *     the same.
     */
    private boolean isSameOuterListAllowance(ContainerType innerType) {
        int outerArrayNestingIndex = computeIndex(ContainerType.ARRAY, innerType);
        int outerNonArrayNestingIndex = computeIndex(ContainerType.NON_ARRAY_LIST, innerType);
        return allowed.get(outerArrayNestingIndex) == allowed.get(outerNonArrayNestingIndex);
    }

    /**
     * Compute the index in {@link #allowed} that contains the nested permission of the given outer and inner container
     * types.
     *
     * @param outerType Outer type of container.
     * @param innerType Inner type of container.
     * @return The index in {@link #allowed} for the given combination of container types.
     */
    private int computeIndex(ContainerType outerType, ContainerType innerType) {
        return innerType.ordinal() + NUM_CONTAINER_TYPES * outerType.ordinal();
    }

    @Override
    protected void preprocessListType(ListType listType, CifCheckViolations violations) {
        checkCombination(getContainerType(listType), listType.getElementType(), violations);
    }

    @Override
    protected void preprocessDictType(DictType dictType, CifCheckViolations violations) {
        checkCombination(ContainerType.DICTIONARY, dictType.getKeyType(), violations);
        checkCombination(ContainerType.DICTIONARY, dictType.getValueType(), violations);
    }

    @Override
    protected void preprocessSetType(SetType setType, CifCheckViolations violations) {
        checkCombination(ContainerType.SET, setType.getElementType(), violations);
    }

    @Override
    protected void preprocessTupleType(TupleType tupleType, CifCheckViolations violations) {
        for (Field fld: tupleType.getFields()) {
            checkCombination(ContainerType.TUPLE, fld.getType(), violations);
        }
    }

    /**
     * Get the kind of container type, or {@code null} if the type is not a container type.
     *
     * @param type Type to inspect.
     * @return Type of container type if a container type is detected, else {@code null}.
     */
    private static ContainerType getContainerType(CifType type) {
        type = CifTypeUtils.normalizeType(type); // Remove type declarations.

        // Decide on the type of container expressed by the type.
        if (type instanceof ListType lt) {
            return CifTypeUtils.isArrayType(lt) ? ContainerType.ARRAY : ContainerType.NON_ARRAY_LIST;
        } else if (type instanceof DictType) {
            return ContainerType.DICTIONARY;
        } else if (type instanceof SetType) {
            return ContainerType.SET;
        } else if (type instanceof TupleType) {
            return ContainerType.TUPLE;
        } else {
            return null;
        }
    }

    /**
     * Check whether the inner CIF type is allowed in the given outer container type.
     *
     * @param outerType Outer container type.
     * @param innerCifType CIF type directly inside the container.
     * @param violations Already reported violations. Is extended in-place if a violation is found.
     */
    private void checkCombination(ContainerType outerType, CifType innerCifType, CifCheckViolations violations) {
        ContainerType innerType = getContainerType(innerCifType);
        if (outerType == null || innerType == null) {
            return; // Accept everything that is not directly nesting container types.
        }
        if (allowed.get(computeIndex(outerType, innerType))) {
            return; // Allowed combination.
        }

        // Derive the phrase to use for the inner container type.
        String innerPhrase;
        if (ALL_LISTS.contains(innerType)) {
            boolean sameAllowance = isSameInnerListAllowance(outerType);
            innerPhrase = sameAllowance ? ContainerType.genericListContainerPhrase : innerType.phrase;
        } else {
            innerPhrase = innerType.phrase;
        }

        // Derive the phrase to use for the outer container type.
        String outerPhrase;
        if (ALL_LISTS.contains(outerType)) {
            boolean sameAllowance = isSameOuterListAllowance(innerType);
            outerPhrase = sameAllowance ? ContainerType.genericListContainerPhrase : outerType.phrase;
        } else {
            outerPhrase = outerType.phrase;
        }

        // Report a violation.
        violations.add(innerCifType, "A%s type is nested inside a%s type", innerPhrase, outerPhrase);
    }

    /** Available types of containers. */
    public enum ContainerType {
        /** Array type (fixed length lists). */
        ARRAY("n array"),

        /** Dictionary type. */
        DICTIONARY(" dictionary"),

        /** Non-array list type. */
        NON_ARRAY_LIST(" non-array list"),

        /** Set type. */
        SET(" set"),

        /** Tuple type. */
        TUPLE(" tuple");

        /**
         * Phrase to use when referring to a list type in general at the {@code ...} in a {@code "A..."} or
         * {@code "a..."} text.
         */
        public static String genericListContainerPhrase = " list";

        /** Phrase to use at the {@code ...} in a {@code "A..."} or {@code "a..."} text. */
        public final String phrase;

        /**
         * Constructor of the {@link ContainerType} class.
         *
         * @param phrase Phrase of the type, the {@code ...} in a {@code "A..."} or {@code "a..."} text.
         */
        private ContainerType(String phrase) {
            this.phrase = phrase;
        }
    }
}
