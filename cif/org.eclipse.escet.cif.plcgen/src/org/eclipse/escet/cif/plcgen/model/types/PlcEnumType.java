//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.model.types;

import java.util.List;

import org.eclipse.escet.cif.plcgen.model.expressions.PlcEnumLiteral;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcExpression;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.Lists;

/** PLC enum type. */
public class PlcEnumType extends PlcDerivedType {
    /** The literals of the enum type. */
    public final List<PlcEnumLiteral> literals;

    /**
     * Constructor for the {@link PlcEnumType} class.
     *
     * @param typeName Name of the enum type.
     * @param literalNames The names of the iterals of the enum type.
     */
    public PlcEnumType(String typeName, List<String> literalNames) {
        super(typeName);
        this.literals = literalNames.stream().map(name -> new PlcEnumLiteral(name, this)).collect(Lists.toList());
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof PlcEnumType enumType) || !typeName.equals(enumType.typeName)) {
            return false;
        }

        // Check the literals for being the same as well.
        Assert.areEqual(literals, enumType.literals,
                "Enum types with the same name must have the same literal values.");
        return true;
    }

    @Override
    public int hashCode() {
        return typeName.hashCode();
    }

    @Override
    public String toString() {
        return "PlcEnumType(\"" + typeName + "\")";
    }

    /**
     * Get an enum literal by its index.
     *
     * @param literalIndex Index of the enum literal to provide.
     * @return The requested enum literal.
     */
    public PlcExpression getLiteral(int literalIndex) {
        return literals.get(literalIndex);
    }
}
