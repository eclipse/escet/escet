//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.model.types;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.escet.common.java.Assert;

/** PLC struct type. */
public class PlcStructType extends PlcDerivedType {
    /** The fields of the struct type. */
    public final List<PlcStructField> fields;

    /**
     * Constructor of the {@link PlcStructType} class.
     *
     * @param typeName Name of the struct type.
     * @param fields The fields of the struct type.
     */
    public PlcStructType(String typeName, List<PlcStructField> fields) {
        super(typeName);
        this.fields = fields;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof PlcStructType otherStructType) || !typeName.equals(otherStructType.typeName)) {
            return false;
        }

        // Check the fields for being the same as well.
        // Same number of fields.
        Assert.areEqual(fields.size(), otherStructType.fields.size(),
                "Struct types with the same name must have the same number of fields.");
        // Pairwise fields must have the same name and type.
        List<PlcStructField> otherFields = otherStructType.fields;
        Assert.check(IntStream.range(0, fields.size())
                .allMatch(i -> fields.get(i).fieldName.equals(otherFields.get(i).fieldName)
                        && fields.get(i).type.equals(otherFields.get(i).type)));
        return true;
    }

    @Override
    public int hashCode() {
        return typeName.hashCode();
    }

    @Override
    public String toString() {
        return "PlcStructType(" + fields.stream().map(f -> f.toString()).collect(Collectors.joining(", ")) + ")";
    }
}
