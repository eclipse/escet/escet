//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.targets;

import java.util.EnumSet;
import java.util.List;

import org.eclipse.escet.cif.metamodel.cif.declarations.Constant;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.ExprAssociativity;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.ExprBinding;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.PlcFuncNotation;
import org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation;
import org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType;
import org.eclipse.escet.cif.plcgen.options.ConvertEnums;
import org.eclipse.escet.cif.plcgen.writers.TwinCatWriter;
import org.eclipse.escet.cif.plcgen.writers.Writer;

/** Code generator for the TwinCAT PLC type. */
public class TwinCatTarget extends PlcBaseTarget {
    /** Constructor of the {@link TwinCatTarget} class. */
    public TwinCatTarget() {
        super(PlcTargetType.TWINCAT, ConvertEnums.KEEP);
    }

    @Override
    public Writer getPlcCodeWriter() {
        return new TwinCatWriter(this);
    }

    @Override
    public boolean supportsArrays() {
        return true;
    }

    @Override
    public boolean supportsConstant(Constant constant) {
        return commonSupportedConstants(constant);
    }

    @Override
    public EnumSet<PlcFuncNotation> getSupportedFuncNotations(PlcFuncOperation funcOper, int numArgs) {
        EnumSet<PlcFuncNotation> funcSupport = super.getSupportedFuncNotations(funcOper, numArgs);

        // Support short circuit AND / OR.
        if (funcOper == PlcFuncOperation.AND_SHORT_CIRCUIT_OP || funcOper == PlcFuncOperation.OR_SHORT_CIRCUIT_OP) {
            funcSupport = EnumSet.copyOf(funcSupport);
            funcSupport.addAll(PlcFuncNotation.ALL);
            return funcSupport;
        }

        return funcSupport;
    }

    @Override
    public int getExprPriority(ExprBinding exprBinding) {
        return switch (exprBinding) {
            case UNARY_NEGATE, UNARY_NOT -> 1;
            case BINARY_DIV, BINARY_MOD, BINARY_MUL -> 3;
            case BINARY_ADD, BINARY_SUB -> 4;
            case BINARY_GREATER_EQUAL, BINARY_GREATER_THAN, BINARY_LESS_EQUAL, BINARY_LESS_THAN -> 5;
            // Documentation claims equality to be 6, but 'a < a = a < a' throws an error comparing a boolean with the
            // type of 'a'. It must be written as '(a < a) = (a < a)' instead.
            case BINARY_EQUAL, BINARY_UNEQUAL -> 5;
            case BINARY_AND -> 7;
            case BINARY_XOR -> 9; // Different from the default.
            case BINARY_OR -> 9;
            case NO_PRIORITY -> Integer.MAX_VALUE;
        };
    }

    @Override
    public ExprAssociativity getExprAssociativity(ExprBinding exprBinding) {
        if (EnumSet.of(ExprBinding.BINARY_LESS_THAN, ExprBinding.BINARY_LESS_EQUAL,
                ExprBinding.BINARY_GREATER_THAN, ExprBinding.BINARY_GREATER_EQUAL,
                ExprBinding.BINARY_EQUAL, ExprBinding.BINARY_UNEQUAL).contains(exprBinding))
        {
            return ExprAssociativity.ALWAYS;
        }
        return super.getExprAssociativity(exprBinding);
    }

    @Override
    public List<PlcElementaryType> getSupportedIntegerTypes() {
        return PlcElementaryType.INTEGER_TYPES_64;
    }

    @Override
    public List<PlcElementaryType> getSupportedRealTypes() {
        return PlcElementaryType.REAL_TYPES_64;
    }

    @Override
    public List<PlcElementaryType> getSupportedBitStringTypes() {
        return PlcElementaryType.BIT_STRING_TYPES_64;
    }

    @Override
    public String getPathSuffixReplacement() {
        return "_twincat";
    }
}
