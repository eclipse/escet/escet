//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.generators;

import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Lists.listc;
import static org.eclipse.escet.common.java.Maps.map;
import static org.eclipse.escet.common.java.Sets.set;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import org.eclipse.escet.cif.common.CifEnumUtils.EnumDeclEqHashWrap;
import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.cif.common.CifTypeUtils;
import org.eclipse.escet.cif.common.TypeEqHashWrap;
import org.eclipse.escet.cif.metamodel.cif.declarations.EnumDecl;
import org.eclipse.escet.cif.metamodel.cif.declarations.EnumLiteral;
import org.eclipse.escet.cif.metamodel.cif.declarations.TypeDecl;
import org.eclipse.escet.cif.metamodel.cif.types.BoolType;
import org.eclipse.escet.cif.metamodel.cif.types.CifType;
import org.eclipse.escet.cif.metamodel.cif.types.EnumType;
import org.eclipse.escet.cif.metamodel.cif.types.Field;
import org.eclipse.escet.cif.metamodel.cif.types.IntType;
import org.eclipse.escet.cif.metamodel.cif.types.ListType;
import org.eclipse.escet.cif.metamodel.cif.types.RealType;
import org.eclipse.escet.cif.metamodel.cif.types.TupleType;
import org.eclipse.escet.cif.metamodel.cif.types.TypeRef;
import org.eclipse.escet.cif.plcgen.generators.typegen.PlcDerivedTypeData;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcDataVariable;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcExpression;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcIntLiteral;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcVarExpression;
import org.eclipse.escet.cif.plcgen.model.types.PlcArrayType;
import org.eclipse.escet.cif.plcgen.model.types.PlcDerivedType;
import org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType;
import org.eclipse.escet.cif.plcgen.model.types.PlcEnumType;
import org.eclipse.escet.cif.plcgen.model.types.PlcStructField;
import org.eclipse.escet.cif.plcgen.model.types.PlcStructType;
import org.eclipse.escet.cif.plcgen.model.types.PlcType;
import org.eclipse.escet.cif.plcgen.options.ConvertEnums;
import org.eclipse.escet.cif.plcgen.targets.PlcTarget;
import org.eclipse.escet.common.java.Assert;

/** Generator for converting CIF types to PLC types. */
public class DefaultTypeGenerator implements TypeGenerator {
    /** Generator for creating clash-free names in the generated code. */
    private final NameGenerator nameGenerator;

    /** Standard integer type. */
    private final PlcElementaryType standardIntType;

    /** Standard real type. */
    private final PlcElementaryType standardRealType;

    /** Mapping from CIF tuple types wrapped in {@link TypeEqHashWrap} instances to their structure type. */
    private final Map<TypeEqHashWrap, PlcStructType> structTypes = map();

    /** Factory for converting, storing and retrieving CIF enumeration types. */
    private final EnumDeclDataFactory enumDeclDataFactory;

    /**
     * Generated derived types with their direct dependencies.
     *
     * <p>
     * The list is built bottom-up, dependencies are always earlier in the list.
     * </p>
     */
    private final List<PlcDerivedTypeData> derivedTypesAndDeps = list();

    /**
     * Constructor of the {@link DefaultTypeGenerator} class.
     *
     * @param target PLC target.
     * @param nameGenerator Generator for creating clash-free names in the generated code.
     */
    public DefaultTypeGenerator(PlcTarget target, NameGenerator nameGenerator) {
        this.nameGenerator = nameGenerator;
        standardIntType = target.getStdIntegerType();
        standardRealType = target.getStdRealType();

        ConvertEnums selectedEnumConversion = target.getActualEnumerationsConversion();
        enumDeclDataFactory = switch (selectedEnumConversion) {
            case CONSTS -> new ConstsEnumDeclsFactory(target, nameGenerator, derivedTypesAndDeps);
            case INTS -> new IntegersEnumDeclsFactory(target, derivedTypesAndDeps);
            case KEEP -> new KeepEnumDeclsFactory(nameGenerator, derivedTypesAndDeps);

            // AUTO should never happen.
            default -> throw new AssertionError("Unexpected enumeration conversion \""
                    + selectedEnumConversion + "\" found.");
        };
    }

    @Override
    public PlcType convertType(CifType type) {
        if (type instanceof BoolType) {
            return PlcElementaryType.BOOL_TYPE;
        } else if (type instanceof IntType) {
            return standardIntType;
        } else if (type instanceof RealType) {
            return standardRealType;
        } else if (type instanceof TypeRef typeRef) {
            return convertType(typeRef.getType().getType());
        } else if (type instanceof TupleType tupleType) {
            return convertTupleType(tupleType);
        } else if (type instanceof EnumType enumType) {
            EnumDeclData declData = getOrCreateConvertedEnumDecl(enumType.getEnum());
            return declData.plcEnumType;
        } else if (type instanceof ListType arrayType) {
            int size = arrayType.getLower();
            Assert.check(CifTypeUtils.isArrayType(arrayType));
            PlcType elemType = convertType(arrayType.getElementType());
            return new PlcArrayType(0, (size == 0) ? 0 : size - 1, elemType);
        } else {
            throw new RuntimeException("Unexpected type: " + type);
        }
    }

    @Override
    public PlcStructType convertTupleType(TupleType tupleType) {
        TypeEqHashWrap typeWrap = new TypeEqHashWrap(tupleType, true, false);
        PlcStructType structType = structTypes.get(typeWrap);
        if (structType == null) {
            structType = makePlcStructType(tupleType);
            structTypes.put(typeWrap, structType);
        }
        return structType;
    }

    @Override
    public List<PlcDerivedTypeData> getDerivedTypesAndDeps() {
        return derivedTypesAndDeps;
    }

    @Override
    public List<PlcDataVariable> getCreatedConstants() {
        return enumDeclDataFactory.getCreatedConstants();
    }

    /**
     * Make a new structure type.
     *
     * @param tupleType Tuple type to convert.
     * @return The created structure type.
     */
    private PlcStructType makePlcStructType(TupleType tupleType) {
        // Convert the fields.
        List<Field> tupleFields = tupleType.getFields();
        List<PlcStructField> structFields = listc(tupleFields.size());
        Set<PlcDerivedType> childDerivedTypes = set();
        int fieldNumber = 1;
        for (Field field: tupleFields) {
            // Convert the field.
            String fieldName = "field" + String.valueOf(fieldNumber);
            PlcType fieldType = convertType(field.getType());
            structFields.add(new PlcStructField(fieldName, fieldType));
            fieldNumber++;

            // Collect the type of the field as well if it is a derived type.
            if (fieldType instanceof PlcDerivedType childDerivedType) {
                childDerivedTypes.add(childDerivedType);
            }
        }

        // Construct the structure type.
        String typeName;
        if (tupleType.eContainer() instanceof TypeDecl typeDecl) {
            String structName = CifTextUtils.getAbsName(typeDecl, false);
            typeName = nameGenerator.generateGlobalName(structName, true);
        } else {
            String structName = "TupleStruct" + tupleType.getFields().size();
            typeName = nameGenerator.generateGlobalName(structName, false);
        }
        PlcStructType structType = new PlcStructType(typeName, structFields);

        // Store the type and return it.
        derivedTypesAndDeps.add(new PlcDerivedTypeData(structType, childDerivedTypes));
        return structType;
    }

    @Override
    public void convertEnumDecl(EnumDecl enumDecl) {
        getOrCreateConvertedEnumDecl(enumDecl);
    }

    @Override
    public PlcExpression convertEnumLiteral(EnumLiteral enumLit) {
        EnumDecl enumDecl = (EnumDecl)enumLit.eContainer();
        EnumDeclData declData = getOrCreateConvertedEnumDecl(enumDecl);
        return declData.getLiteralValue(enumDecl.getLiterals().indexOf(enumLit));
    }

    /**
     * Retrieve or create a converted representation of the requested enumeration declaration.
     *
     * @param enumDecl Enumeration declaration to retrieve or create.
     * @return The declaration data associated with the supplied enumeration declaration.
     */
    private EnumDeclData getOrCreateConvertedEnumDecl(EnumDecl enumDecl) {
        return enumDeclDataFactory.getOrCreateConvertedEnumDecl(enumDecl);
    }

    /** Factory that converts and stores enumeration declarations. */
    private abstract static class EnumDeclDataFactory {
        /** Generated derived types. */
        protected final List<PlcDerivedTypeData> createdDerivedTypes;

        /**
         * Constructor of the {@link EnumDeclDataFactory} class.
         *
         * @param createdDerivedTypes Generated derived types.
         */
        protected EnumDeclDataFactory(List<PlcDerivedTypeData> createdDerivedTypes) {
            this.createdDerivedTypes = createdDerivedTypes;
        }

        /**
         * Retrieve or create a converted representation of the requested enumeration declaration.
         *
         * @param enumDecl Enumeration declaration to retrieve or create.
         * @return The declaration data associated with the supplied enumeration declaration.
         */
        protected abstract EnumDeclData getOrCreateConvertedEnumDecl(EnumDecl enumDecl);

        /**
         * Obtain the created constants representing enumeration literal values of the factory.
         *
         * @return The created constants representing enumeration literal values.
         */
        public List<PlcDataVariable> getCreatedConstants() {
            return List.of();
        }
    }

    /** An {@link EnumDeclDataFactory} that converts CIF enumerations to PLC enumeration derived types. */
    private static class KeepEnumDeclsFactory extends EnumDeclDataFactory {
        /** Generator for creating clash-free names in the generated code. */
        private final NameGenerator nameGenerator;

        /**
         * Mapping from CIF enumerations to their PLC declaration data.
         *
         * <p>
         * The map combines compatible enumerations (as defined by {@link CifTypeUtils#areEnumsCompatible}) to one PLC
         * enumeration type.
         * </p>
         */
        private final Map<EnumDeclEqHashWrap, EnumDeclData> enumTypes = map();

        /**
         * Constructor of the {@link KeepEnumDeclsFactory} class.
         *
         * @param nameGenerator Generator for creating clash-free names in the generated code.
         * @param createdDerivedTypes Generated derived types.
         */
        public KeepEnumDeclsFactory(NameGenerator nameGenerator, List<PlcDerivedTypeData> createdDerivedTypes) {
            super(createdDerivedTypes);
            this.nameGenerator = nameGenerator;
        }

        @Override
        protected EnumDeclData getOrCreateConvertedEnumDecl(EnumDecl enumDecl) {
            EnumDeclEqHashWrap representativeEnumDecl = new EnumDeclEqHashWrap(enumDecl);
            return enumTypes.computeIfAbsent(representativeEnumDecl, key -> convertToPlcEnumType(enumDecl));
        }

        /**
         * Convert a CIF enumeration to a PLC enum type.
         *
         * @param enumDecl Enumeration declaration to convert.
         * @return Data of the converted enumeration.
         */
        private EnumDeclData convertToPlcEnumType(EnumDecl enumDecl) {
            // Create name for enum.
            String typeName = nameGenerator.generateGlobalName(enumDecl);

            // Construct the names of the literals.
            List<String> literalNames = enumDecl.getLiterals().stream()
                    .map(lit -> nameGenerator.generateGlobalName(lit)).toList();
            PlcEnumType plcEnumType = new PlcEnumType(typeName, literalNames);
            PlcExpression[] litValues = plcEnumType.literals.toArray(PlcExpression[]::new);

            // Store the created enumeration type and return the constructed data to the caller.
            createdDerivedTypes.add(new PlcDerivedTypeData(plcEnumType, Set.of()));
            return new EnumDeclData(plcEnumType, litValues);
        }
    }

    /** An {@link EnumDeclDataFactory} that converts CIF enumerations to integer PLC constants. */
    private static class ConstsEnumDeclsFactory extends EnumDeclDataFactory {
        /** Mapping from CIF enumerations to their PLC declaration data. */
        private final Map<EnumDecl, EnumDeclData> enumTypes = map();

        /** PLC target to generate code for. */
        private final PlcTarget target;

        /** Generator for creating clash-free names in the generated code. */
        private final NameGenerator nameGenerator;

        /** Generated constants. */
        private final List<PlcDataVariable> createdConstants = list();

        /**
         * Constructor of the {@link ConstsEnumDeclsFactory} class.
         *
         * @param target PLC target to generate code for.
         * @param nameGenerator Generator for creating clash-free names in the generated code.
         * @param createdDerivedTypes Generated derived types.
         */
        public ConstsEnumDeclsFactory(PlcTarget target, NameGenerator nameGenerator,
                List<PlcDerivedTypeData> createdDerivedTypes)
        {
            super(createdDerivedTypes);
            this.target = target;
            this.nameGenerator = nameGenerator;
        }

        @Override
        protected EnumDeclData getOrCreateConvertedEnumDecl(EnumDecl enumDecl) {
            return enumTypes.computeIfAbsent(enumDecl, key -> convertToPlcConstants(enumDecl));
        }

        /**
         * Convert a CIF enumeration to a set of constants.
         *
         * @param enumDecl Enumeration declaration to convert.
         * @return Data of the converted enumeration.
         */
        private EnumDeclData convertToPlcConstants(EnumDecl enumDecl) {
            // Create the type of the converted enumeration literal values.
            int numLiterals = enumDecl.getLiterals().size();
            PlcType valueType = PlcElementaryType.getTypeByRequiredCount(numLiterals,
                    target.getSupportedBitStringTypes());

            // Construct constants.
            PlcExpression[] litValues = new PlcExpression[numLiterals];
            for (int idx = 0; idx < numLiterals; idx++) {
                String name = nameGenerator.generateGlobalName(enumDecl.getLiterals().get(idx));
                String targetText = target.getUsageVariableText(PlcVariablePurpose.CONSTANT, name);
                PlcDataVariable constVar = new PlcDataVariable(targetText, name, valueType, null,
                        new PlcIntLiteral(idx, valueType));
                createdConstants.add(constVar);
                litValues[idx] = new PlcVarExpression(constVar);
            }

            // Return the data to the caller.
            return new EnumDeclData(valueType, litValues);
        }

        @Override
        public List<PlcDataVariable> getCreatedConstants() {
            return createdConstants;
        }
    }

    /** An {@link EnumDeclDataFactory} that converts CIF enumerations to integer PLC literals. */
    private static class IntegersEnumDeclsFactory extends EnumDeclDataFactory {
        /** Mapping from CIF enumerations to their PLC declaration data. */
        private final Map<EnumDecl, EnumDeclData> enumTypes = map();

        /** PLC target to generate code for. */
        private final PlcTarget target;

        /**
         * Constructor of the {@link IntegersEnumDeclsFactory} class.
         *
         * @param target PLC target to generate code for.
         * @param createdDerivedTypes Generated derived types.
         */
        public IntegersEnumDeclsFactory(PlcTarget target, List<PlcDerivedTypeData> createdDerivedTypes) {
            super(createdDerivedTypes);
            this.target = target;
        }

        @Override
        protected EnumDeclData getOrCreateConvertedEnumDecl(EnumDecl enumDecl) {
            return enumTypes.computeIfAbsent(enumDecl, key -> convertToPlcIntegers(enumDecl));
        }

        /**
         * Convert a CIF enumeration to a set of integers.
         *
         * @param enumDecl Enumeration declaration to convert.
         * @return Data of the converted enumeration.
         */
        private EnumDeclData convertToPlcIntegers(EnumDecl enumDecl) {
            // Create the type of the converted enumeration literal values.
            int numLiterals = enumDecl.getLiterals().size();
            PlcType valueType = PlcElementaryType.getTypeByRequiredCount(numLiterals,
                    target.getSupportedBitStringTypes());

            // Construct the literal values.
            PlcExpression[] litValues = IntStream.range(0, numLiterals)
                    .mapToObj(idx -> new PlcIntLiteral(idx, valueType)).toArray(PlcExpression[]::new);

            // Return the data to the caller.
            return new EnumDeclData(valueType, litValues);
        }
    }

    /** PLC enumeration declaration data of a CIF enumeration declaration. */
    private static class EnumDeclData {
        /** The type of the converted enumeration declaration. */
        public final PlcType plcEnumType;

        /** Values for all literals of the converted enumeration declaration. */
        private final PlcExpression[] literalValues;

        /**
         * Constructor of the {@link EnumDeclData} class.
         *
         * @param plcEnumType The type of the converted enumeration declaration.
         * @param literalValues Values for all literals of the converted enumeration declaration.
         */
        private EnumDeclData(PlcType plcEnumType, PlcExpression[] literalValues) {
            this.plcEnumType = plcEnumType;
            this.literalValues = literalValues;
        }

        /**
         * Convert an enumeration literal of the enumeration declaration to its PLC expression.
         *
         * @param litIndex Index of the literal in the CIF enumeration declaration.
         * @return The associated PLC expression.
         */
        public PlcExpression getLiteralValue(int litIndex) {
            return literalValues[litIndex];
        }
    }
}
