//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

include::../_part_attributes.asciidoc[]

[[lang-tut-svg-chapter-inkscape]]
== Inkscape

indexterm:[SVG,Inkscape]
indexterm:[Inkscape]
SVG images can best be edited with a drawing program that supports vector graphics.
One such program is link:https://inkscape.org/[Inkscape], a free and open source vector graphics editor that uses SVG as its native file format, and is available for many platforms, including Microsoft Windows, macOS, and Linux.
In the CIF tutorial and the rest of the CIF documentation, we'll use Inkscape to create and edit SVG images.

It is beyond the scope of this tutorial to explain how Inkscape works.
For that, see the link:https://inkscape.org/[Inkscape website].
Here, we focus on some of the specifics for working with Inkscape to create SVG images for SVG visualization of CIF models.

Here is a screenshot of the Inkscape interface:

image::{lang-tut-imgsdir}/svg/inkscape/inkscape.png[]

indexterm:[Inkscape,changing ids]

=== Changing ids

SVG images consist of elements, also called or objects.
Each object can be given an id, to be able to uniquely identify the object within the image.
To change the id of an object in Inkscape, right click on the object, and choose menu:Object Properties...[].
The _Object Properties_ tab will appear on the right of the interface, which looks like this:

image::{lang-tut-imgsdir}/svg/inkscape/inkscape_props_xml.png[]

In this tab, you can change the object's id, by entering a new id in the _ID_ field, and clicking the btn:[Set] button.

indexterm:[Inkscape,XML editor]

=== XML editor

Inkscape also features an XML editor, that can be used to view and edit the underlying XML representation of the SVG image.
The screenshot above also shows Inkscape's _XML Editor_ as a window, which can be opened via menu:Edit[XML Editor...].
The XML editor can be useful to find out the exact structure of the XML tree, and to see the precise definitions that were used to represent the objects that you created on Inkscape's canvas.
Panels like the _Object Properties_ panel and the _XML Editor_ panel can be fixed to the right side of the interface as tabs or be used as floating windows.
Simply drag each panel to its preferred location.

indexterm:[Inkscape,document size]
indexterm:[Inkscape,width]
indexterm:[Inkscape,height]

[[lang-tut-svg-chapter-inkscape-size]]
=== Document size

It is recommended to always set the correct size of the image, before adding any shapes or text labels.
To do this, select menu:File[Document Properties...], to open the _Document Properties_ window.
On the _Display_ tab, we recommend to set _Format_ to _px_ and _Display units_ to _px_.
You can then enter the _Width_ and _Height_ of the image.
Finally, in the _XML Editor_ panel, select the root SVG element (`<svg:svg`... node) by left clicking it, and delete the _viewBox_ property by clicking the trash can icon in front of it.
This ensures that the coordinates in the XML tree are the same as those displayed on the Inkscape interface.

indexterm:[Inkscape,coordinate system]

=== Coordinate systems

The coordinate system used by SVG and Inkscape uses the upper left corner of the canvas as origin.
Inkscape uses x and y coordinates of objects relative to the outside of their border (called _stroke_ in Inkscape), while SVG uses x and y coordinates of objects relative to the middle of their border.
It is good to be aware of this, when making CIF/SVG output mappings.

indexterm:[Inkscape,text area]

[[lang-tut-svg-chapter-inkscape-textarea]]
=== Text areas

When using the Inkscape text tool, select the tool, and left click once on the canvas to add a text label.
Once you added it, start typing text.

Inkscape also allows you to select the text tool, left click anywhere on the canvas, drag the mouse pointer to another position, and only then release the left mouse button.
This creates a text area, where text is automatically wrapped to multiple lines, so that it stays within the bounds of the text area.
