//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

include::../_part_attributes.asciidoc[]

[[lang-ref-svg-cifsvg]]
== CIF/SVG declarations

indexterm:[SVG declaration]
indexterm:[SVG,declarations]
indexterm:[I/O declaration,SVG declaration]
indexterm:[CIF/SVG declaration]
CIF supports visualizing the state of a model using SVG images, as well interacting with these images during for instance a simulation.
To better understand these concepts, see the CIF tutorial lessons on <<lang-tut-svg-chapter-visualization,SVG visualization>> and <<lang-tut-svg-chapter-interaction,SVG interaction>>.
Here, we focus more on their technical details.

SVG images can be connected to the CIF model, the images can be updated based on the state of the model, and clicks on elements of the images can be used to influence the behavior of the model.
All of these connections are specified in a CIF model using CIF/SVG declarations (sometimes also called SVG declarations, for simplicity).

The following types of CIF/SVG declarations are available:

* <<lang-ref-svg-svgfile>>:
Specifies which SVG image file to use.

* <<lang-ref-svg-svgcopy>>:
Specifies that a part of an SVG image should be copied.

* <<lang-ref-svg-svgmove>>:
Specifies that a part of an SVG image should be moved.

* <<lang-ref-svg-svgout>>:
Specifies how to update a certain property of an element of an SVG image.

* <<lang-ref-svg-svgin>>:
Specifies how interaction with an SVG image, by clicking on certain elements of an image, affects the behavior of the model.
