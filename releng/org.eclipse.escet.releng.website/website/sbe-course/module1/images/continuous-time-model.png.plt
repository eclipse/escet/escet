################################################################################
# Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
#
# See the NOTICE file(s) distributed with this work for additional
# information regarding copyright ownership.
#
# This program and the accompanying materials are made available under the terms
# of the MIT License which is available at https://opensource.org/licenses/MIT
#
# SPDX-License-Identifier: MIT
################################################################################

# Requires Gnuplot 4.4 or higher.

set terminal pngcairo size 800,300 font 'sans'
set output "continuous-time-model.png"

set style line 1 linecolor rgb '#dc3912' linetype 1 linewidth 2
set style line 2 linecolor rgb '#3366cc' linetype 1 linewidth 2

unset key

set xtics 1

set xlabel "Time"
set ylabel "Speed"

set style line 11 lc rgb '#444444' lt 1
set border 3 back ls 11
set border linewidth 1.5

set style line 12 lc rgb'#808080' lt 0 lw 1
set grid back ls 12
set grid back

set xrange [0:18]
set yrange [0:140]

plot (sin(x * 0.35) + 1.15) * 50 ls 1
