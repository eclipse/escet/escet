//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin.collectors;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.project.MavenProject;
import org.eclipse.escet.common.asciidoc.source.checker.AsciiDocSourceChecker;
import org.eclipse.escet.common.asciidoc.source.checker.AsciiDocSourceProblem;
import org.eclipse.escet.releng.problemreporter.plugin.problems.FileProblem;
import org.eclipse.escet.releng.problemreporter.plugin.problems.FileProblems;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;

/**
 * Problem collector that collects AsciiDoc source checker problems.
 *
 * @see AsciiDocSourceChecker
 */
public class AsciiDocSourceCheckerProblemCollector implements MavenBuildProblemCollector {
    @Override
    public FileProblems collect(MavenSession session) throws IOException {
        // Check each project, collect the problems, and return them.
        FileProblems problems = new FileProblems("AsciiDoc source checker");
        for (MavenProject project: session.getProjects()) {
            // Find problem report.
            Path projectBaseDir = project.getBasedir().toPath();
            String buildDir = project.getBuild().getDirectory();
            Path reportJsonPath = projectBaseDir.resolve(buildDir).resolve("asciidoc-check-results.json");
            if (Files.isRegularFile(reportJsonPath)) {
                // Read problem report.
                GsonBuilder gsonBuilder = new GsonBuilder();
                JsonDeserializer<Path> pathDeserializer = (path, type, ctxt) -> Paths.get(path.getAsString());
                gsonBuilder.registerTypeHierarchyAdapter(Path.class, pathDeserializer);
                Gson gson = gsonBuilder.create();
                List<AsciiDocSourceProblem> projectProblems;
                try (InputStream stream = new BufferedInputStream(new FileInputStream(reportJsonPath.toFile()));
                     Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8))
                {
                    projectProblems = gson.fromJson(reader, new ListOfProblemsTypeToken().getType());
                }

                // Add problems.
                String projectId = project.getArtifactId();
                for (AsciiDocSourceProblem projectProblem: projectProblems) {
                    problems.add(projectId, projectProblem.path.toString(),
                            new FileProblem(projectProblem.line, projectProblem.column, projectProblem.message));
                }
            }
        }
        return problems;
    }

    /** Type token for GSON deserialization of a list of problems. */
    private static class ListOfProblemsTypeToken extends TypeToken<List<AsciiDocSourceProblem>> {
        // Nothing here.
    }
}
