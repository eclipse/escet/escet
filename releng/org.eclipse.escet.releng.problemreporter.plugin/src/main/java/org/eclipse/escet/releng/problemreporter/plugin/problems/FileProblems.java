//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin.problems;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/** Build problems that are associated to a specific lines and columns in files of projects. */
public class FileProblems extends BuildProblems {
    /** Per project, per file, its problems. */
    private final Map<String, Map<String, List<FileProblem>>> problems = new LinkedHashMap<>();

    /**
     * Constructor for the {@link BuildProblems} class.
     *
     * @param name The name of these problems.
     */
    public FileProblems(String name) {
        super(name);
    }

    /**
     * Add a problem.
     *
     * @param project The project with the problem.
     * @param filePath The path to the file with the problem.
     * @param problem The problem.
     */
    public void add(String project, String filePath, FileProblem problem) {
        problems.computeIfAbsent(project, p -> new LinkedHashMap<>()).computeIfAbsent(filePath, p -> new ArrayList<>())
                .add(problem);
    }

    @Override
    public long getCount() {
        return problems.entrySet().stream().flatMap(e -> e.getValue().entrySet().stream())
                .flatMap(e -> e.getValue().stream()).count();
    }

    /**
     * Returns the projects with problems, sorted by their names.
     *
     * @return The names of the projects, in sorted order.
     */
    public List<String> getSortedProjects() {
        return problems.keySet().stream().sorted().toList();
    }

    /**
     * Returns the paths of the files with problems, sorted by their names, for the given project.
     *
     * @param project The name of the project.
     * @return The paths of the files with problems, in sorted order.
     */
    public List<String> getSortedFilePaths(String project) {
        return problems.get(project).keySet().stream().sorted().toList();
    }

    /**
     * Returns the problems, sorted by line, column and problem message, for the given project and file.
     *
     * @param project The name of the project.
     * @param filePath The path to the file.
     * @return The problems, in sorted order.
     */
    public List<FileProblem> getSortedProblems(String project, String filePath) {
        return problems.get(project).get(filePath).stream().sorted().toList();
    }
}
