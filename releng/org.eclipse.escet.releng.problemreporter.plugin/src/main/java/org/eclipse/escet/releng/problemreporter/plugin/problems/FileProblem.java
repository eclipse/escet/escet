//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin.problems;

import java.util.Comparator;
import java.util.Locale;

/** A problem in a file. */
public class FileProblem implements Comparable<FileProblem> {
    /** The line with the problem, as 1-based line index from the start of the file. */
    public final int lineNr;

    /** The column with the problem, as 1-based column index from the start of the line. */
    public final int columnNr;

    /** The problem message. */
    public final String message;

    /**
     * Constructor for the {@link FileProblem} class.
     *
     * @param lineNr The line with the problem, as 1-based line index from the start of the file.
     * @param columnNr The column with the problem, as 1-based column index from the start of the line.
     * @param message The problem message.
     */
    public FileProblem(int lineNr, int columnNr, String message) {
        this.lineNr = lineNr;
        this.columnNr = columnNr;
        this.message = message;
    }

    @Override
    public int compareTo(FileProblem other) {
        return Comparator.comparing((FileProblem p) -> p.lineNr).thenComparing(p -> p.columnNr)
                .thenComparing(p -> p.message).compare(this, other);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "Line %d, column %d, problem \"%s\".", lineNr, columnNr, message);
    }
}
