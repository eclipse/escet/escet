//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin.collectors;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.maven.execution.MavenSession;
import org.eclipse.escet.releng.problemreporter.plugin.problems.GlobalProblems;

/** Problem collector that collects license header check problems. */
public class LicenseHeaderCheckProblemCollector implements MavenBuildProblemCollector {
    @Override
    public GlobalProblems collect(MavenSession session) throws IOException {
        Path rootPath = Paths.get(session.getExecutionRootDirectory());
        Path filePath = rootPath.resolve("misc").resolve("license-header").resolve("license-header-issues.txt");
        List<String> problems = Files.readAllLines(filePath, StandardCharsets.UTF_8);
        return new GlobalProblems("License header check", problems);
    }
}
