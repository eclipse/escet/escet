//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin.problems;

/** Problems reported during the build. */
public abstract class BuildProblems {
    /** The name of these problems. */
    private final String name;

    /**
     * Constructor for the {@link BuildProblems} class.
     *
     * @param name The name of these problems.
     */
    public BuildProblems(String name) {
        this.name = name;
    }

    /**
     * Returns the name of these problems.
     *
     * @return The name of these problems.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the number of problems.
     *
     * @return The number of problems.
     */
    public abstract long getCount();
}
