//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin.problems;

import java.util.Collections;
import java.util.List;

/** Build problems that are global, i.e., not associated to particular projects, files, etc. */
public class GlobalProblems extends BuildProblems {
    /** The problems. */
    private final List<String> problems;

    /**
     * Constructor for the {@link GlobalProblems} class.
     *
     * @param name The name of these problems.
     * @param problems The problems.
     */
    public GlobalProblems(String name, List<String> problems) {
        super(name);
        this.problems = Collections.unmodifiableList(problems);
    }

    /**
     * Returns the problems.
     *
     * @return The problems.
     */
    public List<String> getProblems() {
        return problems;
    }

    @Override
    public long getCount() {
        return problems.size();
    }
}
