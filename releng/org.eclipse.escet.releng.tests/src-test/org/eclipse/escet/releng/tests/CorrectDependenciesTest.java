//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.tests;

import static org.eclipse.escet.common.java.Lists.concat;
import static org.eclipse.escet.common.java.Lists.first;
import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Lists.listc;
import static org.eclipse.escet.common.java.Maps.map;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiPredicate;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.Lists;
import org.eclipse.escet.common.java.Strings;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/** Check this project for being a test aggregation of all projects with tests. */
public class CorrectDependenciesTest {
    /**
     * Test that the manifest and POM dependencies together contain a dependency on every other project with tests, and
     * no other projects.
     *
     * @throws ParserConfigurationException In case no XML parser can be created.
     * @throws IOException In case of an I/O error.
     * @throws SAXException In case the launch configuration file can't be parsed.
     * @throws XPathExpressionException If XML query can't be evaluated.
     */
    @Test
    public void testDependencies()
            throws IOException, SAXException, ParserConfigurationException, XPathExpressionException
    {
        // Get manifest.
        Path manifestPath = Paths.get(".").resolve("META-INF").resolve("MANIFEST.MF");
        Manifest manifest;
        try (InputStream stream = new FileInputStream(manifestPath.toFile())) {
            manifest = new Manifest(stream);
        }

        // Get projects that are a dependency of this project, as indicated in the manifest.
        String requireBundleValue = manifest.getMainAttributes().getValue("Require-Bundle");
        requireBundleValue = requireBundleValue.replaceAll(";bundle-version=\"[^\"]+\"", "");
        Assert.check(!requireBundleValue.contains(";"), requireBundleValue);
        List<String> manifestProjectNames = Arrays.asList(requireBundleValue.split(",")).stream().sorted().toList();

        // Read POM file.
        Path pomPath = Paths.get("pom.xml");
        Document pomDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(pomPath.toFile());
        NodeList pomDependencyNodes = (NodeList)XPathFactory.newInstance().newXPath().evaluate(
                "/project/dependencies/dependency/artifactId", pomDoc, XPathConstants.NODESET);
        List<String> pomProjectNames = list();
        for (int i = 0; i < pomDependencyNodes.getLength(); i++) {
            NodeList children = pomDependencyNodes.item(i).getChildNodes();
            for (int j = 0; j < children.getLength(); j++) {
                pomProjectNames.add(children.item(j).getTextContent());
            }
        }

        // Get all dependency project names, in sorted order.
        List<String> dependencyProjectNames = concat(manifestProjectNames, pomProjectNames).stream().sorted().toList();

        // Get test projects, excluding this project, in sorted order.
        String thisProjectName = Paths.get(".").toAbsolutePath().normalize().getFileName().toString();
        List<String> testProjectNames = getTestProjectNames().stream().filter(p -> !p.equals(thisProjectName)).toList();

        // Check that the dependencies of this project are all test projects, except this project itself.
        assertLinesMatch(testProjectNames, dependencyProjectNames);
    }

    /**
     * Test that the 'test-all' launch configuration runs the tests of all projects with tests, including this project.
     * Concretely, it checks that one launch configuration is executed per project with tests.
     *
     * @throws ParserConfigurationException In case no XML parser can be created.
     * @throws IOException In case of an I/O error.
     * @throws SAXException In case the launch configuration file can't be parsed.
     */
    @Test
    public void testLaunchConfig() throws SAXException, IOException, ParserConfigurationException {
        // Read launch configuration file.
        Path launchConfigPath = Paths.get("test-all.launch");
        Document launchConfigDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(launchConfigPath.toFile());

        // Get launch configurations that are part of the launch group, by their 0-based index in the launch group.
        NodeList launchEntries = launchConfigDoc.getElementsByTagName("stringAttribute");
        Pattern attrNamePattern = Pattern.compile("org.eclipse.debug.core.launchGroup.(\\d+).name");
        Map<Integer, String> launchElementsByNr = map();
        for (int i = 0; i < launchEntries.getLength(); i++) {
            // Get attributes of the entry.
            NamedNodeMap attrs = launchEntries.item(i).getAttributes();

            // Get 0-based index number.
            String key = attrs.getNamedItem("key").getNodeValue();
            Matcher matcher = attrNamePattern.matcher(key);
            if (!matcher.matches()) {
                continue;
            }
            int nr = Integer.parseInt(matcher.group(1));

            // Get value, i.e., the launch configuration name.
            String value = attrs.getNamedItem("value").getNodeValue();

            // Store the information.
            launchElementsByNr.put(nr, value);
        }

        // Find all launch configuration files, as mapping from launch config name to names of projects that contain it.
        Map<String, List<String>> launchNameToProjectName;
        BiPredicate<Path, BasicFileAttributes> matcher = (p, a) -> p.getFileName().toString().endsWith(".launch");
        try (Stream<Path> paths = Files.find(getRepoPath(), Integer.MAX_VALUE, matcher)) {
            launchNameToProjectName = paths.collect(Collectors.toMap(
                    // Launch configuration filename (without the file extension) is the key.
                    p -> Strings.slice(p.getFileName().toString(), null, -".launch".length()),
                    // Project name is the value.
                    p -> list(p.getParent().getFileName().toString()),
                    // In case of multiple projects with the same launch configuration, collect them all into a list.
                    (a, b) -> Lists.concat(a, b),
                    // Result is a map.
                    () -> map()));
        }

        // Map launch configuration names to project names, to get map of 0-based group index to project name.
        Map<Integer, String> launchProjectsByNr = map();
        for (Entry<Integer, String> entry: launchElementsByNr.entrySet()) {
            String launchName = entry.getValue();
            List<String> projectNames = launchNameToProjectName.get(launchName);
            Assert.notNull(projectNames, "No project found that contains launch configuration file: " + launchName);
            Assert.areEqual(projectNames.size(), 1, "Expected exactly one project for launch config: " + launchName);
            String projectName = first(projectNames);
            launchProjectsByNr.put(entry.getKey(), projectName);
        }

        // Get list of project names for which launch configurations are executed in the group launch configuration, in
        // the order that they are listed in the group launch configuration.
        List<String> launchProjectNames = listc(launchProjectsByNr.size());
        for (int i = 0; i < launchProjectsByNr.size(); i++) {
            String projectName = launchProjectsByNr.get(i);
            Assert.notNull(projectName, "No project found for a number in 'test-all' group launch configuration: " + i);
            launchProjectNames.add(projectName);
        }

        // Get test projects, including this project itself, in sorted order of their names.
        List<String> testProjectNames = getTestProjectNames();

        // Check that all test projects have a launch configuration in the group launch configuration, in the right
        // order.
        assertLinesMatch(testProjectNames, launchProjectNames);
    }

    /**
     * Get the path to the root of the Git repo.
     *
     * @return The repo root path.
     */
    private Path getRepoPath() {
        Path projectPath = Paths.get(".").toAbsolutePath().normalize();
        Path repoPath = projectPath.getParent().getParent();
        Assert.check(Files.exists(repoPath.resolve(".gitattributes")));
        return repoPath;
    }

    /**
     * Get the names of the projects with tests.
     *
     * @return The names of the projects with tests.
     * @throws IOException In case of an I/O error.
     */
    private List<String> getTestProjectNames() throws IOException {
        BiPredicate<Path, BasicFileAttributes> matcher = (p, a) -> p.getFileName().toString().equals(".project");
        try (Stream<Path> paths = Files.find(getRepoPath(), Integer.MAX_VALUE, matcher)) {
            return paths
                    // Get the project directories of the found '.project' files.
                    .map(p -> p.getParent())
                    // Filter to only those projects with a test folder.
                    .filter(p -> Files.isDirectory(p.resolve("src-test"))
                            || Files.isDirectory(p.resolve("src-test-gen"))
                            || Files.isDirectory(p.resolve("src/test/java")))
                    // Get project names.
                    .map(p -> p.getFileName().toString()).sorted().toList();
        }
    }
}
