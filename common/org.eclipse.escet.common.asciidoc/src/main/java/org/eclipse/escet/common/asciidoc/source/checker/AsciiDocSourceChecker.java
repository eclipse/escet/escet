//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.common.asciidoc.source.checker;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.escet.common.asciidoc.source.checker.checks.SingleSentencePerLineCheck;
import org.eclipse.escet.common.asciidoc.source.checker.checks.SourceBlockIndentCheck;

import com.google.common.base.Verify;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

/** AsciiDoc source file checker. */
public class AsciiDocSourceChecker {
    /** Constructor for the {@link AsciiDocSourceChecker} class. */
    private AsciiDocSourceChecker() {
        // Static class.
    }

    /**
     * Main method of the AsciiDoc source file checker.
     *
     * @param args The command line arguments:
     *     <ul>
     *     <li>The path to the root directory that contains the AsciiDoc source files.</li>
     *     </ul>
     * @throws IOException In case of an I/O error.
     */
    public static void main(String[] args) throws IOException {
        Verify.verify(args.length == 1);
        Path sourceRootPath = Paths.get(args[0]);
        checkSources(sourceRootPath, System.out::println, System.err::println, null);
    }

    /**
     * Check AsciiDoc sources for common problems.
     *
     * @param sourceRootPath The path to the root directory that contains the AsciiDoc source files.
     * @param infoLogger The logger to use for the information stream.
     * @param errorLogger The logger to use for the error stream.
     * @param resultJsonPath The path to the JSON file to which to write the results of the check. May be {@code null}
     *     to not write the file.
     * @return The number of problems found.
     * @throws IOException In case of an I/O error.
     */
    public static int checkSources(Path sourceRootPath, Consumer<String> infoLogger, Consumer<String> errorLogger,
            Path resultJsonPath) throws IOException
    {
        // Search for AsciiDoc files.
        List<Path> asciiDocFiles;
        try {
            Predicate<Path> isAsciiDocFile = path -> {
                String fileName = path.getFileName().toString().toLowerCase(Locale.US);
                return fileName.endsWith(".adoc") || fileName.endsWith(".asciidoc");
            };
            asciiDocFiles = Files.walk(sourceRootPath).filter(Files::isRegularFile).filter(isAsciiDocFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IOException("Failed to search for AsciiDoc source files.", e);
        }

        // Check files.
        return checkSources(asciiDocFiles, infoLogger, errorLogger, resultJsonPath);
    }

    /**
     * Check AsciiDoc sources for common problems.
     *
     * @param asciiDocFiles The paths to the AsciiDoc source files to check.
     * @param infoLogger The logger to use for the information stream.
     * @param errorLogger The logger to use for the error stream.
     * @param resultJsonPath The path to the JSON file to which to write the results of the check. May be {@code null}
     *     to not write the file.
     * @return The number of problems found.
     * @throws IOException In case of an I/O error.
     */
    public static int checkSources(List<Path> asciiDocFiles, Consumer<String> infoLogger, Consumer<String> errorLogger,
            Path resultJsonPath) throws IOException
    {
        // Check AsciiDoc files.
        infoLogger.accept(String.format(Locale.US, "Checking %,d AsciiDoc source file(s).", asciiDocFiles.size()));
        List<AsciiDocSourceProblem> problems = new ArrayList<>();
        for (Path asciiDocFile: asciiDocFiles) {
            // Create check context.
            List<String> lines;
            try {
                lines = Files.readAllLines(asciiDocFile, StandardCharsets.UTF_8);
            } catch (IOException e) {
                throw new IOException("Failed to read AsciiDoc source file: " + asciiDocFile, e);
            }
            AsciiDocSourceCheckContext context = new AsciiDocSourceCheckContext(asciiDocFile, lines, problems);

            // Perform checks.
            new SingleSentencePerLineCheck().check(context);
            new SourceBlockIndentCheck().check(context);
        }

        // Log problems.
        if (problems.isEmpty()) {
            infoLogger.accept("No problems found.");
        } else {
            errorLogger.accept(String.format(Locale.US, "Found %,d problems:", problems.size()));
            Collections.sort(problems);
            for (AsciiDocSourceProblem problem: problems) {
                errorLogger.accept(" - " + problem.toString());
            }
        }

        // Write problems to result file, if requested.
        if (resultJsonPath != null) {
            // Get problems as JSON.
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setPrettyPrinting();
            JsonSerializer<Path> pathSerializer = (path, type, ctxt) -> new JsonPrimitive(path.toString());
            gsonBuilder.registerTypeHierarchyAdapter(Path.class, pathSerializer);
            Gson gson = gsonBuilder.create();
            String problemsJson = gson.toJson(problems);

            // Create directory that contains the JSON file, in case it doesn't yet exist.
            Files.createDirectories(resultJsonPath.getParent());

            // Write the JSON file.
            Files.writeString(resultJsonPath, problemsJson);
        }

        // Return the number of problems that were found.
        return problems.size();
    }
}
