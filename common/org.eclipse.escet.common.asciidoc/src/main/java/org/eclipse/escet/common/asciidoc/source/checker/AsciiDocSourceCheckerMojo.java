//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.common.asciidoc.source.checker;

import java.io.File;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * AsciiDoc source file checker Maven mojo.
 *
 * @see AsciiDocSourceChecker
 */
@Mojo(name = "source-check")
public class AsciiDocSourceCheckerMojo extends AbstractMojo {
    /** The path to the root directory that contains the AsciiDoc source files. */
    @Parameter(required = true)
    private File sourceRootPath;

    /** The path to the JSON file to which to write the results of the check, or {@code null} if not specified. */
    @Parameter
    private File problemReportJsonPath;

    /** Whether to fail the build on reported problems ({@code true}) or continue the build ({@code false}). */
    @Parameter(defaultValue = "true")
    private boolean failBuild;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        Log log = getLog();
        int problemCount;
        try {
            problemCount = AsciiDocSourceChecker.checkSources(sourceRootPath.toPath(), log::info, log::error,
                    (problemReportJsonPath == null) ? null : problemReportJsonPath.toPath());
        } catch (Throwable e) {
            log.error(e);
            throw new MojoExecutionException("Error while executing Maven plugin.", e);
        }

        if (failBuild && problemCount > 0) {
            throw new MojoFailureException("One or more AsciiDoc source files have problems.");
        }
    }
}
