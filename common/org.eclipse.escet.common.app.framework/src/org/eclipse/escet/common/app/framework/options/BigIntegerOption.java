//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.common.app.framework.options;

import static org.eclipse.escet.common.java.Strings.fmt;

import java.math.BigInteger;

import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.exceptions.InvalidOptionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/** Base class for all {@link BigInteger}-valued options. */
public abstract class BigIntegerOption extends Option<BigInteger> {
    /** Whether the option has a special value, encoded as {@code null}. */
    protected final boolean hasSpecialValue;

    /**
     * The default value of the {@link BigInteger}-valued option. May be {@code null} to indicate the special value, but
     * only if {@link #hasSpecialValue} is {@code true}.
     */
    protected final BigInteger defaultValue;

    /**
     * The default normal value of the {@link BigInteger}-valued option. Must be {@code null} if and only if
     * {@link #hasSpecialValue} is {@code false}.
     */
    protected final BigInteger defaultNormalValue;

    /** The minimum allowed value, or {@code null} for no minimum. */
    protected final BigInteger minimumValue;

    /** The maximum allowed value, or {@code null} for no maximum. */
    protected final BigInteger maximumValue;

    /**
     * The command line syntax for the special value. Must not overlap with the syntax of integers. Must be {@code null}
     * if {@link #hasSpecialValue} is {@code false}.
     */
    protected final String specialValueSyntax;

    /**
     * The description for this option, in the option dialog, or {@code null} if {@link #showInDialog} is {@code false}.
     */
    protected final String optDialogDescr;

    /**
     * The text of the label of the radio button for the special value. The text should represent that whenever the
     * radio button is selected, the special value is used. Must be {@code null} if {@link #hasSpecialValue} is
     * {@code false}, or if {@link #showInDialog} is {@code false}.
     */
    protected final String optDialogSpecialText;

    /**
     * The text of the label of the radio button for the normal value. The text should represent that whenever the radio
     * button is selected, the special value is not used. Must be {@code null} if {@link #hasSpecialValue} is
     * {@code false}, or if {@link #showInDialog} is {@code false}.
     */
    protected final String optDialogNormalText;

    /**
     * The text of the label before the {@link Text text widget} for this option, in the option dialog. May be
     * {@code null} to indicate that no text label is needed. Must be {@code null} if {@link #showInDialog} is
     * {@code false}.
     */
    protected final String optDialogLabelText;

    /**
     * Constructor for the {@link BigIntegerOption} class, without a special value. Don't directly create instances of
     * derived classes. Use the {@link Options#getInstance} method instead.
     *
     * @param name The name of the option. Example: {@code "Number of runs"}.
     * @param description The description of the option. Example: {@code
     *      "The number of runs to perform. Must be non-negative. [DEFAULT=20]"}.
     * @param cmdShort The short (one letter) option name, for command line processing. May be {@code null} if the
     *     option has no short name.
     * @param cmdLong The long option name (excluding the "--" prefix), for command line processing. Must not be
     *     {@code null}. Must not be "*".
     * @param cmdValue The name of the option value, for command line processing. Must not be {@code null}.
     * @param defaultValue The default value of the option. Mostly irrelevant if the option is mandatory.
     * @param minimumValue The minimum allowed value, or {@code null} for no minimum.
     * @param maximumValue The maximum allowed value, or {@code null} for no maximum.
     * @param showInDialog Whether to show the option in option dialogs.
     * @param optDialogDescr The description for this option, in the option dialog, or {@code null} if
     *     {@link #showInDialog} is {@code false}. Example: {@code "The number of runs to perform."}.
     * @param optDialogLabelText The text of the label before the {@link Text text widget} for this option, in the
     *     option dialog. <b>If set, text must end with a colon.</b> May be {@code null} to indicate that no text label
     *     is needed. Must be {@code null} if {@link #showInDialog} is {@code false}. Example: {@code "Runs:"}.
     */
    public BigIntegerOption(String name, String description, Character cmdShort, String cmdLong, String cmdValue,
            BigInteger defaultValue, BigInteger minimumValue, BigInteger maximumValue, boolean showInDialog,
            String optDialogDescr, String optDialogLabelText)
    {
        this(name, description, cmdShort, cmdLong, cmdValue, defaultValue, minimumValue, maximumValue,
                showInDialog, optDialogDescr, optDialogLabelText, false, null, null, null, null);
    }

    /**
     * Constructor for the {@link BigIntegerOption} class. Don't directly create instances of derived classes. Use the
     * {@link Options#getInstance} method instead.
     *
     * @param name The name of the option. Example: {@code "Number of runs"}.
     * @param description The description of the option. Example: {@code
     *      "The number of runs to perform. Specify \"auto\" to use an automatically determined number of runs,
     *      or a custom non-negative number of runs. [DEFAULT=auto]"}.
     * @param cmdShort The short (one letter) option name, for command line processing. May be {@code null} if the
     *     option has no short name.
     * @param cmdLong The long option name (excluding the "--" prefix), for command line processing. Must not be
     *     {@code null}. Must not be "*".
     * @param cmdValue The name of the option value, for command line processing. Must not be {@code null}.
     * @param defaultValue The default value of the option. Mostly irrelevant if the option is mandatory. May be
     *     {@code null} to indicate the special value, but only if {@link #hasSpecialValue} is {@code true}.
     * @param minimumValue The minimum allowed value, or {@code null} for no minimum.
     * @param maximumValue The maximum allowed value, or {@code null} for no maximum.
     * @param showInDialog Whether to show the option in option dialogs.
     * @param optDialogDescr The description for this option, in the option dialog, or {@code null} if
     *     {@link #showInDialog} is {@code false}. Example: {@code "The number of runs to perform."}.
     * @param optDialogLabelText The text of the label before the {@link Text text widget} for this option, in the
     *     option dialog. <b>If set, text must end with a colon.</b> May be {@code null} to indicate that no text label
     *     is needed. Must be {@code null} if {@link #showInDialog} is {@code false}. Example: {@code "Runs:"}.
     * @param hasSpecialValue Whether the option has a special value, encoded as {@code null}.
     * @param defaultNormalValue The default normal value of the {@link BigInteger}-valued option. Must be {@code null}
     *     if and only if {@link #hasSpecialValue} is {@code false}.
     * @param specialValueSyntax The command line syntax for the special value. Must not overlap with the syntax of
     *     integers. Must be {@code null} if {@link #hasSpecialValue} or {@link #showInDialog} is {@code false}.
     * @param optDialogSpecialText The text of the label of the radio button for the special value. The text should
     *     represent that whenever the radio button is selected, the special value is used. Must be {@code null} if
     *     {@link #hasSpecialValue} is {@code false}, or if {@link #showInDialog} is {@code false}. Example: {@code "Use
     *      automatically determined number of runs"}.
     * @param optDialogNormalText The text of the label of the radio button for the normal value. The text should
     *     represent that whenever the radio button is selected, the special value is not used. Must be {@code null} if
     *     {@link #hasSpecialValue} is {@code false}, or if {@link #showInDialog} is {@code false}. Example: {@code
     *      "Use custom number of runs"}.
     */
    public BigIntegerOption(String name, String description, Character cmdShort, String cmdLong, String cmdValue,
            BigInteger defaultValue, BigInteger minimumValue, BigInteger maximumValue, boolean showInDialog,
            String optDialogDescr, String optDialogLabelText, boolean hasSpecialValue, BigInteger defaultNormalValue,
            String specialValueSyntax, String optDialogSpecialText, String optDialogNormalText)
    {
        super(name, description, cmdShort, cmdLong, cmdValue, showInDialog);
        Assert.notNull(cmdLong);
        Assert.check(!cmdLong.equals("*"));
        Assert.notNull(cmdValue);
        Assert.implies(defaultValue == null, hasSpecialValue);
        if (defaultValue != null) {
            if (minimumValue != null) {
                Assert.check(minimumValue.compareTo(defaultValue) <= 0);
            }
            if (maximumValue != null) {
                Assert.check(defaultValue.compareTo(maximumValue) <= 0);
            }
        }
        Assert.ifAndOnlyIf(!showInDialog || !hasSpecialValue, defaultNormalValue == null);
        if (defaultNormalValue != null) {
            if (minimumValue != null) {
                Assert.check(minimumValue.compareTo(defaultNormalValue) <= 0);
            }
            if (maximumValue != null) {
                Assert.check(defaultNormalValue.compareTo(maximumValue) <= 0);
            }
        }
        Assert.ifAndOnlyIf(showInDialog, optDialogDescr != null);
        Assert.implies(!showInDialog, optDialogLabelText == null);
        if (optDialogLabelText != null) {
            Assert.check(optDialogLabelText.endsWith(":"));
        }
        Assert.ifAndOnlyIf(hasSpecialValue, specialValueSyntax != null);
        Assert.ifAndOnlyIf(hasSpecialValue && showInDialog, optDialogSpecialText != null);
        Assert.ifAndOnlyIf(hasSpecialValue && showInDialog, optDialogNormalText != null);
        this.defaultValue = defaultValue;
        this.minimumValue = minimumValue;
        this.maximumValue = maximumValue;
        this.optDialogDescr = optDialogDescr;
        this.optDialogLabelText = optDialogLabelText;
        this.hasSpecialValue = hasSpecialValue;
        this.defaultNormalValue = defaultNormalValue;
        this.specialValueSyntax = specialValueSyntax;
        this.optDialogSpecialText = optDialogSpecialText;
        this.optDialogNormalText = optDialogNormalText;
    }

    @Override
    public BigInteger getDefault() {
        return defaultValue;
    }

    @Override
    public BigInteger parseValue(String optName, String value) {
        // Special value handling.
        if (hasSpecialValue) {
            if (value.equals(specialValueSyntax)) {
                return null;
            }
        }

        // Normal value.
        BigInteger v;
        try {
            v = new BigInteger(value);
        } catch (NumberFormatException e) {
            String msg = fmt("Invalid integer number \"%s\".", value);
            throw new InvalidOptionException(msg);
        }
        if (minimumValue != null) {
            checkValue(v.compareTo(minimumValue) >= 0, v + " < " + minimumValue);
        }
        if (maximumValue != null) {
            checkValue(v.compareTo(maximumValue) <= 0, v + " > " + maximumValue);
        }
        return v;
    }

    @Override
    public String[] getCmdLine(Object value) {
        if (value == null) {
            Assert.check(hasSpecialValue);
            return new String[] {"--" + cmdLong + "=" + specialValueSyntax};
        } else {
            return new String[] {"--" + cmdLong + "=" + value};
        }
    }

    @Override
    public OptionGroup<BigInteger> createOptionGroup(Composite page) {
        // Paranoia checking.
        Assert.check(showInDialog);

        // Construct option group.
        return new BigIntegerOptionGroup(page);
    }

    /** Option dialog option group for the {@link BigIntegerOption}. */
    private class BigIntegerOptionGroup extends OptionGroup<BigInteger> implements SelectionListener {
        /** Radio button for the special value, if applicable. */
        private Button specialButton;

        /** Radio button for the normal value, if applicable. */
        private Button normalButton;

        /** Label before the text widget, if applicable. */
        Label valueLabel;

        /** Text widget. */
        Text valueText;

        /**
         * Constructor for the {@link BigIntegerOptionGroup} class.
         *
         * @param page The options page that is the parent of this option group.
         */
        public BigIntegerOptionGroup(Composite page) {
            super(page, BigIntegerOption.this);
        }

        @Override
        protected void addComponents(Group group) {
            if (hasSpecialValue) {
                specialButton = new Button(group, SWT.RADIO);
                specialButton.setText(optDialogSpecialText);
                normalButton = new Button(group, SWT.RADIO);
                normalButton.setText(optDialogNormalText);
                specialButton.addSelectionListener(this);
                normalButton.addSelectionListener(this);
            }

            valueText = new Text(group, SWT.BORDER);
            String defaultText = ((defaultValue == null) ? defaultNormalValue : defaultValue).toString();
            valueText.setText(defaultText);

            Control[] valueLayout;
            if (optDialogLabelText == null) {
                valueLayout = new Control[] {valueText};
            } else {
                valueLabel = new Label(group, SWT.NULL);
                valueLabel.setText(optDialogLabelText);
                valueLayout = new Control[] {valueLabel, valueText};
            }

            Object[] layout;
            if (hasSpecialValue) {
                layout = new Object[] {specialButton, normalButton, valueLayout};
            } else if (optDialogLabelText == null) {
                layout = valueLayout;
            } else {
                layout = new Object[] {valueLayout};
            }

            layoutGeneric(layout, hasSpecialValue ? 32 : 0);
        }

        @Override
        public String getDescription() {
            return optDialogDescr;
        }

        @Override
        public void setToValue(BigInteger value) {
            if (value == null) {
                Assert.check(hasSpecialValue);

                specialButton.setSelection(true);
                Event event = new Event();
                event.widget = specialButton;
                widgetSelected(new SelectionEvent(event));

                value = defaultNormalValue;
            } else if (hasSpecialValue) {
                normalButton.setSelection(true);
                Event event = new Event();
                event.widget = normalButton;
                widgetSelected(new SelectionEvent(event));
            }

            valueText.setText(value.toString());
        }

        @Override
        public String[] getCmdLine() {
            if (hasSpecialValue && specialButton.getSelection()) {
                return new String[] {"--" + cmdLong + "=" + specialValueSyntax};
            }

            return new String[] {"--" + cmdLong + "=" + valueText.getText()};
        }

        @Override
        public void widgetSelected(SelectionEvent e) {
            if (e.widget == specialButton) {
                valueText.setEnabled(false);
            } else if (e.widget == normalButton) {
                valueText.setEnabled(true);
            }
        }

        @Override
        public void widgetDefaultSelected(SelectionEvent e) {
            widgetSelected(e);
        }
    }
}
